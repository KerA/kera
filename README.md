# KerA

A unified ingestion and storage system for scalable big data processing.

KerA is developed within the [KerData team](https://team.inria.fr/kerdata/). The project is licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0).

## Features

### Design principles

KerA is a unified ingestion and storage system that meets the following design principles:

- **Unified analytics architecture**: KerA focuses on how to transform data through a workflow composed of stateful and/or stateless stream-based operators. It has been designed with the computation in mind: how to define the computation, when to trigger the computation and how to combine the computation with offline analytics. Furthermore, all high-level data management (fault-tolerance, persistence of operator states, etc.) are handled natively by the unified layer. Stream-based interfaces are provided for data ingestion, storage and processing.
- **Scalable data ingestion**: KerA offers dynamic partitioning using semantic grouping. Logical partitions defined by the user correspond to multiple physical sub-partitions that are created and filled in order by producers and dynamically discovered and processed in order by consumers. KerA also includes a lightweight offset indexing that assigns offsets at coarse granularity (sub-partition level).
- **Diverse data access patterns**: KerA provides interfaces for both online (records or streams) and offline (objects) access patterns. The data model is flexible enough to allow the access to streams/objects and the fine-grained access to records of a stream. The latter are modelled with a multi-key-value format. Furthermore, the ingestion/storage system leverages a log-structured approach (based on RAMCloud) for both data in memory and on disk. Replication is possible in both synchronous and asynchronous modes.
- **Efficient integration**: KerA should be co-located with processing engines by leveraging a shared memory buffer, thus bypassing the network and reducing the communication interferences between reads and writes. Metadata is handled on the broker providing access to the corresponding data.

### RAMCloud

KerA was developed on top of [RAMCloud](https://ramcloud.atlassian.net/wiki/spaces/RAM/overview) to leverage its RPC framework.

RAMCloud is a new class of super-high-speed storage for large-scale datacenter applications. It is designed for applications in which a large number of servers in a datacenter need low-latency access to a large durable datastore. RAMCloud offers the following properties:

- **Low latency**: RAMCloud keeps all data in DRAM at all times, so applications can read RAMCloud objects remotely over a datacenter network in as little as 5μs. Writes take less than 15μs. Unlike systems such as memcached, applications never have to deal with cache misses or wait for disk/memory accesses. As a result, RAMCloud storage is 10-1000x faster than other available alternatives.
- **Large scale**: RAMCloud aggregates the DRAM of thousands of servers to support total capacities of 1PB or more.
- **Durability**: RAMCloud replicates all data on nonvolatile secondary storage such as disk or memory, so no data is lost if servers crash or the power fails. One of RAMCloud's unique features is that it recovers very quickly from server crashes (only 1-2 seconds) so the availability gaps after crashes are almost unnoticeable. As a result, RAMCloud combines the durability of replicated disk with the speed of DRAM. If you have used memcached, you have probably experienced the challenges of managing a second durable storage system and maintaining consistency between it and memcached. With RAMCloud, there is no need for a second storage system.
- **Powerful data model**: RAMCloud's basic data model is a key-value store, but we have extended it with several additional features, such as:
  - Multiple tables, each with its own key space.
  - Transactional updates that span multiple objects in different tables.
  - Secondary indices.
  - Strong consistency: unlike other NoSQL storage systems, all updates in RAMCloud are consistent, immediately visible, and durable.
- **Easy deployment**: RAMCloud is a software package that runs on commodity Intel servers with the Linux operating system. RAMCloud is available freely in open source form.

The RAMCloud project is based in the Department of Computer Science at Stanford University. It is no longer maintained.

## Documentation

KerA is documented on its [wiki](https://gitlab.inria.fr/Kerdata/Kerdata-Codes/kera/-/wikis/home).