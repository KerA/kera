/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_WRITEGROUPBATCHMULTIRPCWRAPPER_H
#define RAMCLOUD_WRITEGROUPBATCHMULTIRPCWRAPPER_H

#include <iostream>

#include "GroupMultiRpcWrapper.h"

namespace KerA {

class WriteGroupBatchMultiRpcWrapper : public GroupMultiRpcWrapper {
    static const WireFormat::MultiOpGroup::OpType type =
                      WireFormat::MultiOpGroup::OpType::WRITEGROUPBATCH;

  PUBLIC:
    WriteGroupBatchMultiRpcWrapper(RamCloud* ramcloud,
                                   MultiWriteGroupBatchObject* const requests[],
                                   uint32_t numRequests,
			                             uint64_t tableId, uint64_t producerId);

  PROTECTED:
    void appendRequest(MultiOpGroupObject* request, Buffer* buf);
    bool readResponse(MultiOpGroupObject* request,
                      Buffer* response, uint32_t* respOffset);
};

} // end KerA

#endif /* WRITEGROUPBATCHMULTIRPCWRAPPER_H */
