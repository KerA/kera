/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_STREAMLET_H
#define RAMCLOUD_STREAMLET_H

#include <stdint.h>
#include <stdio.h>
#include <unordered_map>
#include <list>

#include "Common.h"
#include "SpinLock.h"
#include "StreamletGroupMetadata.h"
#include "Segment.h"
#include "Buffer.h"
#include "Log.h"
#include "LogSegment.h"

namespace KerA {

/**
 * METADATA FOR STREAMLET GROUPS
 * corresponds to streamlet entry (set of groups)
 */
class Streamlet {
public:

	Streamlet(uint64_t streamId, uint32_t streamletId,
			uint32_t numberSegmentsPerGroup,
			uint32_t numberActiveGroupsPerStreamlet,
			const std::vector<Log*> &logs);
	~Streamlet();

	SpinLock appendLock; //streamlet lock
	UnnamedSpinLock entryLocks[32]; //not more than 32 active groups per streamlet

	//if true this streamlet is under migration
	//migration should first update segmentManager idToLogicalSegmentMap
	//the persistence manager checks this flag to true, flushes segment, takes lock, appends reference to physical segment, checks this flag back to false
	//if the migration manager encounters this flag to true, will wait & retry
	 std::atomic<bool> migrationFlag;

	uint64_t streamId;
	uint32_t streamletId;
	uint64_t nextGroupId; //unique per streamlet, nextGroupId-1 is maxGroupId/last group

	// I need minGroupId later - used by cleaner and catch up readers eventually
	uint64_t minGroupId;
	uint64_t maxGroupId;

    /// for each streamletId we have a vector of Q entries, each entry holds the next active groupId (used by producers)
	std::vector<StreamletGroupMetadata*> streamletIdToEntryGroupMap;
	uint32_t numberSegmentsPerGroup;
	uint32_t numberActiveGroupsPerStreamlet;

    //next Segment reference may be invalidated/updated by the persistence manager/cleaner
    //this can happen when a physical segment is written to disk and/or released
    //the other place keeping a reference to segments is the virtual log - see idToVirtualSegmentsMap
    // we design something similar for stream-streamlet-group-logicalSegment <-> physicalSegment reference
    // idToLogicalSegmentMap - do we need to go through the stream since a streamlet may be migrated later? maybe, however we keep ids

    //todo how to decide when to release a physical segment (persistence manager): a few steps
    //general policy
    //e.g., if 95% of segments are durably closed, start releasing segments, first from segments persisted not yet released
    //if 75% of segments are durably closed, start writing segments to disk -> 25%, first from oldest already processed
    //old-new policy
    //always start with oldest durably closed segments
    //reader impact
    //should avoid releasing segments not yet processed: the 25%
    //catch up readers
    //the persistence manager should maintain statistics about last accessed group-segments per stream/let
    //these statistics should be refreshed every 5% of usage increase
    //segments are only released if they are not be consumed next according to statistics
    //since the system manages multiple streams, each stream can potentially have one reader

    //todo getSegmentById should be updated according to existence of the physical segment reference
    //and should trigger the persistence manager accordingly (catch up readers)


    /// keeps the physicalSegmentId for each logical segmentId of a given streamId/streamletId/groupId
    /// groupId is unique per streamlet todo design cleaner
    /// logicalSegmentId 1..P
    /// gets accessed by readers and can trigger a Cleaner fetch


    //on SegmentManager do
    // create a table REFT that  points from physicalSegmentId to logicalSegment*
    //to reduce contention, make it like a hashtable with each key pointing to a table partition of REFT
    //physicalSegmentId % 1024 -> keyLock
    //take keyLock for reads and by persistence manager

	//size of groupIdLogicalIdToPhysicalSegmentIdMap[groupId%32][groupId] gives the last segmentId since segments are 1..P
//	std::unordered_map<uint16_t, //groupIdBucket
	 std::unordered_map<uint64_t, //groupId unique per streamlet
	 std::unordered_map<uint64_t, //logicalSegmentId to physicalSegmentId
	 	   uint64_t>> groupIdLogicalIdToPhysicalSegmentIdMap;
	//groupIdLogicalIdToPhysicalSegmentIdMap[groupId%32][groupId][logicalSegmentId] = physicalSegmentId

    // if more consumers share a streamlet and each has to consume producer's groups in order
    //we need to maintain order of groups per entry and I would need another map here
    //check Stream::streamIdToStreamletIdToAvailableActiveGroups
    //probably not needed for now

	//whenever a new consumer group is registered with the coordinator for processing one stream,
	//Masters receive a readerId to insert into this structure based on streamIdToStreamletIdToAvailableActiveGroups
	//a consumer group can only register for a stream after the stream is created
	//next structure keeps evidence of last group id allocated for processing
	std::map<uint64_t, //readerId - multiple consumer groups/readers may share streamlets, index always updated
	uint64_t> //last group id
	 readerIdProcessedActiveGroupIndex; //readerId is id of the consumer group's application;
	//the reader is responsible for keeping group's offset which is defined as [streamId, streamletId, groupId, segmentId, offset/position]

    uint32_t getNextAvailableGroupSegmentIds(uint64_t readerId, uint32_t nActiveGroups, Buffer* response);
    uint64_t getNextAvailableGroupId(uint64_t readerId);

    void updateGroupIdToSegmentsListMap(uint64_t groupId, uint64_t segmentId, uint64_t physicalSegmentId);

    //get the associated physicalSegment id
    uint64_t getSegmentById(uint64_t groupId, uint64_t segmentId);

    StreamletGroupMetadata* getStreamletGroupMetadata(uint32_t entryIndex);
    uint64_t getNextGroupId();
    bool checkSegmentLastAndGroupFilled(uint64_t groupId, uint64_t segmentId, uint32_t numberSegmentsPerGroup);

    //obtain logical segment ids - todo should return first-last interval or maxSegmentId since segments per group are numbered 1..P
    bool getSegmentsByGroupId(
        		uint64_t groupId, bool useOffset,
    			uint64_t segmentId,  Buffer* outBuffer, uint32_t* length);

    friend class Log;

    DISALLOW_COPY_AND_ASSIGN(Streamlet);
};

} // namespace

#endif // !RAMCLOUD_STREAMLET_H
