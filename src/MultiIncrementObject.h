/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_MULTIINCREMENTOBJECT_H
#define RAMCLOUD_MULTIINCREMENTOBJECT_H

#include "Minimal.h"
#include "MultiOpObject.h"
#include "RejectRules.h"

namespace KerA {

/**
 * Objects of this class are used to pass parameters into \c multiIncrement
 * and for multiIncrement to return the new value and the status values
 * for conditional operations (if used).
 */
struct MultiIncrementObject : public MultiOpObject {
    /**
     * Summand to add to the existing object, which is either interpreted as
     * an integer or as a floating point value depending on which summand is
     * non-zero.
     */
    int64_t incrementInt64;
    double incrementDouble;

    /**
     * The RejectRules specify when conditional increments should be aborted.
     */
    const RejectRules* rejectRules;

    /**
     * The version number of the newly written object is returned here.
     */
    uint64_t version;

    /**
     * Value of the object after increasing
     */
    union {
        int64_t asInt64;
        double asDouble;
    } newValue;

    MultiIncrementObject(uint64_t tableId, const void* key, uint16_t keyLength,
                int64_t incrementInt64, double incrementDouble,
                const RejectRules* rejectRules = NULL)
        : MultiOpObject(tableId, key, keyLength)
        , incrementInt64(incrementInt64)
        , incrementDouble(incrementDouble)
        , rejectRules(rejectRules)
        , version()
        , newValue()
    {}

    MultiIncrementObject()
        : MultiOpObject()
        , incrementInt64()
        , incrementDouble()
        , rejectRules()
        , version()
        , newValue()
    {}

    MultiIncrementObject(const MultiIncrementObject& other)
        : MultiOpObject(other)
        , incrementInt64(other.incrementInt64)
        , incrementDouble(other.incrementDouble)
        , rejectRules(other.rejectRules)
        , version(other.version)
        , newValue(other.newValue)
    {}

    MultiIncrementObject& operator=(const MultiIncrementObject& other) {
        MultiOpObject::operator =(other);
        incrementInt64 = other.incrementInt64;
        incrementDouble = other.incrementDouble;
        rejectRules = other.rejectRules;
        version = other.version;
        newValue = other.newValue;
        return *this;
    }
};

}

#endif /* RAMCLOUD_MULTIINCREMENTOBJECT_H */
