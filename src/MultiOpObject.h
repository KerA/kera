/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_MULTIOPOBJECT_H
#define RAMCLOUD_MULTIOPOBJECT_H

#include "Status.h"

namespace KerA {

/**
 * The base class used to pass parameters into the MultiOp Framework. Any
 * multi operation xxxxx that uses the Framework should have its own
 * MultixxxxxObject that extends this object to describe its own parameters.
 */
struct MultiOpObject {
    /**
     * The table containing the desired object (return value from
     * a previous call to getTableId).
     */
    uint64_t tableId;

    /**
     * Variable length key that uniquely identifies the object within table.
     * It does not necessarily have to be null terminated like a string.
     * The caller is responsible for ensuring that this key remains valid
     * until the call is reaped/canceled.
     */
    const void* key;

    /**
     * Length of key, in bytes.
     */
    uint16_t keyLength;

    /**
     * 
     */
    uint64_t keyHash;

    /**
     * The status of read (either that the read succeeded, or the
     * error in case it didn't) is returned here.
     */
    Status status;

  PROTECTED:
    MultiOpObject(uint64_t tableId, const void* key, uint16_t keyLength)
        : tableId(tableId)
        , key(key)
        , keyLength(keyLength)
        , keyHash()
        , status()
    {}

    MultiOpObject(uint64_t tableId, uint64_t keyHash)
        : tableId(tableId)
        , key()
        , keyLength()
        , keyHash(keyHash)
        , status()
    {}

    MultiOpObject()
        : tableId()
        , key()
        , keyLength()
        , keyHash()
        , status()
    {}

    MultiOpObject(const MultiOpObject& other)
        : tableId(other.tableId)
        , key(other.key)
        , keyLength(other.keyLength)
        , keyHash(other.keyHash)
        , status(other.status)
    {};

    MultiOpObject& operator=(const MultiOpObject& other) {
        tableId = other.tableId;
        key = other.key;
        keyLength = other.keyLength;
        keyHash = other.keyHash;
        status = other.status;
        return *this;
    }

    virtual ~MultiOpObject() {};
};

}

#endif