/* Copyright 2018-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2012-2017 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef RAMCLOUD_GROUPMULTIRPCWRAPPER_H
#define RAMCLOUD_GROUPMULTIRPCWRAPPER_H

#include <memory>
#include <unordered_map>
#include <iostream>

#include "RamCloud.h"
#include "RpcWrapper.h"
#include "Transport.h"
#include "WireFormat.h"

namespace KerA {
/**
 * modified for stream multi-write KERAI
 * This class implements the client side framework for MasterService multi-ops.
 * It manages multiple concurrent RPCs, each requesting one or more objects
 * from a single server. The behavior of this class is similar to an
 * RpcWrapper, but it isn't an RpcWrapper subclass because it doesn't
 * correspond to a single RPC. Implementation-specific information about this
 * class can be found at the top of MultiOp.cc
 *
 * To add a new multi-operation, xxx, to this framework follow these steps:
 *   1) In Ramcloud.h, create a new MultixxxObject that extends MultiOpObject.
 *      This MultixxxObject will serve as a place to pass in parameters
 *      and store server responses.
 *
 *   2) In WireFormat.h, find struct MultiOp. Inside,
 *      2a) Create a new enum OpType to identify your operation.
 *      2b) Create a new struct xxxPart within MultiOp::Request
 *          and MultiOp::Response.
 *
 *   3) In xxx.cc and xxx.h, create a new Multixxx class that extends MultiOp.
 *      3a) Extend the constructor. Contract/Description located in MultiOp.cc
 *      3b) Implement the two PROTECTED virtual functions. Contract/Description
 *          located below.
 *
 *   4) In MasterService.cc, find the MasterService::multiOp, add your
 *      OpType to the switch statement and implement the server-side multi-op.
 *
 *   5) (optional) Add a high-level call in RamCloud.cc for Multixxx
 *
 * See MultiRead and MultiWrite for examples of multi operations using this
 * framework.
 */
class GroupMultiRpcWrapper {
  public:
    /// Constructor is PROTECTED; this class is meant to be abstracted.
    virtual ~GroupMultiRpcWrapper() {}

    void cancel();
    bool isReady();
    void wait();

    //allows reusing this object
    void reset(MultiOpGroupObject * const requests[], uint32_t numRequests);
    bool startRpcs();

    //KERAI params per request, passed to PartRpc#reqHdr
    //this to avoid re-creating these params for every Part object!
    //so every RPC batching 1000 requests will have 1000*18 bytes less!
    //and every RPC's part created on the master server will need less bytes
    //should boost performance 2x
    uint64_t tableId; //identifies streamId; using table to be consistent with RC's framework, although table==stream
    uint64_t producerId; //if != -1, use this to find an available group or create a new one

//    /**
//     * used to identify Master! KERAI - this is not related to object's key which is optionally given to each request
//     * Variable length key that uniquely identifies the object within table.
//     * It does not necessarily have to be null terminated like a string.
//     * The caller is responsible for ensuring that this key remains valid
//     * until the call is reaped/canceled.
//     */
//    const void* key;
//
//    /**
//     * Length of key, in bytes.
//     */
//    uint16_t keyLength;
//    uint64_t keyHash;

  PROTECTED:
    GroupMultiRpcWrapper(RamCloud* ramcloud, 
                WireFormat::MultiOpGroup::OpType type,
                MultiOpGroupObject * const requests[], uint32_t numRequests,
				        uint64_t tableId, uint64_t producerId);

    /// Encapsulates the state of a single RPC sent to a single server.
    class PartRpc : public RpcWrapper {
        friend class GroupMultiRpcWrapper;
      public:
        PartRpc(RamCloud* ramcloud, Transport::SessionRef session,
                WireFormat::MultiOpGroup::OpType type,
				uint64_t tableId, uint64_t producerId);
        virtual ~PartRpc() {}
        bool inProgress() {return getState() == IN_PROGRESS;
                                };
        bool isFinished() {return getState() == FINISHED;
                                };
        bool handleTransportError();
        void send();

        /// Overall client state information.
        RamCloud* ramcloud;

        /// Session that will be used to transmit the RPC.
        Transport::SessionRef session;

        /// Information about all of the objects that are being requested
        /// in this RPC. Note: the batch size used to be larger than this,
        /// but one of the biggest the performance benefits comes from
        /// issuing multiple RPCs that can be pipelined. Thus, it's
        /// better to have a smaller batch size so that pipelining kicks
        /// in for fewer total objects. If the total size of the multi-op
        /// request is huge (thousands?) then this approach is slightly
        /// slower (as of 12/2014) but smaller batch sizes are faster as
        /// long as the total number of objects in the multi-op request
        /// is a few hundred or less.
#ifdef TESTING
        static const uint32_t MAX_OBJECTS_PER_RPC = 16; //32768B per batch
#else
        static const uint32_t MAX_OBJECTS_PER_RPC = 16;
#endif
        MultiOpGroupObject* requests[MAX_OBJECTS_PER_RPC];

        /// Header for the RPC (used to update count as objects are added).
        WireFormat::MultiOpGroup::Request* reqHdr;

        DISALLOW_COPY_AND_ASSIGN(PartRpc);
    };

  PRIVATE:
    /// Buffer of requests for the same master.  Buffer is flushed at the
    /// end or when its size reaches MAX_OBJECTS_PER_RPC.
    typedef std::vector<MultiOpGroupObject*> SessionQueue;

    void dispatchRequest(MultiOpGroupObject* request,
                         Transport::SessionRef *session,
                         SessionQueue **queue);
    void finishRpc(GroupMultiRpcWrapper::PartRpc* rpc);
    void flushSessionQueue(Transport::SessionRef session,
                           SessionQueue *queue);

    /**
     * Adds a request back to a session buffer.
     *
     * \param request
     *      Pointer to the request to be read in.
     */
    inline void
    retryRequest(MultiOpGroupObject* request) {
        request->status = STATUS_RETRY;
        Transport::SessionRef session;
        SessionQueue *queue;
        dispatchRequest(request, &session, &queue);
    }

    /// A special Status value indicating than an RPC is underway but
    /// we haven't yet seen the response.
    static const Status UNDERWAY = Status(STATUS_MAX_VALUE+1);

    /// The maximum RPC length we will ever issue to a master in bytes. Requests
    /// that exceed this value will be issued in multiple RPCs.
    static const uint32_t maxRequestSize = Transport::MAX_RPC_LEN -
                                        sizeof(WireFormat::MultiOpGroup::Request);

    /// Overall client state information.
    RamCloud* ramcloud;

    /// The type of multi* operation that extends this super class; This
    /// is used to multiplex the MultiOp RPC
    WireFormat::MultiOpGroup::OpType opType;

    /// Copy of constructor argument containing information about
    /// desired objects.
    MultiOpGroupObject * const * requests;

    /// Copy constructor argument giving size of \c requests.
    uint32_t numRequests;

    /// Position pointer into requests array.  Request before are already
    /// assigned to a SessionQueue.  At the end of the operation, numDispatched
    /// is equal to numRequests.
    uint32_t numDispatched;

    /// An array holding the constituent RPCs that we are managing.
#ifdef TESTING
    static const uint32_t MAX_RPCS = 16;
#else
    static const uint32_t MAX_RPCS = 16;
#endif
    Tub<PartRpc> rpcs[MAX_RPCS];

    /// Represents a permutation of rpcs in such a way that all RPCs starting
    /// at startIndexIdleRpc are currently unused and can be filled and sent,
    /// whereas all RPCs before startIndexIdleRpc are underway.
    Tub<PartRpc> *ptrRpcs[MAX_RPCS];
    uint16_t startIndexIdleRpc;

    /// Set by \c cancel.
    bool canceled;

    /**
     * Uses the pointer of the reference as a hash key.  Used by sessionQueues.
     */
    struct HashSessionRef {
        size_t operator()(const Transport::SessionRef &session) const {
            return std::hash<Transport::Session *>()(session.get());
        }
    };

    /// Maps sessions to buffers of requests for these sessions.  Filled by
    /// dispatchRequest and packaged into RPCs by flushSessionQueue.
    typedef std::unordered_map<Transport::SessionRef,
                               std::shared_ptr<SessionQueue>,
                               HashSessionRef> SessionQueues;
    SessionQueues sessionQueues;

    /// Used for tests only. True = ignores buffer size checking in finishRpc.
    /// Needed since test responses don't put anything in the response buffer.
    bool test_ignoreBufferOverflow;

    // TOOD(syang0) These should be abstracted into sub class.
    virtual void appendRequest(MultiOpGroupObject* request, Buffer* buf)=0;
    virtual bool readResponse(MultiOpGroupObject* request, Buffer* response,
                                 uint32_t* respOffset)=0;

    DISALLOW_COPY_AND_ASSIGN(GroupMultiRpcWrapper);
};

} // end KerA

#endif  // GROUPMULTIRPCWRAPPER_H
