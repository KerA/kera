/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_MULTIREADOBJECT_H
#define RAMCLOUD_MULTIREADOBJECT_H

#include "Minimal.h"
#include "MultiOpObject.h"
#include "ObjectBuffer.h"
#include "RejectRules.h"
#include "Tub.h"

namespace KerA {

/**
 * Objects of this class are used to pass parameters into \c multiRead
 * and for multiRead to return result values.
 */
struct MultiReadObject : public MultiOpObject {
    /**
     * If the read for this object was successful, the Tub<ObjectBuffer>
     * will hold the contents of the desired object. If not, it will
     * not be initialized, giving "false" when the buffer is tested.
     */
    Tub<ObjectBuffer>* value;

    /**
     * The RejectRules specify when conditional reads should be aborted.
     */
    const RejectRules* rejectRules;

    /**
     * The version number of the object is returned here.
     */
    uint64_t version;

    MultiReadObject(uint64_t tableId, const void* key, uint16_t keyLength,
            Tub<ObjectBuffer>* value, const RejectRules* rejectRules = NULL)
        : MultiOpObject(tableId, key, keyLength)
        , value(value)
        , rejectRules(rejectRules)
        , version()
    {}

    MultiReadObject()
        : value()
        , rejectRules()
        , version()
    {}

    MultiReadObject(const MultiReadObject& other)
        : MultiOpObject(other)
        , value(other.value)
        , rejectRules(other.rejectRules)
        , version(other.version)
    {}

    MultiReadObject& operator=(const MultiReadObject& other) {
        MultiOpObject::operator =(other);
        value = other.value;
        rejectRules = other.rejectRules;
        version = other.version;
        return *this;
    }
};

}

#endif