/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2010-2017 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef RAMCLOUD_RAMCLOUD_H
#define RAMCLOUD_RAMCLOUD_H

#include "CoordinatorRpcWrapper.h"
#include "IndexRpcWrapper.h"
#include "LinearizableObjectRpcWrapper.h"
#include "ObjectBuffer.h"
#include "ObjectRpcWrapper.h"
#include "OptionParser.h"
#include "ServerMetrics.h"

#include "MultiIncrementObject.h"
#include "MultiReadObject.h"
#include "MultiReadGroupObject.h"
#include "MultiRemoveObject.h"
#include "MultiWriteObject.h"
#include "MultiWriteGroupObject.h"
#include "MultiWriteGroupBatchObject.h"

#include "LogMetrics.pb.h"
#include "ServerConfig.pb.h"
#include "ServerStatistics.pb.h"

namespace KerA {
class ClientLeaseAgent;
class ClientTransactionManager;
class ObjectFinder;
class RpcTracker;

/**
 * The RamCloud class provides the primary interface used by applications to
 * access a RAMCloud cluster.
 *
 * Each RamCloud object provides access to a particular RAMCloud cluster;
 * all of the RAMCloud RPC requests appear as methods on this object.
 *
 * In multi-threaded clients there must be a separate RamCloud object for
 * each thread; as of 5/2012 these objects are not thread-safe.
 */
class RamCloud {
  public:
    void coordSplitAndMigrateIndexlet(
            ServerId newOwner, uint64_t tableId, uint8_t indexId,
            const void* splitKey, KeyLength splitKeyLength);
    uint64_t createTable(const char* name, uint32_t serverSpan = 1);
    uint64_t createTable(const char* name, uint32_t serverSpan, 
                         uint32_t streamletSpan, uint32_t countLogs = 1);
    void dropTable(const char* name);
    void createIndex(uint64_t tableId, uint8_t indexId, uint8_t indexType,
            uint8_t numIndexlets = 1);
    void dropIndex(uint64_t tableId, uint8_t indexId);
    void echo(const char* serviceLocator, const void* message, uint32_t length,
         uint32_t echoLength, Buffer* echo);
    uint64_t enumerateTable(uint64_t tableId, bool keysOnly,
         uint64_t tabletFirstHash, Buffer& state, Buffer& objects);
    void getLogMetrics(const char* serviceLocator,
            ProtoBuf::LogMetrics& logMetrics);
    ServerMetrics getMetrics(uint64_t tableId, const void* key,
            uint16_t keyLength);
    ServerMetrics getMetrics(const char* serviceLocator);
    void getRuntimeOption(const char* option, Buffer* value);
    uint32_t getLocalStreamlets(uint64_t streamId, uint64_t serverId, Buffer* response);
    void getServerConfig(const char* serviceLocator,
            ProtoBuf::ServerConfig& serverConfig);
    void getServerStatistics(const char* serviceLocator,
            ProtoBuf::ServerStatistics& serverStats);
    string* getServiceLocator();
    uint64_t getTableId(const char* name);
    uint64_t getNextAvailableGroupId(uint64_t tableId, uint32_t streamletId, uint64_t readerId, uint64_t keyHash);
	void getNextAvailableGroupSegmentIds(uint64_t tableId,
			uint32_t streamletId, uint64_t readerId, uint64_t keyHash,
			uint32_t nActiveGroups, Buffer* response);
    uint32_t getSegmentsByGroupId(uint64_t tableId, uint32_t streamletId, uint64_t keyHash, uint64_t groupId,
    		bool useOffset, uint64_t segmentIdOffset, uint32_t offset, uint32_t position, Buffer* response);
    void getGroupOffset(uint64_t tableId, uint64_t keyHash,
			uint64_t readerId, uint64_t groupId, Buffer* response);
    bool updateGroupOffset(uint64_t tableId, uint64_t keyHash, uint32_t streamletId,
    			uint64_t readerId, uint64_t groupId, bool useOldOffset,
    			uint64_t oldSegmentId, uint32_t oldOffset, uint32_t oldPosition, uint16_t oldObjectKeyLength,
    			uint64_t segmentId, uint32_t offset, uint32_t position, uint16_t objectKeyLength, Buffer* response);
    double incrementDouble(uint64_t tableId,
            const void* key, uint16_t keyLength,
            double incrementValue, const RejectRules* rejectRules = NULL,
            uint64_t* version = NULL);
    int64_t incrementInt64(uint64_t tableId,
            const void* key, uint16_t keyLength,
            int64_t incrementValue, const RejectRules* rejectRules = NULL,
            uint64_t* version = NULL);
    uint32_t readHashes(uint64_t tableId, uint32_t numHashes, Buffer* pKHashes,
            Buffer* response, uint32_t* numObjects);
    void indexServerControl(uint64_t tableId, uint8_t indexId,
            const void* key, uint16_t keyLength,
            WireFormat::ControlOp controlOp,
            const void* inputData, uint32_t inputLength, Buffer* outputData);
    void lookupIndexKeys(uint64_t tableId, uint8_t indexId,
            const void* firstKey, uint16_t firstKeyLength,
            uint64_t firstAllowedKeyHash,
            const void* lastKey, uint16_t lastKeyLength,
            uint32_t maxNumHashes,
            Buffer* responseBuffer,
            uint32_t* numHashes, uint16_t* nextKeyLength,
            uint64_t* nextKeyHash);
    void migrateTablet(uint64_t tableId, uint64_t firstKeyHash,
            uint64_t lastKeyHash, ServerId newOwnerMasterId);
    void multiIncrement(MultiIncrementObject* requests[], uint32_t numRequests);
    void multiRead(MultiReadObject* requests[], uint32_t numRequests);
    void multiReadGroup(MultiReadGroupObject* requests[], uint32_t numRequests, uint32_t maxStreamletFetchBytes,
        Buffer *rpcResponses[], uint32_t numResponses, uint64_t totalObjectsSize, uint64_t readerId, bool useSharedPlasma);
    void multiRemove(MultiRemoveObject* requests[], uint32_t numRequests);
    void multiWrite(MultiWriteObject* requests[], uint32_t numRequests);
    void multiWriteGroup(MultiWriteGroupObject* requests[], uint32_t numRequests,
        uint64_t tableId, uint64_t producerId, bool useNextGroup, bool allowSharedGroups,
        const void* key, uint16_t keyLength);
    void multiWriteGroupBatch(MultiWriteGroupBatchObject* requests[], uint32_t numRequests,
            uint64_t tableId, uint64_t producerId);
    void objectServerControl(uint64_t tableId, const void* key,
            uint16_t keyLength, WireFormat::ControlOp controlOp,
            const void* inputData = NULL, uint32_t inputLength = 0,
            Buffer* outputData = NULL);
    void read(uint64_t tableId, const void* key, uint16_t keyLength,
            Buffer* value, const RejectRules* rejectRules = NULL,
            uint64_t* version = NULL);
    void readKeysAndValue(uint64_t tableId, const void* key, uint16_t keyLength,
            ObjectBuffer* value, const RejectRules* rejectRules = NULL,
            uint64_t* version = NULL);
    void remove(uint64_t tableId, const void* key, uint16_t keyLength,
            const RejectRules* rejectRules = NULL, uint64_t* version = NULL);
    void serverControlAll(WireFormat::ControlOp controlOp,
            const void* inputData = NULL, uint32_t inputLength = 0,
            Buffer* outputData = NULL);
    void logMessageAll(LogLevel level, const char* fmt, ...)
        __attribute__ ((format (gnu_printf, 3, 4)));
    void splitTablet(const char* name, uint64_t splitKeyHash);
    void testingFill(uint64_t tableId, const void* key, uint16_t keyLength,
            uint32_t numObjects, uint32_t objectSize);
    uint64_t testingGetServerId(uint64_t tableId, const void* key,
            uint16_t keyLength);
    string testingGetServiceLocator(uint64_t tableId, const void* key,
            uint16_t keyLength);
    void testingKill(uint64_t tableId, const void* key, uint16_t keyLength);
    void setRuntimeOption(const char* option, const char* value);
    void testingWaitForAllTabletsNormal(uint64_t tableId,
            uint64_t timeoutNs = ~0lu);
    void write(uint64_t tableId, const void* key, uint16_t keyLength,
            const void* buf, uint32_t length,
            const RejectRules* rejectRules = NULL, uint64_t* version = NULL,
            bool async = false);
    void write(uint64_t tableId, const void* key, uint16_t keyLength,
            const char* value, const RejectRules* rejectRules = NULL,
            uint64_t* version = NULL, bool async = false);
    void write(uint64_t tableId, uint8_t numKeys, KeyInfo *keyInfo,
            const void* buf, uint32_t length,
            const RejectRules* rejectRules = NULL, uint64_t* version = NULL,
            bool async = false);
    void write(uint64_t tableId, uint8_t numKeys, KeyInfo *keyInfo,
            const char* value, const RejectRules* rejectRules = NULL,
            uint64_t* version = NULL, bool async = false);

    void poll();
    explicit RamCloud(CommandLineOptions* options);
    explicit RamCloud(Context* context);
    explicit RamCloud(const char* serviceLocator,
            const char* clusterName = "main");
    explicit RamCloud(Context* context, const char* serviceLocator,
            const char* clusterName = "main");
    virtual ~RamCloud();

  PRIVATE:
    /**
     * Service locator for the cluster coordinator.
     */
    string coordinatorLocator;

    /**
     * Usually, RamCloud objects create a new context in which to run. This is
     * the location where that context is stored. This is dynamically allocated
     * memory, and must be freed (NULL means not allocated yet).
     */
    Context* realClientContext;

  public:
    /**
     * This usually refers to realClientContext. For testing purposes and
     * clients that want to provide their own context that they've mucked with,
     * this refers to an externally defined context.
     */
    Context* clientContext;

  public: // public for now to make administrative calls from clients

    // See "Header Minimization" in designNotes for info on why these
    // are pointers.
    ClientLeaseAgent *clientLeaseAgent;
    RpcTracker *rpcTracker;
    ClientTransactionManager *transactionManager;

  private:
    DISALLOW_COPY_AND_ASSIGN(RamCloud);
};

/**
 * Encapsulates the state of a RamCloud::coordSplitAndMigrateIndexlet operation,
 * allowing it to execute asynchronously.
 */
class CoordSplitAndMigrateIndexletRpc : public CoordinatorRpcWrapper {
  public:
    CoordSplitAndMigrateIndexletRpc(RamCloud* ramcloud,
            ServerId newOwner, uint64_t tableId, uint8_t indexId,
            const void* splitKey, KeyLength splitKeyLength);
    ~CoordSplitAndMigrateIndexletRpc() {}
    /// \copydoc RpcWrapper::docForWait
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(CoordSplitAndMigrateIndexletRpc);
};

/**
 * Encapsulates the state of a RamCloud::createTable operation,
 * allowing it to execute asynchronously.
 */
class CreateTableRpc : public CoordinatorRpcWrapper {
  public:
    CreateTableRpc(RamCloud* ramcloud, const char* name,
            uint32_t serverSpan = 1, uint32_t streamletSpan = 1, 
            uint32_t countLogs = 1);
    ~CreateTableRpc() {}
    uint64_t wait();

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(CreateTableRpc);
};

/**
 * Encapsulates the state of a RamCloud::dropTable operation,
 * allowing it to execute asynchronously.
 */
class DropTableRpc : public CoordinatorRpcWrapper {
  public:
    DropTableRpc(RamCloud* ramcloud, const char* name);
    ~DropTableRpc() {}
    /// \copydoc RpcWrapper::docForWait
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(DropTableRpc);
};

/**
 * Encapsulates the state of a RamCloud::createIndex operation,
 * allowing it to execute asynchronously.
 */
class CreateIndexRpc : public CoordinatorRpcWrapper {
  public:
    CreateIndexRpc(RamCloud* ramcloud, uint64_t tableId, uint8_t indexId,
              uint8_t indexType, uint8_t numIndexlets = 1);
    ~CreateIndexRpc() {}
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(CreateIndexRpc);
};

/**
 * Encapsulates the state of a RamCloud::dropIndex operation,
 * allowing it to execute asynchronously.
 */
class DropIndexRpc : public CoordinatorRpcWrapper {
  public:
    DropIndexRpc(RamCloud* ramcloud, uint64_t tableId, uint8_t indexId);
    ~DropIndexRpc() {}
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(DropIndexRpc);
};

/**
 * Encapsulates the state of a RamCloud::echo operation,
 * allowing it to execute asynchronously.
 */
class EchoRpc : public RpcWrapper {
  public:
    EchoRpc(RamCloud* ramcloud, const char* serviceLocator,
            const void* message, uint32_t length, uint32_t echoLength,
            Buffer* echo);
    ~EchoRpc() {}
    virtual void completed();
    uint64_t getCompletionTime();
    /// \copydoc RpcWrapper::docForWait
    void wait();

  PRIVATE:
    RamCloud* ramcloud;
    /// TSC value when the RPC is sent.
    uint64_t startTime;
    /// TSC value when the RPC is completed.
    uint64_t endTime;
    DISALLOW_COPY_AND_ASSIGN(EchoRpc);
};

/**
 * Encapsulates the state of a RamCloud::enumerateTable
 * request, allowing it to execute asynchronously.
 */
class EnumerateTableRpc : public ObjectRpcWrapper {
  public:
    EnumerateTableRpc(RamCloud* ramcloud, uint64_t tableId, bool keysOnly,
            uint64_t tabletFirstHash, Buffer& iter, Buffer& objects);
    ~EnumerateTableRpc() {}
    uint64_t wait(Buffer& nextIter);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(EnumerateTableRpc);
};

/**
 * Encapsulates the state of a RamCloud::testingFill operation,
 * allowing it to execute asynchronously.
 */
class FillWithTestDataRpc : public ObjectRpcWrapper {
  public:
    FillWithTestDataRpc(RamCloud* ramcloud, uint64_t tableId, const void* key,
            uint16_t keyLength, uint32_t numObjects, uint32_t objectSize);
    ~FillWithTestDataRpc() {}
    /// \copydoc RpcWrapper::docForWait
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(FillWithTestDataRpc);
};

/**
 * Encapsulates the state of a RamCloud::getLogMetrics operation,
 * allowing it to execute asynchronously.
 */
class GetLogMetricsRpc: public RpcWrapper {
  public:
    GetLogMetricsRpc(RamCloud* ramcloud, const char* serviceLocator);
    ~GetLogMetricsRpc() {}
    void wait(ProtoBuf::LogMetrics& logMetrics);

  PRIVATE:
    RamCloud* ramcloud;
    DISALLOW_COPY_AND_ASSIGN(GetLogMetricsRpc);
};

/**
 * Encapsulates the state of a RamCloud::getMetrics operation,
 * allowing it to execute asynchronously.
 */
class GetMetricsRpc : public ObjectRpcWrapper {
  public:
    GetMetricsRpc(RamCloud* ramcloud, uint64_t tableId,
            const void* key, uint16_t keyLength);
    ~GetMetricsRpc() {}
    ServerMetrics wait();

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(GetMetricsRpc);
};

/**
 * Encapsulates the state of a RamCloud::getMetrics operation that
 * uses a service locator to identify the server rather than an object.
 */
class GetMetricsLocatorRpc : public RpcWrapper {
  public:
    GetMetricsLocatorRpc(RamCloud* ramcloud, const char* serviceLocator);
    ~GetMetricsLocatorRpc() {}
    ServerMetrics wait();

  PRIVATE:
    RamCloud* ramcloud;
    DISALLOW_COPY_AND_ASSIGN(GetMetricsLocatorRpc);
};

/**
 * Encapsulate the state of RamCloud:: getRuntimeOption operation
 * allowing to execute asynchronously.
 */
class GetRuntimeOptionRpc : public CoordinatorRpcWrapper{
  public:
    GetRuntimeOptionRpc(RamCloud* ramcloud, const char* option,
            Buffer* value);
    ~GetRuntimeOptionRpc() {}
    void wait();
  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(GetRuntimeOptionRpc);
};

class GetLocalStreamletsRpc : public CoordinatorRpcWrapper{
  public:
	GetLocalStreamletsRpc(RamCloud* ramcloud, uint64_t streamId,
			uint64_t serverId, Buffer* response);
    ~GetLocalStreamletsRpc() {}
    uint32_t wait();
  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(GetLocalStreamletsRpc);
};

/**
 * Encapsulates the state of a RamCloud::getServerConfig operation,
 * allowing it to execute asynchronously.
 */
class GetServerConfigRpc : public RpcWrapper {
  public:
    GetServerConfigRpc(RamCloud* ramcloud, const char* serviceLocator);
    ~GetServerConfigRpc() {}
    void wait(ProtoBuf::ServerConfig& serverConfig);

  PRIVATE:
    RamCloud* ramcloud;
    DISALLOW_COPY_AND_ASSIGN(GetServerConfigRpc);
};

/**
 * Encapsulates the state of a RamCloud::getServerStatistics operation,
 * allowing it to execute asynchronously.
 */
class GetServerStatisticsRpc : public RpcWrapper {
  public:
    GetServerStatisticsRpc(RamCloud* ramcloud, const char* serviceLocator);
    ~GetServerStatisticsRpc() {}
    void wait(ProtoBuf::ServerStatistics& serverStats);

  PRIVATE:
    RamCloud* ramcloud;
    DISALLOW_COPY_AND_ASSIGN(GetServerStatisticsRpc);
};

/**
 * Encapsulates the state of a RamCloud::getTableId operation,
 * allowing it to execute asynchronously.
 */
class GetTableIdRpc : public CoordinatorRpcWrapper {
  public:
    GetTableIdRpc(RamCloud* ramcloud, const char* name);
    ~GetTableIdRpc() {}
    uint64_t wait();

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(GetTableIdRpc);
};

class GetNextAvailableGroupIdRpc : public ObjectRpcWrapper {
  public:
	GetNextAvailableGroupIdRpc(RamCloud* ramcloud, uint64_t tableId, uint32_t streamletId, uint64_t readerId, uint64_t keyHash);
    ~GetNextAvailableGroupIdRpc() {}
    uint64_t wait();

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(GetNextAvailableGroupIdRpc);
};

class GetNextAvailableGroupSegmentIdRpc : public ObjectRpcWrapper {
  public:
	GetNextAvailableGroupSegmentIdRpc(RamCloud* ramcloud, uint64_t tableId,
			uint32_t streamletId, uint64_t readerId, uint64_t keyHash,
			uint32_t nActiveGroups, Buffer* response);
    ~GetNextAvailableGroupSegmentIdRpc() {}
    void wait();

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(GetNextAvailableGroupSegmentIdRpc);
};

class GetSegmentsByGroupIdRpc : public ObjectRpcWrapper {
  public:
	GetSegmentsByGroupIdRpc(RamCloud* ramcloud, uint64_t streamId, uint32_t streamletId, uint64_t groupId, uint64_t keyHash,
			bool useOffset, uint64_t segmentIdOffset, uint32_t offset, uint32_t position, Buffer* response);
    ~GetSegmentsByGroupIdRpc() {}
    uint32_t wait();

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(GetSegmentsByGroupIdRpc);
};

class GetGroupOffsetRpc : public ObjectRpcWrapper {
  public:
	GetGroupOffsetRpc(RamCloud* ramcloud, uint64_t tableId, uint64_t keyHash,
			uint64_t readerId, uint64_t groupId, Buffer* response);
    ~GetGroupOffsetRpc() {}
    void wait();

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(GetGroupOffsetRpc);
};

class UpdateGroupOffsetRpc : public ObjectRpcWrapper {
  public:
	UpdateGroupOffsetRpc(RamCloud* ramcloud, uint64_t tableId, uint64_t keyHash, uint32_t streamletId,
			uint64_t readerId, uint64_t groupId, bool useOldOffset,
			uint64_t oldSegmentId, uint32_t oldOffset, uint32_t oldPosition, uint16_t oldObjectKeyLength,
			uint64_t segmentId, uint32_t offset, uint32_t position, uint16_t objectKeyLength, Buffer* response);
    ~UpdateGroupOffsetRpc() {}
    bool wait();

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(UpdateGroupOffsetRpc);
};

/**
 * Encapsulates the state of a RamCloud::incrementDouble operation,
 * allowing it to execute asynchronously.
 */
class IncrementDoubleRpc : public LinearizableObjectRpcWrapper {
  public:
    IncrementDoubleRpc(RamCloud* ramcloud, uint64_t tableId, const void* key,
            uint16_t keyLength, double incrementValue,
            const RejectRules* rejectRules = NULL);
    ~IncrementDoubleRpc() {}
    double wait(uint64_t* version = NULL);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(IncrementDoubleRpc);
};

/**
 * Encapsulates the state of a RamCloud::incrementInt64 operation,
 * allowing it to execute asynchronously.
 */
class IncrementInt64Rpc : public LinearizableObjectRpcWrapper {
  public:
    IncrementInt64Rpc(RamCloud* ramcloud, uint64_t tableId, const void* key,
            uint16_t keyLength, int64_t incrementValue,
            const RejectRules* rejectRules = NULL);
    ~IncrementInt64Rpc() {}
    int64_t wait(uint64_t* version = NULL);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(IncrementInt64Rpc);
};

/**
 * Encapsulates the state of a RamCloud::readHashes operation,
 * allowing it to execute asynchronously.
 */
class ReadHashesRpc : public ObjectRpcWrapper {
  public:
    ReadHashesRpc(RamCloud* ramcloud, uint64_t tableId, uint32_t numHashes,
            Buffer* pKHashes, Buffer* response);
    ~ReadHashesRpc() {}
    /// \copydoc RpcWrapper::docForWait
    uint32_t wait(uint32_t* numObjects);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(ReadHashesRpc);
};

/**
 * Encapsulates the state of a RamCloud::indexServerControl operation,
 * allowing it to execute asynchronously.
 */
class IndexServerControlRpc : public IndexRpcWrapper {
  public:
    IndexServerControlRpc(RamCloud* ramcloud, uint64_t tableId, uint8_t indexId,
            const void* key, uint16_t keyLength,
            WireFormat::ControlOp controlOp,
            const void* inputData, uint32_t inputLength, Buffer* outputData);
    ~IndexServerControlRpc() {}
    void wait();
  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(IndexServerControlRpc);
};

/**
 * Encapsulates the state of a RamCloud::testingKill operation.  This
 * RPC should never be waited for!!  If you do, it will track the object
 * around the cluster, killing the server currently holding the object,
 * waiting for the object to reappear on a different server, then killing
 * that server, and so on forever.
 */
class KillRpc : public ObjectRpcWrapper {
  public:
    KillRpc(RamCloud* ramcloud, uint64_t tableId, const void* key,
            uint16_t keyLength);
    ~KillRpc() {}
    /// \copydoc RpcWrapper::docForWait
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(KillRpc);
};



/**
 * Encapsulates the state of a RamCloud::lookupIndexKeys operation,
 * allowing it to execute asynchronously.
 */
class LookupIndexKeysRpc : public IndexRpcWrapper {
  public:
    LookupIndexKeysRpc(RamCloud* ramcloud, uint64_t tableId, uint8_t indexId,
            const void* firstKey, uint16_t firstKeyLength,
            uint64_t firstAllowedKeyHash,
            const void* lastKey, uint16_t lastKeyLength,
            uint32_t maxNumHashes, Buffer* responseBuffer);
    ~LookupIndexKeysRpc() {}

    void handleIndexDoesntExist();
    void wait(uint32_t* numHashes, uint16_t* nextKeyLength,
            uint64_t* nextKeyHash);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(LookupIndexKeysRpc);
};

/**
 * Encapsulates the state of a RamCloud::migrateTablet operation,
 * allowing it to execute asynchronously.
 */
class MigrateTabletRpc : public ObjectRpcWrapper {
  public:
    MigrateTabletRpc(RamCloud* ramcloud, uint64_t tableId,
            uint64_t firstKeyHash, uint64_t lastKeyHash,
            ServerId newMasterOwnerId);
    ~MigrateTabletRpc() {}
    /// \copydoc RpcWrapper::docForWait
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(MigrateTabletRpc);
};

/**
 * Encapsulates the state of a RamCloud::read operation,
 * allowing it to execute asynchronously.
 */
class ReadRpc : public ObjectRpcWrapper {
  public:
    ReadRpc(RamCloud* ramcloud, uint64_t tableId, const void* key,
            uint16_t keyLength, Buffer* value,
            const RejectRules* rejectRules = NULL);
    ~ReadRpc() {}
    void wait(uint64_t* version = NULL);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(ReadRpc);
};

/**
 * Encapsulates the state of a RamCloud::read operation,
 * allowing it to execute asynchronously. The difference from
 * ReadRpc is in the contents of the returned buffer.
 */
class ReadKeysAndValueRpc : public ObjectRpcWrapper {
  public:
    ReadKeysAndValueRpc(RamCloud* ramcloud, uint64_t tableId, const void* key,
            uint16_t keyLength, ObjectBuffer* value,
            const RejectRules* rejectRules = NULL);
    ~ReadKeysAndValueRpc() {}
    void wait(uint64_t* version = NULL);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(ReadKeysAndValueRpc);
};

/**
 * Encapsulates the state of a RamCloud::remove operation,
 * allowing it to execute asynchronously.
 */
class RemoveRpc : public LinearizableObjectRpcWrapper {
  public:
    RemoveRpc(RamCloud* ramcloud, uint64_t tableId, const void* key,
            uint16_t keyLength, const RejectRules* rejectRules = NULL);
    ~RemoveRpc() {}
    void wait(uint64_t* version = NULL);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(RemoveRpc);
};

/**
 * Encapsulates the state of a RamCloud::objectServerControl operation,
 * allowing it to execute asynchronously.
 */
class ObjectServerControlRpc : public ObjectRpcWrapper {
  public:
    ObjectServerControlRpc(RamCloud* ramcloud, uint64_t tableId,
        const void* key, uint16_t keyLength, WireFormat::ControlOp controlOp,
        const void* inputData = NULL, uint32_t inputLength = 0,
        Buffer* outputData = NULL);
    ~ObjectServerControlRpc() {}
    void wait();
  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(ObjectServerControlRpc);
};

/**
 * Encapsulates the state of a RamCloud::setRuntimeOption operation,
 * allowing it to execute asynchronously.
 */
class SetRuntimeOptionRpc : public CoordinatorRpcWrapper {
  public:
    SetRuntimeOptionRpc(RamCloud* ramcloud, const char* option,
            const char* value);
    ~SetRuntimeOptionRpc() {}
    /// \copydoc RpcWrapper::docForWait
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(SetRuntimeOptionRpc);
};

/**
 * Encapsulates the state of a RamCloud::splitTablet operation,
 * allowing it to execute asynchronously.
 */
class SplitTabletRpc : public CoordinatorRpcWrapper {
  public:
    SplitTabletRpc(RamCloud* ramcloud, const char* name,
            uint64_t splitKeyHash);
    ~SplitTabletRpc() {}
    /// \copydoc RpcWrapper::docForWait
    void wait() {simpleWait(context);}

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(SplitTabletRpc);
};

/**
 * Encapsulates the state of a RamCloud::write operation,
 * allowing it to execute asynchronously.
 */
class WriteRpc : public LinearizableObjectRpcWrapper {
  public:
    WriteRpc(RamCloud* ramcloud, uint64_t tableId, const void* key,
            uint16_t keyLength, const void* buf, uint32_t length,
            const RejectRules* rejectRules = NULL, bool async = false);
    // this constructor will be used when the object has multiple keys
    WriteRpc(RamCloud* ramcloud, uint64_t tableId,
            uint8_t numKeys, KeyInfo *keyInfo,
            const void* buf, uint32_t length,
            const RejectRules* rejectRules = NULL, bool async = false);
    ~WriteRpc() {}
    void wait(uint64_t* version = NULL);

  PRIVATE:
    DISALLOW_COPY_AND_ASSIGN(WriteRpc);
};

} // namespace KerA

#endif // RAMCLOUD_RAMCLOUD_H
