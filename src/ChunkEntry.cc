/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ChunkEntry.h"

namespace KerA {

ChunkEntry::ChunkEntry(uint64_t producerId, uint64_t streamId) :
		producerId(producerId), countEntries(0), chunklength(0), //lengthLastEntry(0),
		streamId(streamId), streamletId(0), groupId(-1UL), segmentId(-1UL), header() {
}

void ChunkEntry::init(ChunkEntry* orig) {
	if(orig != NULL) {
		this->header.checksum = orig->getChecksum();
		this->producerId = orig->producerId;
		this->countEntries = orig->countEntries;
		this->chunklength = orig->chunklength;
//		this->lengthLastEntry = orig->lengthLastEntry;
		this->streamId = orig->streamId;
		this->streamletId = orig->streamletId;
		this->groupId = orig->groupId;
		this->segmentId = orig->segmentId;
	}
}

} // namespace KerA
