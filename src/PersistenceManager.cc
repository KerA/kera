/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "PersistenceManager.h"
#include "Buffer.h"
#include "ClientException.h"
#include "Cycles.h"
#include "InMemoryStorage.h"
#include "PerfStats.h"
#include "ServerConfig.h"
#include "ShortMacros.h"
#include "MultiFileStorage.h"
#include "Status.h"

namespace KerA {


/**
 * Create a PersistenceManager.
 *
 * \param context
 *      Overall information about the RAMCloud server. The new service
 *      will be registered in this context.
 * \param config
 *      Settings for this instance. The caller guarantees that config will
 *      exist for the duration of this PersistenceManager lifetime.
 */
PersistenceManager::PersistenceManager(Context* context,
                             const ServerConfig* config)
    : context(context)
    , mutex()
    , config(config)
    , formerServerId()
    , serverId()
    , storage()
    , frames()
    , segmentSize(config->segmentSize)
    , readSpeed()
    , bytesWritten(0)
    , initCalled(false)
    , testingSkipCallerIdCheck(false)
{
	//now using config->backup settings; later create a persistenceManager settings todo
    if (config->backup.inMemory) {
        storage.reset(new InMemoryStorage(config->segmentSize,
                                          config->backup.numSegmentFrames,
                                          config->backup.writeRateLimit));
    } else {
        size_t maxWriteBuffers = config->backup.maxNonVolatileBuffers;
        if (maxWriteBuffers == 0) {
            // Allow unlimited write buffers; this is risky, because an
            // overloaded backup process may buffer enough to exhaust
            // the server's free memory, in which case the Linux OOM killer
            // will kill the process or, even worse, there will be paging.
            maxWriteBuffers = config->backup.numSegmentFrames;
        }

        storage.reset(new MultiFileStorage(config->segmentSize,
                                           config->backup.numSegmentFrames,
                                           config->backup.writeRateLimit,
                                           maxWriteBuffers,
                                           config->backup.file.c_str(), //todo replace this with persistenceManager.file
                                           O_DIRECT | O_SYNC));
    }
    if (storage->getMetadataSize() < sizeof(BackupReplicaMetadata))
        DIE("Storage metadata block too small to hold BackupReplicaMetadata");

    benchmark();

    BackupStorage::Superblock superblock = storage->loadSuperblock();
    if (config->clusterName == "__unnamed__") {
        LOG(NOTICE, "Cluster '__unnamed__'; ignoring existing backup storage. "
            "Any replicas stored will not be reusable by future backups. "
            "Specify clusterName for persistence across backup restarts.");
    } else {
        LOG(NOTICE, "Backup storing replicas with clusterName '%s'. Future "
            "backups must be restarted with the same clusterName for replicas "
            "stored on this backup to be reused.", config->clusterName.c_str());
        if (config->clusterName == superblock.getClusterName()) {
            LOG(NOTICE, "Replicas stored on disk have matching clusterName "
                "('%s'). Scanning storage to find all replicas and to make "
                "them available to recoveries.",
                superblock.getClusterName());
            formerServerId = superblock.getServerId();
            LOG(NOTICE, "Will enlist as a replacement for formerly crashed "
                "server %s which left replicas behind on disk",
                formerServerId.toString().c_str());
            //todo read from storage
        } else {
            LOG(NOTICE, "Replicas stored on disk have a different clusterName "
                "('%s'). Scribbling storage to ensure any stale replicas left "
                "behind by old backups aren't used by future backups",
                superblock.getClusterName());
            storage->fry();
        }
    }
}

PersistenceManager::~PersistenceManager()
{
}

/**
 * Perform an initial benchmark of the storage system. This is later passed to
 * the coordinator during enlistment so that masters can intelligently select
 * backups.
 *
 * Benchmark is not run during unit if tests config.backup.mockSpeed is not 0.
 * It is not run as part of init() because that method must be fast (right now,
 * as soon as enlistment completes runs for some service, clients can start
 * accessing the service).
 */
void
PersistenceManager::benchmark()
{
    if (config->backup.mockSpeed == 0) {
        auto strategy = static_cast<BackupStrategy>(config->backup.strategy);
        readSpeed = storage->benchmark(strategy);
    } else {
        readSpeed = config->backup.mockSpeed;
    }
}

/**
 * Perform once-only initialization for the backup service after having
 * enlisted the process with the coordinator.
 *
 * Any actions performed here must not block the process or dispatch thread,
 * otherwise the server may be timed out and declared failed by the coordinator.
 */
void
PersistenceManager::initOnceEnlisted(ServerId assignedServerId)
{
    assert(!initCalled);

    serverId = assignedServerId;

    LOG(NOTICE, "pm My server ID is %s", serverId.toString().c_str());
    if (metrics->serverId == 0) {
        metrics->serverId = *serverId;
    }

    storage->resetSuperblock(serverId, config->clusterName);
    LOG(NOTICE, "PersistenceManager %s will store replicas under cluster name '%s'",
        serverId.toString().c_str(), config->clusterName.c_str());
    initCalled = true;
}

/**
 * store given source/segment
 */
bool
PersistenceManager::writeSegment(
		const WireFormat::BackupWrite::Request* reqHdr, Buffer& source) {
    Lock _(mutex);

    if (!initCalled) {
//        LOG(WARNING, "%s invoked before initialization complete; "
//                "returning STATUS_RETRY", WireFormat::BackupWrite::opcode);
        throw RetryException(HERE, 100, 100,
                "backup service not yet initialized");
    }
    CycleCounter<RawMetric> serviceTicks(&metrics->backup.serviceTicks);

    ServerId masterId(reqHdr->masterId);
    uint64_t segmentId = reqHdr->segmentId;
    uint64_t logId = reqHdr->logId;
    uint64_t streamId = reqHdr->streamId;

    //check this frame does not exists
    auto frameIt = frames.find({masterId, logId, segmentId});
    BackupStorage::FrameRef frame;
    if (frameIt != frames.end())
        frame = frameIt->second;

    if (frame) {
		// This should never happen.
		LOG(ERROR, "Master tried to write replica for <%s,%lu> but "
			"another replica was recovered from storage with the same id; "
			"rejecting write request", masterId.toString().c_str(),
			segmentId);
		// This exception will crash the calling master.
		throw BackupBadSegmentIdException(HERE);
    }

    // Perform open, if any.
    if (reqHdr->open && !frame) {
        LOG(DEBUG, "Opening <%s,%lu,%lu>", masterId.toString().c_str(),
            logId, segmentId);
        frame = storage->open(config->backup.sync, masterId, logId, segmentId);
        frames[MasterSegmentIdPair(masterId, logId, segmentId)] = frame;
    }

    // Perform write.
    if (!frame) {
        LOG(WARNING, "Tried write to a replica of segment <%s,%lu,%lu> but "
            "no such replica was open on this backup (server id %s)",
            masterId.toString().c_str(), logId, segmentId,
            serverId.toString().c_str());
        throw BackupBadSegmentIdException(HERE);
    } else {
        if (!frame->currentlyOpen()) {
            // The frame has already been closed. The most likely reason
            // is that this RPC is a retry of a previous RPC whose response
            // was lost. Log a message, but then just return without doing
            // anything (i.e., make the RPC idempotent)
            LOG(NOTICE, "Write requested for closed replica <%s,%lu,%lu>; "
                "treating the request as noop",
                masterId.toString().c_str(), logId, segmentId);
            return false;
        }
        CycleCounter<RawMetric> __(&metrics->backup.writeCopyTicks);
        Tub<BackupReplicaMetadata> metadata;
        if (reqHdr->certificateIncluded) {
            metadata.construct(reqHdr->certificate,
                               masterId.getId(),
							   logId,
							   streamId,
							   segmentId,
                               segmentSize,
                               reqHdr->segmentEpoch,
                               reqHdr->close, reqHdr->primary);
        }
        frame->append(source, sizeof(*reqHdr),
                      reqHdr->length, reqHdr->offset,
                      metadata.get(), sizeof(*metadata)); //todo this results in a copy see MultiFileStorage
        metrics->backup.writeCopyBytes += reqHdr->length;
        PerfStats::threadStats.backupBytesReceived += reqHdr->length;
        bytesWritten += reqHdr->length;
    }

    // Perform close, if any.
    if (reqHdr->close) {
        LOG(DEBUG, "Closing <%s,%lu,%lu>", masterId.toString().c_str(), logId, segmentId);
        frame->close();
    }
    return true;
}

} // namespace KerA
