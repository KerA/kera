/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_KEYINFO_H
#define RAMCLOUD_KEYINFO_H

#include "Minimal.h"

namespace KerA {

/**
 * This structure describes a key (primary or secondary) and its length.
 * This will be used by clients issuing write RPCs to specify possibly
 * many secondary keys to be included in the objects to be written.
 */
struct KeyInfo
{
    const void *key;        // primary or secondary key. A NULL value here
                            // indicates absence of a key. Note that this
                            // cannot be true for primary key.
    uint16_t keyLength;     // length of the corresponding key. A 0 value
                            // here means that key is a NULL terminated
                            // string and the actual length is computed
                            // on demand.
};

}

#endif