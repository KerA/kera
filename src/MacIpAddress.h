/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2014-2016 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef RAMCLOUD_MACIPADDRESS_H
#define RAMCLOUD_MACIPADDRESS_H

#include "Common.h"
#include "Driver.h"
#include "ServiceLocator.h"
#include "IpAddress.h"
#include "MacAddress.h"

namespace KerA {

/**
 * This class provides a way for the SolarFlareDriver and transport to deduce
 * the address of the destination or source(local address) of a packet.
 */
class MacIpAddress : public Driver::Address {
  public:
    explicit MacIpAddress(const ServiceLocator* serviceLocator);
    explicit MacIpAddress(const uint32_t ip,
                               const uint16_t port,
                               const uint8_t mac[6] = NULL);


    explicit MacIpAddress(const MacIpAddress& other)
        : Address()
        , ipAddress(other.ipAddress)
        , macAddress(other.macAddress)
        , macProvided(other.macProvided)
    {}

    string toString() const;

    // A KerA::IpAddress object that hold the layer 3 address
    Tub<IpAddress> ipAddress;

    // A KerA::MacAddress object that holds the layer 2 address
    Tub<MacAddress> macAddress;

    // True if MAC address is provided and not equal to 00:00:00:00:00:00
    bool macProvided;
};

}// end KerA
#endif //RAMCLOUD_MACIPADDRESS_H
