/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RAMCLOUD_MULTIWRITEGROUPBATCHOBJECT_H
#define RAMCLOUD_MULTIWRITEGROUPBATCHOBJECT_H

#include "Buffer.h"
#include "MultiOpGroupObject.h"
#include "Tub.h"

namespace KerA {

/**
 * Objects of this class are used to pass parameters into \c multiWriteGroup
 * and for multiWriteGroup to return status values for conditional operations
 * (if used).
 */
struct MultiWriteGroupBatchObject : public MultiOpGroupObject {
    /**
     * Pointer to the contents of the new objects.
     */
    Tub<Buffer>* objects;

    //used in response, if Status == RETRY, check this to see how many objects successfully appended
    uint32_t numberObjectsAppended;
    uint64_t entryId;

    MultiWriteGroupBatchObject(Tub<Buffer>* objects, uint64_t entryId,
            uint64_t keyHash, uint32_t numberObjectsAppended = 0)
        : MultiOpGroupObject(keyHash)
        , objects(objects)
        , numberObjectsAppended(numberObjectsAppended)
        , entryId(entryId)
    {}

    MultiWriteGroupBatchObject()
        : MultiOpGroupObject()
        , objects()
        , numberObjectsAppended()
        , entryId()
    {}

    MultiWriteGroupBatchObject(const MultiWriteGroupBatchObject& other)
        : MultiOpGroupObject(other)
        , objects(other.objects)
        , numberObjectsAppended(other.numberObjectsAppended)
        , entryId(other.entryId)
    {}

    MultiWriteGroupBatchObject& operator=(const MultiWriteGroupBatchObject& other) {
        MultiOpGroupObject::operator =(other);
        objects = other.objects;
        numberObjectsAppended = other.numberObjectsAppended;
        entryId = other.entryId;
        return *this;
    }
};

}

#endif