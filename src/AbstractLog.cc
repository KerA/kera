/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2009-2016 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <assert.h>
#include <stdint.h>

#include "Log.h"
#include "LogCleaner.h"
#include "PerfStats.h"
#include "ServerConfig.h"
#include "ShortMacros.h"
#include "Object.h"
#include "Util.h"

namespace KerA {

/**
 * Constructor for AbstractLog. for kera only Log needed, however
 * to compile stuff this keeps the head/appendLock and managers refs
 *
 * \param entryHandlers
 *      Class to query for various bits of per-object information. For instance,
 *      the log may want to know whether an object is still needed or if it can
 *      be garbage collected. Methods on the given class instance will be
 *      invoked to field such queries.
 * \param segmentManager
 *      The SegmentManager this log should allocate its head segments from.
 * \param replicaManager
 *      The ReplicaManager that will be used to make each of this Log's
 *      Segments durable.
 * \param segmentSize
 *      The size, in bytes, of segments this log will use.
 */
AbstractLog::AbstractLog(LogEntryHandlers* entryHandlers,
                         SegmentManager* segmentManager,
                         ReplicaManager* replicaManager,
                         uint32_t segmentSize,
                         uint32_t numberSegmentsPerGroup,
                         uint32_t numberActiveGroupsPerStreamlet)
    : entryHandlers(entryHandlers),
      segmentManager(segmentManager),
      replicaManager(replicaManager),
      segmentSize(segmentSize),
	  vhead(NULL),
      head(NULL),
	  appendLock("AbstractLog::appendLock"),
	  logEverSynced(false),
      metrics()
{

}

/**
 * Append multiple entries to the log atomically. This ensures that either
 * everything is written, or none of it is. Furthermore, recovery is
 * guaranteed to see either none or all of the entries.
 *
 * Note that the append operation is not synchronous with respect to backups.
 * To ensure that the data appended has been safely written to backups, the
 * sync() method must be invoked after appending. Until sync() is called, the
 * data may or may not have been made durable.
 *
 * \param appends
 *      Array containing the entries to append. References to the entries
 *      are also returned here.
 * \param numAppends
 *      Number of entries in the appends array.
 * \return
 *      True if the append succeeded, false if there was insufficient space
 *      to complete the operation.
 */
bool
AbstractLog::append(AppendVector* appends, uint32_t numAppends)
{
    return true;
}

/**
 * Flushes all the log entries from the given buffer to the log
 * atomically.
 *
 * \param logBuffer
 *      The buffer which contains various log entries
 * \param[out] references
 *      This will hold the reference for each log entry that
 *      is written to the log. The caller must have allocated
 *      memory for this
 * \param numEntries
 *      Number of log entries in the buffer
 */
bool
AbstractLog::append(Buffer *logBuffer, Reference *references,
                    uint32_t numEntries)
{
    return true;
}

/**
 * This method is invoked when a log entry is no longer needed (for example, an
 * object that has been deleted). This method does not change anything in the
 * log itself, but it is used to keep track of free space in segments to help
 * the cleaner identify good candidates for cleaning.
 *
 * Free should be called only once for each entry. Behaviour is undefined if
 * it is called more than once on the same reference.
 *
 * \param reference
 *      Reference to the entry being freed.
 */
void
AbstractLog::free(Reference reference)
{
}

/**
 * Populate the given protocol buffer with various log metrics.
 *
 * \param[out] m
 *      The protocol buffer to fill with metrics.
 */
void
AbstractLog::getMetrics(ProtoBuf::LogMetrics& m)
{
    m.set_total_append_calls(metrics.totalAppendCalls);
    m.set_ticks_per_second(Cycles::perSecond());
    m.set_total_append_ticks(metrics.totalAppendTicks);
    m.set_total_no_space_ticks(metrics.totalNoSpaceTicks);
    m.set_total_bytes_appended(metrics.totalBytesAppended);
    m.set_total_metadata_bytes_appended(metrics.totalMetadataBytesAppended);

    segmentManager->getMetrics(*m.mutable_segment_metrics());
    segmentManager->getAllocator().getMetrics(*m.mutable_seglet_metrics());
}

/**
 * Populate the given perfStats instance with various memory metrics.
 * \param[out] stats
 *      The instance of perfStats to be updated for memory stats.
 */
void
AbstractLog::getMemoryStats(PerfStats* stats)
{
}

/**
 * Given a reference to an entry previously appended to the log, return the
 * entry's type and fill in a buffer that points to the entry's contents.
 * This is the method to use to access something after it is appended to the
 * log.
 *
 * In the common case (small entries, especially) this method is very fast as
 * the reference already points to a contiguous entry. When this is not the
 * case, we take a slower path to look up the subsequent discontiguous pieces
 * of the entry, which typically incurs several additional cache misses.
 *
 * \param reference
 *      Reference to the entry requested. This value is returned in the append
 *      method. If this reference is invalid behaviour is undefined. The log
 *      will indicate when references become invalid via the LogEntryHandlers
 *      class.
 * \param outBuffer
 *      Buffer to append the entry being looked up to.
 * \return
 *      The type of the entry being looked up is returned here.
 */
LogEntryType
AbstractLog::getEntry(Reference reference, Buffer& outBuffer)
{
    return reference.getEntry(&segmentManager->getAllocator(), &outBuffer);
}

/**
 * Given a reference to an appended entry, return the identifier of the segment
 * that contains the entry. An example use of this is tombstones, which mark
 * themselves with the segment id of the object they're deleting. When that
 * segment leaves the system, the tombstone may be garbage collected.
 */
uint64_t
AbstractLog::getSegmentId(Reference reference)
{
    return getSegment(reference)->id;
}

/**
 * Given the size of a new live object, check whether we have enough
 * space to put it into the log without risking being unable to clean.
 * A false reply means some existing objects must be deleted before
 * any new objects can be created (i.e., the current problem can't be
 * fixed by the cleaner). If false is returned, a log message is
 * generated.
 *
 * \param objectSize
 *       The total amount of log space that will be consumed by the object and
 *       its metadata
 */
bool
AbstractLog::hasSpaceFor(uint64_t objectSize) {
    return false;
}
/**
 * Check if a segment is still in the system. This method can be used to
 * determine if data once written to the log is no longer present in the
 * RAMCloud system and hence will not appear again during either normal
 * operation or recovery. Tombstones use this to determine when they are
 * eligible for garbage collection.
 *
 * \param segmentId
 *      The Segment identifier to check for liveness.
 * \return
 *      True if the given segment is present in the log, otherwise false.
 */
bool
AbstractLog::segmentExists(uint64_t segmentId)
{
    return false; //not used
}

/******************************************************************************
 * PRIVATE METHODS
 ******************************************************************************/

LogSegment*
AbstractLog::getSegment(Reference reference)
{
    return segmentManager->getAllocator().getOwnerSegment(
        reinterpret_cast<const void*>(reference.toInteger()));
}

/**
 * Append a typed entry to the log by copying in the data. Entries are binary
 * blobs described by a simple <type, length> tuple.
 *
 * Note that the append operation is not synchronous with respect to backups.
 * To ensure that the data appended has been safely written to backups, the
 * sync() method must be invoked after appending. Until sync() is called, the
 * data may or may not have been made durable.
 *
 * \param lock
 *      Ensures that the caller holds the monitor lock; not actually used.
 * \param type
 *      Type of the entry. See LogEntryTypes.h.
 * \param buffer
 *      Pointer to buffer containing the entry to be appended.
 * \param length
 *      Size of the entry pointed to by #buffer in bytes.
 * \param[out] outReference
 *      If the append succeeds, a reference to the created entry is returned
 *      here. This reference may be used to access the appended entry via the
 *      lookup method. It may also be inserted into a HashTable.
 * \param[out] outTickCounter
 *      If non-NULL, store the number of processor ticks spent executing this
 *      method.
 * \return
 *      True if the append succeeded, false if there was insufficient space
 *      to complete the operation.
 */
bool
AbstractLog::append(const SpinLock::Guard& lock,
            LogEntryType type,
            const void* buffer,
            uint32_t length,
            Reference* outReference,
            uint64_t* outTickCounter)
{
    return true;
}

/**
 * Append a a complete log entry to the log by copying in the data.
 *
 * Note that the append operation is not synchronous with respect to backups.
 * To ensure that the data appended has been safely written to backups, the
 * sync() method must be invoked after appending. Until sync() is called, the
 * data may or may not have been made durable.
 *
 * \param lock
 *      Ensures that the caller holds the monitor lock; not actually used.
 * \param buffer
 *      Pointer to buffer containing the entry to be appended.
 * \param[out] entryLength
 *      Size of the entry including the entry header information which
 *      starts at the location pointer to by #buffer
 * \param[out] outReference
 *      If the append succeeds, a reference to the created entry is returned
 *      here. This reference may be used to access the appended entry via the
 *      lookup method. It may also be inserted into a HashTable.
 * \param[out] outTickCounter
 *      If non-NULL, store the number of processor ticks spent executing this
 *      method.
 * \return
 *      True if the append succeeded, false if there was insufficient space
 *      to complete the operation.
 */
bool
AbstractLog::append(const SpinLock::Guard& lock,
            const void* buffer,
            uint32_t *entryLength,
            Reference* outReference,
            uint64_t* outTickCounter)
{
    return true;
}

/**
 * Append a typed entry to the log by coping in the data. Entries are binary
 * blobs described by a simple <type, length> tuple.
 *
 * Note that the append operation is not synchronous with respect to backups.
 * To ensure that the data appended has been safely written to backups, the
 * sync() method must be invoked after appending. Until sync() is called, the
 * data may or may not have been made durable.
 *
 * \param lock
 *      Ensures that the caller holds the monitor lock; not actually used.
 * \param type
 *      Type of the entry. See LogEntryTypes.h.
 * \param buffer
 *      Buffer object containing the entry to be appended.
 * \param[out] outReference
 *      If the append succeeds, a reference to the created entry is returned
 *      here. This reference may be used to access the appended entry via the
 *      lookup method. It may also be inserted into a HashTable.
 * \param[out] outTickCounter
 *      If non-NULL, store the number of processor ticks spent executing this
 *      method.
 * \return
 *      True if the append succeeded, false if there was insufficient space to
 *      complete the operation.
 */
bool
AbstractLog::append(const SpinLock::Guard& lock,
            LogEntryType type,
            Buffer& buffer,
            Reference* outReference,
            uint64_t* outTickCounter)
{
    return append(lock,
                  type,
                  buffer.getRange(0, buffer.size()),
                  buffer.size(),
                  outReference,
                  outTickCounter);
}

} // namespace
