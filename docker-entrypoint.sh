#!/bin/bash

set -e

case "$1" in
    sh|bash)
        set -- "$@"
        exec "$@"
    ;;
    coordinator)
        shift
        /opt/kera/install/bin/coordinator "$@" 1>/logs/coordinator.out 2>&1 &
        status=$?
        if [ $status -ne 0 ]; then
            echo "Failed to start KerA coordinator: $status"
            exit $status
        fi

        exec tail -f $(ls -Art /logs/coordinator.out | tail -n 1)
    ;;
    broker)
        # The plasma store creates the /tmp/plasma socket on startup
        /opt/kera/install/bin/plasma_store -m 1000000000 -s /tmp/plasma 1>/logs/plasma.out 2>&1 &
        status=$?
        if [ $status -ne 0 ]; then
            echo "Failed to start KerA server: $status"
            exit $status
        fi

        sleep 2

        shift
        /opt/kera/install/bin/server "$@" 1>/logs/server.out 2>&1 &
        status=$?
        if [ $status -ne 0 ]; then
            echo "Failed to start plasma: $status"
            exit $status
        fi

        exec tail -f $(ls -Art /logs/plasma.out | tail -n 1) & tail -f $(ls -Art /logs/server.out | tail -n 1)
    ;;
esac
