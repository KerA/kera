#!/bin/bash

set -e

case "$1" in
    sh|bash)
        set -- "$@"
        exec "$@"
    ;;
    producer-1)
        shift
        /opt/kera/install/bin/producer-1 "$@" 1>/logs/producer-1.out 2>&1 &
        status=$?
        if [ $status -ne 0 ]; then
            echo "Failed to start KerA producer-1: $status"
            exit $status
        fi

        exec tail -f $(ls -Art /logs/producer-1.out | tail -n 1)
    ;;
    producer-3)
        shift
        /opt/kera/install/bin/producer-3 "$@" 1>/logs/producer-3.out 2>&1 &
        status=$?
        if [ $status -ne 0 ]; then
            echo "Failed to start KerA producer-3: $status"
            exit $status
        fi

        exec tail -f $(ls -Art /logs/producer-3.out | tail -n 1)
    ;;
    consumer-3)
        shift
        /opt/kera/install/bin/consumer-3 "$@" 1>/logs/consumer-3.out 2>&1 &
        status=$?
        if [ $status -ne 0 ]; then
            echo "Failed to start KerA consumer-3: $status"
            exit $status
        fi

        exec tail -f $(ls -Art /logs/consumer-3.out | tail -n 1)
    ;;
esac
