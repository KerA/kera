/* Copyright 2020 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Cycles.h"
#include "MultiWriteGroupBatchObjectE.h"
#include "Record.h"
#include "WriteGroupBatchMultiRpcWrapper.h"

#include <iostream>

#define SERVER_SPAN 1
#define STREAMLET_SPAN 1
#define STREAMLET_ID 1
#define BATCH_SIZE 2000
#define KEY_SIZE 10
#define RECORD_SIZE 100
#define RECORD_COUNT 1000000
#define PRODID 1

int main(int argc, char *argv[]) {
    if (argc != 2) {
        std::cerr << "Usage: " << argv[0] << " coordinatorLocator" << std::endl;
        return -1;
    }

    // Creating RamCloud interface objects.

    KerA::Tub<KerA::RamCloud> ramcloud;
    ramcloud.construct(argv[1], "test");

    uint64_t streamId =
      ramcloud->createTable("table1", SERVER_SPAN, STREAMLET_SPAN);

    // Application-specific variables.

    uint64_t tabletRange = 1 + ~0UL / STREAMLET_SPAN;
    uint64_t keyHash = (STREAMLET_ID - 1) * tabletRange;

    uint64_t totalRecords = 0;
    uint64_t totalBatches = 0;

    // Building a primary key.

    KerA::Tub<KerA::Key> key;
    key.construct(streamId,
                  static_cast<void*>(NULL),
                  static_cast<KerA::KeyLength>(0));
    size_t tslength = 0;
    std::string tsnow = "";
    char keyv[KEY_SIZE];
    if (KEY_SIZE > 0) {
        memset(keyv, 0, KEY_SIZE);
        std::string prodids = std::to_string(PRODID) + " ";
        tslength = prodids.copy(keyv, prodids.length());
        key.construct(streamId, static_cast<void*>(keyv), 
                      static_cast<KerA::KeyLength>(KEY_SIZE));
    }

    KerA::Tub<Record> record;
    record.construct(KEY_SIZE, RECORD_SIZE);
    char value[RECORD_SIZE];
    for (int val = 0; val < RECORD_SIZE; ++val) {
        value[val] = 'x';
    }

    KerA::StreamObject::appendHeaderKeysAndValueToBuffer(*key.get(),
                                                        value,
                                                        RECORD_SIZE,
                                                        record.get(),
                                                        true);
    const uint32_t recordLength = record->size();

    // Sending the RPC request to the server.

    uint64_t entryId = 1;

    KerA::Tub<KerA::Buffer> buffer;
    buffer.construct();

    KerA::Tub<MultiWriteGroupBatchObjectE> batch;
    batch.construct(&buffer, entryId++, -1, BATCH_SIZE);
    batch->reset(keyHash, STREAMLET_ID);

    for (uint64_t i = 0; i < RECORD_COUNT; i++) {
        /*
        if (KEY_SIZE > 0) {
            memset(keyv + tslength, 0, KEY_SIZE - tslength);
            tsnow = std::to_string(KerA::Cycles::rdtsc());
            tsnow.copy(keyv + tslength, tsnow.length());
        }
        */

        bool toBeClosed = false;

        if (!batch->closed) {
            if (batch->batchPart->length + recordLength <= BATCH_SIZE) {
                // Append the record to the batch.
                batch->objects->get()->appendCopy(
                    record->getRange(0, recordLength),
                    recordLength
                );
                batch->batchPart->length += recordLength;
                batch->batchPart->countEntries = 1;

                if (batch->batchPart->length + recordLength > BATCH_SIZE) {
                    toBeClosed = true;
                }
            } else {
                toBeClosed = true;
            }
        }

        if (toBeClosed) {
            KerA::Crc32C crc;
            const uint32_t recordsOffset = KerA::sizeof32(
                KerA::WireFormat::MultiOpGroup::Request::WriteGroupBatchPart
            );
            crc.update(
                batch->objects->get()->getRange(
                    recordsOffset,
                    batch->batchPart->length - recordsOffset
                ),
                batch->batchPart->length - recordsOffset
            );
            batch->batchPart->header.checksum = crc.getResult();
            batch->streamletIdBatchIndex = 0;
            batch->closed = true;

            // Sending the RPC request.

            //auto data = reinterpret_cast<KerA::MultiWriteGroupBatchObject* const *>(batch.get());
            //std::cout << "numberObjectsAppended: " << (*data)->numberObjectsAppended << std::endl;
            //std::cout << "entryId: " << (*data)->entryId << std::endl;

            KerA::MultiWriteGroupBatchObject * const requests[] = {batch.get()};

            KerA::WriteGroupBatchMultiRpcWrapper batchRequest(
                    ramcloud.get(),
                    requests,
                    1,
                    streamId,
                    PRODID);
            
            while (!batchRequest.isReady()) {
                ramcloud->poll();
                KerA::Cycles::sleep(1000);  // 1 ms
            }

            totalBatches++;
            totalRecords += batch->numberObjectsAppended;

            std::cout << ">>> record " << i << " total_records " << totalRecords << " total_batches " << totalBatches << std::endl;

            batch->reset(keyHash, STREAMLET_ID);
        }
    }
    
    return EXIT_SUCCESS;
}
