/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "Record.h"

Record::Record(uint32_t keySize, uint32_t recordSize)
{
    const KerA::ObjectLength objectLength =
        KerA::sizeof32(KerA::KeyCount)
        + KerA::sizeof32(KerA::CumulativeKeyLength)
        + keySize
        + recordSize;  // + sizeof32(StreamObject::HeaderGroup);

    KerA::Segment::EntryHeader entryHeader(KerA::LOG_ENTRY_TYPE_OBJ,
                                               objectLength);

    KerA::Buffer entryHeaderBuffer;  // overhead per record
    entryHeaderBuffer.append(&entryHeader,
                                    KerA::sizeof32(entryHeader));
    entryHeaderBuffer.append(&objectLength,
                                    entryHeader.getLengthBytes());

    const uint32_t entryHeaderBufferLength = KerA::sizeof32(entryHeader)
            + entryHeader.getLengthBytes();
    
    // contains entry header + length + object's [keys-value]
    this->allocFirstChunkInternal(entryHeaderBufferLength + objectLength);  // contiguous region
    this->resetFirstChunk();  // use only batchStream
    this->appendCopy(
        entryHeaderBuffer.getRange(0, entryHeaderBufferLength),
        entryHeaderBufferLength
    );
}
