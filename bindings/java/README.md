# Java bindings

## Build

Java and C++ sources for the bindings are located in `kera_jni`.

Make sure to compile KerA first by running `make` in the top-level directory. Use `./gradlew assemble` from this directory or `make java` from the top-level directory to build the Java bindings.

Run `./gradlew clean` to clean the output.

## Tests (WIP)

Java and C++ sources for the unit tests are located in `kera_jni_test`.

Run `./gradlew test` to run the tests. Run `./gradlew run -Plocator=<locatorString>` to run `TestClient`.

Unit tests are currently non-functional.
