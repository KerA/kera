/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera.test;

import fr.inria.kera.Util;

public class TestCluster {
    static {
        // Load native library
        Util.loadLibrary("kera_jni_test");
    }

    // Keep pointers to the C++ RAMCloud and MockCluster objects.
    private long[] pointers;

    /**
     * Construct a new TestCluster with one master.
     */
    public TestCluster() {
        pointers = new long[3];
        createMockCluster(pointers);
    }

    // Native methods documented in corresponding C++ file.
    private static native void createMockCluster(long[] pointers);
    private static native void destroy(long[] pointers);

    /**
     * Get the pointer to the C++ RamCloud object.
     *
     * @return The memory address of the C++ RamCloud object, for use in
     *         contructing a Java RAMCloud object tied to the C++ RamCloud
     *         object.
     */
    public long getRamcloudClientPointer() {
        return pointers[1];
    }

    /**
     * When the test is done with the TestCluster and RAMCloud object, call this
     * method to delete the pointers to the corresponding C++ objects.
     */
    public void destroy() {
        destroy(pointers);
    }
}
