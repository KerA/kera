/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2014 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
package fr.inria.kera.test;

import fr.inria.kera.*;

import java.lang.reflect.*;
import org.testng.annotations.*;

import static org.testng.AssertJUnit.*;

import java.util.*;

/**
 * Class that holds methods to create and destroy the test cluster used for the
 * unit tests.
 * 
 * run this as
 * 
 * ./gradlew -Dorg.gradle.daemon=true test --info --tests
 * *ClientTestClusterSetup.createDeleteTable
 * 
 * ./gradlew -Djava.library.path="/media/sf_Dropbox/ramcloud/vbox/RAMCloud/obj.streamletGroupsReplicas/" 
 * -Dorg.gradle.daemon=true test --info --tests *KeraHandoverClusterSetup.startKeraConsumerThread
 */
public class KeraBindingsSharedClusterSetup {
	
	public static KeraBindingsShared bindings;
	public static TestCluster cluster;
	private long tableId;
	private String key;

	@BeforeSuite
	public void setupClient() {
		cluster = new TestCluster();
		bindings = new KeraBindingsShared(cluster.getRamcloudClientPointer(), new Handover());
	}

	@AfterSuite
	public void cleanUpClient() {
		cluster.destroy();
	}

	@Test
	public void createDeleteTable() {
		long tableId = bindings.createTable("testTable", 1, 1, 1);
		assertEquals(tableId, 1);
		long readId = bindings.getTableId("testTable");
		assertEquals(tableId, readId);
		bindings.dropTable("testTable");
	}

	@Test
	public void startPullThread() {
		bindings.startMultiReadPullThread(new java.util.Properties(), new java.util.ArrayList<Streamlet>());
		assertEquals(1, 1);
	}

	@Test
	public void startKeraConsumerThread() throws Exception {
		//todo this test fails,  other KeraHandover* tests ok: pull, shared push, single thread pull
		long tableId = bindings.createTable("testTable", 1, 1, 1);
		assertEquals(tableId, 1);
		long readId = bindings.getTableId("testTable");
		assertEquals(tableId, readId);
		
		Properties keraProdProperties = new Properties();
		keraProdProperties.setProperty("PRODUCER_ID", "1");
		keraProdProperties.setProperty("STREAM_ID", Long.valueOf(tableId).toString());
		keraProdProperties.setProperty("RECORDS_COUNT", "100000");
		keraProdProperties.setProperty("NNODES", "1");
		keraProdProperties.setProperty("NSTREAMLETS", "1");
		keraProdProperties.setProperty("BATCH_STREAM", "6");
		keraProdProperties.setProperty("REQUEST_SIZE", "6");
		keraProdProperties.setProperty("RECORD_SIZE", "1");
		keraProdProperties.setProperty("KEY_SIZE", "0");

		KeraProducerHandover producer = new KeraProducerHandover(cluster.getRamcloudClientPointer(), new Handover());
		producer.startProducerThread(keraProdProperties); //blocks
		
		Properties keraProperties = new Properties();
		keraProperties.setProperty("READER_ID", "1");
		keraProperties.setProperty("STREAM_ID", Long.valueOf(tableId).toString());
		keraProperties.setProperty("RECORDS_COUNT", "100000"); //ensure consumer stops
		keraProperties.setProperty("NNODES", "1");
		keraProperties.setProperty("NSTREAMLETS", "1");
		keraProperties.setProperty("NACTIVEGROUPS", "1");
		keraProperties.setProperty("BATCH_STREAM", "1024"); //todo minimum, enforce on client, 88 = 16B (batchPart) + 58B chunk record + 12B record with its headers
		keraProperties.setProperty("NRECORDS_THROTTLE", "10000000");
		
		List<Streamlet> streamlets = new ArrayList<Streamlet>();
		Streamlet s = new Streamlet("streamName", 1);
		streamlets.add(s);
		
		Handover h = new Handover();
		KeraConsumerSharedThread kct = new KeraConsumerSharedThread(h, 
				streamlets, 100, keraProperties, cluster.getRamcloudClientPointer());
		kct.start();
		
		Thread.sleep(10000);
		System.out.println("done closing");
		kct.shutdown();
		
		kct.join();
		
		bindings.dropTable("testTable");
		assertEquals(1, 1);
	}

	/**
	 * Invoke the specified method on the target object through reflection.
	 *
	 * @param object
	 *          The object to invoke the method on.
	 * @param name
	 *          The name of the method to invoke.
	 * @oaran argClasses The classes of the arguments to the method
	 * @param args
	 *          The arguments to invoke with.
	 * @return The result of the invoked method.
	 */
	public static Object invoke(Object object, String name, Class[] argClasses, Object... args) {
		Object out = null;
		try {
			Method method = object.getClass().getDeclaredMethod(name, argClasses);
			method.setAccessible(true);
			out = method.invoke(object, args);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return out;
	}
}
