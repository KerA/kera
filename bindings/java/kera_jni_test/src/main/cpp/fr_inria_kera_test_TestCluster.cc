/* Copyright 2018-2020 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright (c) 2013 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "fr_inria_kera_test_TestCluster.h"

#include "MockCluster.h"
#include "TestUtil.h"
#include "RamCloud.h"

#include <iostream>

using namespace KerA;

/**
 * Creates a mock cluster with one master.
 *
 * \param env
 *      The current JNI environment.
 * \param clazz
 *      The calling Java class.
 * \param pointers
 *      A java long array to store the resulting pointers of the
 *      RAMCloud and cluster objects.
 */
JNIEXPORT void JNICALL Java_fr_inria_kera_test_TestCluster_createMockCluster
(JNIEnv *env, jclass clazz, jlongArray pointers) {
    // Disable the verbose C++ log messages
    Logger::get().setLogLevels(4);
    KerA::AbstractServerList::skipServerIdCheck = true;
    KerA::CoordinatorService::forceSynchronousInit = true;

    //TestLog::Enable* logEnabler = new TestLog::Enable();
    Context* context = new Context();
    MockCluster* cluster = new MockCluster(context,"mock:host=coordinator");
    ServerConfig config = ServerConfig::forTesting();
    config.services = {WireFormat::MASTER_SERVICE, WireFormat::BACKUP_SERVICE, WireFormat::ADMIN_SERVICE};
    config.localLocator = "mock:host=master";

    config.numberSegmentsPerGroup = 1024;
    config.numberActiveGroupsPerStreamlet = 1;
    config.master.numReplicas = 1;
    config.master.masterActiveGroupsPerStreamlet = 1;
    config.master.logBytes = 1324 * 1024 * 1024UL;
    config.master.allowLocalBackup = true;
    config.master.disableLogCleaner = true;
    config.master.disableInMemoryCleaning = true;

    config.maxCores = 3;
    config.maxObjectKeySize = 512;
    config.maxObjectDataSize = 1024;
    config.segmentSize = 8 * 1024 * 1024;  // bytes
    config.segletSize = 8 * 1024 * 1024;  // bytes

    config.backup.inMemory = true;
    config.backup.numSegmentFrames = 256;
    config.backup.maxNonVolatileBuffers = 128;
    config.backup.maxRecoveryReplicas = 2;
    config.backup.file = "/var/tmp/backup.log";

    cluster->addServer(config);
    //config.maxCores = 2;
    //config.backup.inMemory = true;
    //config.backup.numSegmentFrames = 2048;
    //config.services = {WireFormat::BACKUP_SERVICE, WireFormat::ADMIN_SERVICE};
    //config.localLocator = "mock:host=master2x";
    //cluster->addServer(config);

    RamCloud* ramcloud = new RamCloud(context, "mock:host=coordinator");

    /*
    Transport::SessionRef session =
            ramcloud->clientContext->transportManager->getSession(
            "mock:host=master1x");
    BindTransport::BindSession* session1 = static_cast<BindTransport::BindSession*>(session.get());

    ramcloud->createTable("table1", 1, 1);
    */

    uint64_t* pointerPointer = static_cast<uint64_t*>(env->GetPrimitiveArrayCritical(pointers, 0));
    pointerPointer[0] = reinterpret_cast<uint64_t>(cluster);
    pointerPointer[1] = reinterpret_cast<uint64_t>(ramcloud);
    //pointerPointer[2] = reinterpret_cast<uint64_t>(context);
    env->ReleasePrimitiveArrayCritical(pointers, reinterpret_cast<void*>(pointerPointer), 0);
}

/**
 * Deletes a mock cluster and RAMCloud object.
 *
 * \param env
 *      The current JNI environment.
 * \param clazz
 *      The calling Java class.
 * \param pointers
 *      A java long array with the pointers to delete.
 */
JNIEXPORT void JNICALL Java_fr_inria_kera_test_TestCluster_destroy
(JNIEnv *env, jclass clazz, jlongArray pointers) {
    uint64_t* pointerPointer = static_cast<uint64_t*>(env->GetPrimitiveArrayCritical(pointers, 0));

	//Context* context = reinterpret_cast<Context*>(pointerPointer[2]);
	//delete context;
    MockCluster* cluster = reinterpret_cast<MockCluster*>(pointerPointer[0]);
    delete cluster;

    RamCloud* ramcloud = reinterpret_cast<RamCloud*>(pointerPointer[1]);
    delete ramcloud;

    env->ReleasePrimitiveArrayCritical(pointers, reinterpret_cast<void*>(pointerPointer), JNI_ABORT);
}
