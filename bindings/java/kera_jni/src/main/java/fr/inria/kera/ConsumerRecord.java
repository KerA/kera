/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

// except key-value, avoid if possible the other params
public final class ConsumerRecord<K, V> {
    
//    private final String stream;
//    private final int streamletId;
//    private final long groupId;
//    private final long segmentId;
//    private final int segmentOffset;
    private final K key;
    private final V value;

//    public ConsumerRecord(String stream, int streamletId, long groupId, 
//            long segmentId, int segmentOffset, K key, V value) 
//    {
//        if (stream == null) {
//            throw new IllegalArgumentException("Stream cannot be null");
//        } else {
//            this.stream = stream;
//            this.streamletId = streamletId;
//            this.groupId = groupId;
//            this.segmentId = segmentId;
//            this.segmentOffset = segmentOffset;
//            this.key = key;
//            this.value = value;
//        }
//    }
    
    public ConsumerRecord(K key, V value) 
    {
        this.key = key;
        this.value = value;
    }
    
    public K key() {
        return this.key;
    }

    public V value() {
        return this.value;
    }

//    public String stream() { return this.stream; }
//    public int streamletId() { return this.streamletId; }
//    public long groupId() {return this.groupId;}
//    public long segmentId() {return this.segmentId;}
//    public long segmentOffset() {return this.segmentOffset;}
    public String toString() {
        return "ConsumerRecord(key = " + this.key + ", value = " + this.value + ")";
    }
}
