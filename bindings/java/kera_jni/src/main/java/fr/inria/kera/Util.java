/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2014 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package fr.inria.kera;

import java.util.*;

import cz.adamh.utils.NativeUtils;

/**
 * This class provides common Java utility methods for the RAMCloud bindings.
 */
public class Util {
    private static Set<String> loadedLibraries;

    static {
        loadedLibraries = new HashSet<>();
    }

    /**
     * Loads the specified native library.
     *
     * @param library
     *      The name of the native library to load.
     */
    public static void loadLibrary(String library) {
        if (!loadedLibraries.add(library)) {
            // Library already loaded
            return;
        }
        try {
            System.loadLibrary(library);
        } catch (UnsatisfiedLinkError ex) {
            System.err.println(ex.getMessage());
            // Load from jar
            try {
                NativeUtils.loadLibraryFromJar("/lib" + library + ".so");
            } catch (Exception ex2) {
                // Temporary file creation failed, let user handle.
                System.err.println(ex2.getMessage());
                ex2.printStackTrace();
            }
        } catch (Exception ex) {
            // Other error, let user handle.
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
}
