/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

/**
 * group offset
 */
public class GroupSegmentInfo {

	private long groupId = -1;
	private long segmentId = -1;
	private int offset = -1;
	private int maxObjects = 0;
	private int maxResponseLength = -1;

	public long getGroupId() {
		return this.groupId;
	}
	
	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}
	
	public long getSegmentId() {
		return this.segmentId;
	}
	
	public void setSegmentId(long segmentId) {
		this.segmentId = segmentId;
	}
	
	public int getOffset() {
		return this.offset;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public int getMaxObjects() {
		return this.maxObjects;
	}
	
	public void setMaxObjects(int maxObjects) {
		this.maxObjects = maxObjects;
	}

	public int getMaxResponseLength() {
		return this.maxResponseLength;
	}
	
	public void setMaxResponseLength(int maxResponseLength) {
		this.maxResponseLength = maxResponseLength;
	}
}
