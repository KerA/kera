/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * Copyright (c) 2014 Stanford University
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR(S) DISCLAIM ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL AUTHORS BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

package fr.inria.kera;

import fr.inria.kera.multiop.*;

import java.util.*;

/**
 * A Java RAMCloud client used for testing. Will contain sample code eventually.
 */
public class KeraTestClient {
    private RAMCloud ramcloud;
    private long tableId;

    static {
        // Load C++ shared library for JNI
        Util.loadLibrary("kera_jni");
    }

    /**
     * A simple end-to-end test of the java bindings.
     */
    public static void main(String argv[]) {
        new KeraTestClient().go(argv);
        //arguments
        //0 locator
        //1 streamName
//    	s << argv[2] << ' ' << argv[3] << ' ' << argv[4] << ' ' << argv[5] << ' ' 
        //<< argv[6] << ' ' << argv[7] << ' ' << argv[8] << ' ' << argv[9] << ' ' << argv[10];
//    	s >> recordCount >> BATCH_STREAM >> NRECORDS >> NSTREAMLETS 
        //>> NNODES >> READERID >> streamletCount >> clientId >> NACTIVEGROUPS;

    }

    private void go(String[] argv) {
        // Include a pause to add gdb if need to debug c++ code
        boolean debug = false;
        if (debug) {
            System.out.print("Press [Enter] to continue:");
            Scanner scn = new Scanner(System.in);
            scn.nextLine();
        }

        ramcloud = new RAMCloud(argv[0]);
        tableId = ramcloud.createTable("hi");

        multiReadTest();

        ramcloud.dropTable("hi");
        ramcloud.disconnect();
    }

    private void multiReadTest() {
        int numTimes = 5000;

        MultiReadObject[] reads = new MultiReadObject[numTimes];
        for (int i = 0; i < numTimes; i++) {
            byte[] key = new byte[30];
            for (int j = 0; j < 4; j++) {
                key[j] = (byte) ((i >> (j * 8)) & 0xFF);
            }
            byte[] value = new byte[100];
            ramcloud.write(tableId, key, value, null);
            // System.out.println("Wrote:" + i);
            reads[i] = new MultiReadObject(tableId, key);
        }
        // System.out.println("filled table");

        long start = System.nanoTime();
        ramcloud.read(reads);
        long time = System.nanoTime() - start;

        System.out.println("Average multiread time per object: " + ((double) time / numTimes / 1000.0));
    }

}
