/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.inria.kera;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.NoSuchElementException;

public class ConsumerRecords<K, V> implements Iterable<ConsumerRecord<K, V>> {

    @SuppressWarnings("unchecked")
    public static final ConsumerRecords<Object, Object> EMPTY = new ConsumerRecords<>(Collections.EMPTY_MAP);

    private final Map<Streamlet, List<ConsumerRecord<K, V>>> records;

    public ConsumerRecords(Map<Streamlet, List<ConsumerRecord<K, V>>> records) {
        this.records = records;
    }

    /**
     * Get just the records for the given partition
     * 
     * @param partition The partition to get records for
     */
    public List<ConsumerRecord<K, V>> records(Streamlet partition) {
        List<ConsumerRecord<K, V>> recs = this.records.get(partition);
        if (recs == null)
            return Collections.emptyList();
        else
            return Collections.unmodifiableList(recs);
    }

    /**
     * Get just the records for the given topic
     */
    public Iterable<ConsumerRecord<K, V>> records(String topic) {
        if (topic == null)
            throw new IllegalArgumentException("Topic must be non-null.");
        List<List<ConsumerRecord<K, V>>> recs = new ArrayList<>();
        for (Map.Entry<Streamlet, List<ConsumerRecord<K, V>>> entry : records.entrySet()) {
            if (entry.getKey().stream().equals(topic))
                recs.add(entry.getValue());
        }
        return new ConcatenatedIterable<>(recs);
    }

    /**
     * Get the partitions which have records contained in this record set.
     * @return the set of partitions with data in this record set (may be empty if no data was returned)
     */
    public Set<Streamlet> partitions() {
        return Collections.unmodifiableSet(records.keySet());
    }

    @Override
    public Iterator<ConsumerRecord<K, V>> iterator() {
        return new ConcatenatedIterable<>(records.values()).iterator();
    }

    /**
     * The number of records for all topics
     */
    public int count() {
        int count = 0;
        for (List<ConsumerRecord<K, V>> recs: this.records.values())
            count += recs.size();
        return count;
    }

    private static class ConcatenatedIterable<K, V> implements Iterable<ConsumerRecord<K, V>> {

        private final Iterable<? extends Iterable<ConsumerRecord<K, V>>> iterables;

        public ConcatenatedIterable(Iterable<? extends Iterable<ConsumerRecord<K, V>>> iterables) {
            this.iterables = iterables;
        }

        @Override
        public Iterator<ConsumerRecord<K, V>> iterator() {
            return new AbstractIterator<ConsumerRecord<K, V>>() {
                Iterator<? extends Iterable<ConsumerRecord<K, V>>> iters = iterables.iterator();
                Iterator<ConsumerRecord<K, V>> current;

                public ConsumerRecord<K, V> makeNext() {
                    while (current == null || !current.hasNext()) {
                        if (iters.hasNext())
                            current = iters.next().iterator();
                        else
                            return allDone();
                    }
                    return current.next();
                }
            };
        }
    }

    public boolean isEmpty() {
        return records.isEmpty();
    }

    @SuppressWarnings("unchecked")
    public static <K, V> ConsumerRecords<K, V> empty() {
        return (ConsumerRecords<K, V>) EMPTY;
    }

    /**
     * A base class that simplifies implementing an iterator
     * @param <T> The type of thing we are iterating over
     */
    public static abstract class AbstractIterator<T> implements Iterator<T> {

        private enum State {
            READY, NOT_READY, DONE, FAILED
        }

        private State state = State.NOT_READY;
        private T next;

        @Override
        public boolean hasNext() {
            switch (state) {
                case FAILED:
                    throw new IllegalStateException("Iterator is in failed state");
                case DONE:
                    return false;
                case READY:
                    return true;
                default:
                    return maybeComputeNext();
            }
        }

        @Override
        public T next() {
            if (!hasNext())
                throw new NoSuchElementException();
            state = State.NOT_READY;
            if (next == null)
                throw new IllegalStateException("Expected item but none found.");
            return next;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Removal not supported");
        }

        public T peek() {
            if (!hasNext())
                throw new NoSuchElementException();
            return next;
        }

        protected T allDone() {
            state = State.DONE;
            return null;
        }

        protected abstract T makeNext();

        private Boolean maybeComputeNext() {
            state = State.FAILED;
            next = makeNext();
            if (state == State.DONE) {
                return false;
            } else {
                state = State.READY;
                return true;
            }
        }

    }
    
}
