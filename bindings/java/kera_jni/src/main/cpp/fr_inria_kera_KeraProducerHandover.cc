/* Copyright 2020-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fr_inria_kera_KeraProducerHandover.h"
#include "JavaCommon.h"

#include "Buffer.h"
#include "ClientException.h"
#include "ChunkEntry.h"
#include "ChunkEntryDigestChecksum.h"
#include "Crc32C.h"
#include "Common.h"
#include "Cycles.h"
#include "Logger.h"
#include "LogMetadata.h"
#include "MultiRemove.h"
#include "MultiRead.h"
#include "MultiWrite.h"
#include "PerfStats.h"
#include "RamCloud.h"
#include "Segment.h"
#include "ShortMacros.h"
#include "StreamObject.h"
#include "TimeTrace.h"
#include "Tub.h"
#include "Util.h"
#include "WireFormat.h"
#include "WriteGroupBatchMultiRpcWrapper.h"

#include <stdio.h>
#include <thread>
#include <list>
#include <string>
#include <queue>
#include <iostream>
#include <sstream>
#include <atomic>
#include <algorithm>

using namespace KerA;

//how much memory the producer can use to batch records before sending them = CACHE_SIZE * BATCH_SIZE
#define CACHE_SIZE 256
#define MAXNUMSTREAMLETS 128

struct MultiWriteGroupBatchObjectE;

//next is used to access prodStreamletBatches[streamletId]
UnnamedSpinLock prodStreamletActiveLocks[MAXNUMSTREAMLETS]; //gives a lock for each streamletId deque

//key=streamletId, value is a list of indexes to prodBatchesReuse; we append to the tail of indexes, the others should be closed
//to be used by producers for appending next record
//to be used by writers to identify "to be/closed" batches to be written
//prodStreamletBatches[streamletId] is protected by prodStreamletActiveLocks[streamletId%MAXNUMSTREAMLETS]
std::map<uint32_t, std::deque<uint32_t>> prodStreamletBatches;

//contains batch objects with objects' content in prodBuffers[i]
//access is protected by prodStreamletActiveLocks[streamletId%MAXNUMSTREAMLETS]
std::array<MultiWriteGroupBatchObjectE*, CACHE_SIZE> prodBatchesReuse; //size of CACHE_SIZE

//next is used to access prodBatchesReuseIndexes
SpinLock prodCacheLock("batches"); //for batches

//here the writers push written object's indexes
//it is used by producers ONlY if prodStreamletBatches[streamletId].back contains no open batch to append records to
//protected by prodCacheLock
std::queue<uint32_t> prodBatchesReuseIndexes;
//used to drain prodBatchesReuseIndexes
std::queue<uint32_t> prodBatchesReuseIndexesCached;

std::atomic<uint64_t> prodRecords; //total records written until now, accessed by main thread and writers
std::atomic<bool> producerStop;
std::atomic<bool> writerStop;

Tub<Buffer> prodBuffers[CACHE_SIZE];

struct MultiWriteGroupBatchObjectE: public MultiWriteGroupBatchObject {

    std::atomic<bool> closed;
    uint64_t creationTime; //initialized when first record is appended to
    WireFormat::MultiOpGroup::Request::WriteGroupBatchPart* batchPart;
    uint32_t streamletIdBatchIndex;

    MultiWriteGroupBatchObjectE(Tub<Buffer>* objects, uint64_t entryId,
            uint64_t keyHash) :
            MultiWriteGroupBatchObject(objects, entryId, keyHash), closed(
                    false), creationTime(-1), batchPart(NULL), streamletIdBatchIndex(
                    -1) {
    }

    MultiWriteGroupBatchObjectE() :
            MultiWriteGroupBatchObject(), closed(), creationTime(), batchPart(), streamletIdBatchIndex() {
    }

    MultiWriteGroupBatchObjectE(const MultiWriteGroupBatchObjectE& other) :
            MultiWriteGroupBatchObject(other), closed(false), creationTime(
                    other.creationTime), batchPart(NULL), streamletIdBatchIndex(
                    other.streamletIdBatchIndex) {
    }

    MultiWriteGroupBatchObjectE& operator=(
            const MultiWriteGroupBatchObjectE& other) {
        MultiWriteGroupBatchObject::operator =(other);
        closed = false;
        creationTime = other.creationTime;
        batchPart = other.batchPart;
        streamletIdBatchIndex = other.streamletIdBatchIndex;
        return *this;
    }
};

/**
 * streamId
 * BATCH_SIZE in total bytes per batch
 * RECORD_SIZE in bytes record value
 * KEY_SIZE in bytes record key
 * NSTREAMLETS number of streamlets from 1..N
 * NNODES number of nodes from 1..N not used for now
 * partitionerMode if false partition round-robin (next streamlet), if true partition by key#hash
 */
void produce_batches(uint64_t streamId, uint32_t BATCH_SIZE,
        uint32_t RECORD_SIZE, uint16_t KEY_SIZE, uint32_t NSTREAMLETS,
        uint32_t NNODES, bool partitionerMode, uint64_t recordCount, uint64_t PRODID)
try
{
    uint32_t streamletId = 0; //to be initialized later, >=1

    uint32_t streamletSpan = NSTREAMLETS;
    uint64_t tabletRange = 1 + ~0UL / streamletSpan;
    uint64_t keyHash = -1; // (streamletId-1) * tabletRange;
    std::string prodids = std::to_string(PRODID) + " ";

    //when stream record has no key, initialize entryHeaderBuffer
    Tub<Buffer> entryHeaderBuffer; //overhead per record
    entryHeaderBuffer.construct();

    //total object length including overhead/headers - minus entryHeader
    ObjectLength objectLength = KEY_INFO_LENGTH(1) + KEY_SIZE + RECORD_SIZE; // + sizeof32(StreamObject::HeaderGroup);
    Segment::EntryHeader entryHeader(LOG_ENTRY_TYPE_OBJ, objectLength);
    uint32_t entryHeaderBufferLength = sizeof32(entryHeader) + entryHeader.getLengthBytes();
    entryHeaderBuffer.get()->append(&entryHeader, sizeof32(entryHeader));
    entryHeaderBuffer.get()->append(&objectLength, entryHeader.getLengthBytes());

    //the position in the batch buffer where records are appended
    //before submitting a batch we compute the checksum over all records
    uint32_t recordsOffset = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
    fprintf(stdout, "======== recordsOffset overhead per batch ======= count=%d\n", recordsOffset);
    fflush (stdout);

    Tub<Key> key;
    key.construct(streamId, static_cast<void*>(NULL), 0); //supposed KEY_SIZE==0 no key
    Tub<SpinLock::Guard> takeCacheLock;
    Tub<SpinLock::Guard> takeStreamletActiveLock;

    uint32_t totalRecords = 0;
    char value[RECORD_SIZE];
    char keyv[KEY_SIZE];
    for (int val = 0; val < RECORD_SIZE; ++val) { value[val] = 'x'; }

    size_t tslength = 0;
    if (KEY_SIZE > 0) {
        memset(keyv, 0, KEY_SIZE);
        tslength = prodids.copy(keyv, prodids.length());
        key.construct(streamId, static_cast<void*>(keyv), KEY_SIZE);
    }

    Tub<Buffer> objectBuffer; //contains entry header + length + object's [keys-value]
    objectBuffer.construct();

    objectBuffer.get()->allocFirstChunkInternal(entryHeaderBufferLength+objectLength); //contiguous region, 11 extra for headers
    objectBuffer.get()->resetFirstChunk(); //use only batchStream

    objectBuffer.get()->appendCopy(entryHeaderBuffer.get()->getRange(0, entryHeaderBufferLength), entryHeaderBufferLength);

//    uint32_t checksum =
    StreamObject::appendHeaderKeysAndValueToBuffer(*key.get(),
            value, RECORD_SIZE, objectBuffer.get(), true);
    uint32_t objectBufferLength = objectBuffer.get()->size();

    //a reference to reusable nextBatch
    uint32_t batchIndexReuse = -1;
    std::string tsnow = "";

    uint32_t prevStreamletId = 0; //we keep it to load balance records

    MultiWriteGroupBatchObjectE* nextBatch = NULL;
    MultiWriteGroupBatchObjectE* lastBatch = NULL;

    std::vector<MultiWriteGroupBatchObjectE*> lastBatches; //a hint to avoid querying closed batches by writers
    lastBatches.resize(NSTREAMLETS);

    int count = 0;
    while (!producerStop)
    {
        batchIndexReuse = -1;
        nextBatch = NULL;
        lastBatch = NULL;

        if(totalRecords >= recordCount) {
            fprintf(stdout, "======== done produce_batches =======\n");
            fflush (stdout);
            break;
        }

        if (KEY_SIZE > 0) {
            memset(keyv+tslength, 0, KEY_SIZE-tslength);
//            *reinterpret_cast<uint64_t*>(keyv + tslength) = Cycles::toMicroseconds(Cycles::rdtsc());
            tsnow = std::to_string(Cycles::toMicroseconds(Cycles::rdtsc()));
            tsnow.copy(keyv+tslength, tsnow.length());
        }

        //check partitionerMode and initialize streamletId
        if (partitionerMode && KEY_SIZE > 0) {
            uint64_t keyPartHash = key.get()->getHash(streamId, keyv, KEY_SIZE);
            streamletId = keyPartHash % NSTREAMLETS + 1;
        } else {
            prevStreamletId = streamletId;
            streamletId = (++streamletId > NSTREAMLETS) ? 1 : streamletId; //next one
        }

        lastBatch = lastBatches[streamletId-1]; //points to last known

        //if last known batch exists and not closed then
        //take streamlet lock, check still not closed
        //append record if possible
        //release streamlet lock

        if(lastBatch != NULL && !lastBatch->closed) {
            SpinLock::Guard _(prodStreamletActiveLocks[streamletId%MAXNUMSTREAMLETS]);

            nextBatch = lastBatch;
            //append if possible
            if(!nextBatch->closed)
            {
                //do the append IF I have space for another record and its overhead todo enable next part
                if ((nextBatch->batchPart->length + objectBufferLength) <= BATCH_SIZE) {
                    //append the record
                    //======== [entryHeaderBuffer + content] our client buffer looks exactly like what we write on server

                    nextBatch->objects->get()->appendCopy(objectBuffer.get()->getRange(0, objectBufferLength), objectBufferLength);
                    nextBatch->batchPart->length += objectBufferLength;
                    nextBatch->batchPart->countEntries +=1;
//                    nextBatch->batchPart->lengthLastEntry = objectBufferLength;
                    totalRecords++;

                    //close it here if possible assuming records of 100 bytes and more
                    if(BATCH_SIZE - nextBatch->batchPart->length < objectBufferLength ) {
                        //compute checksum once for all records
                        Crc32C crc;
                        crc.update(nextBatch->objects->get()->getRange(recordsOffset, nextBatch->batchPart->length-recordsOffset), nextBatch->batchPart->length-recordsOffset);
                        nextBatch->batchPart->header.checksum = crc.getResult();

//                        fprintf(stdout, "======== Producer: recordsChecksum, recordsOffset, length, chunkChecksum ======= count=%d,%d,%d\n", recordsChecksum,
//                                recordsOffset, nextBatch->batchPart->length-recordsOffset, nextBatch->batchPart->header.checksum);
//                        fflush (stdout);

                        nextBatch->closed = true;
                        lastBatches[streamletId-1] = NULL;
                    }

                    continue;
                } else { //no more space, mark this closed;
                    //compute checksum
                    Crc32C crc;
                    crc.update(nextBatch->objects->get()->getRange(recordsOffset, nextBatch->batchPart->length-recordsOffset), nextBatch->batchPart->length-recordsOffset);
                    nextBatch->batchPart->header.checksum = crc.getResult();

//                    fprintf(stdout, "======== Producer: recordsChecksum, recordsOffset, length, chunkChecksum ======= count=%d,%d,%d\n", recordsChecksum,
//                            recordsOffset, nextBatch->batchPart->length-recordsOffset, nextBatch->batchPart->header.checksum);
//                    fflush (stdout);

                    nextBatch->closed = true;
                    lastBatches[streamletId-1] = NULL;
                }

                //VERSION pre-fill batch todo disable it - just for testing
                //do the append IF I have space for another record and its overhead
//                while ((nextBatch->batchPart->length + objectBufferLength) <= BATCH_SIZE) {
//                    //append the record
//                    //======== [entryHeaderBuffer + content] our client buffer looks exactly like what we write on server
//                    nextBatch->objects->get()->append(entryHeaderBuffer.get(), 0, entryHeaderBufferLength);
//                    //next also computes a checksum over the entry
////                    uint32_t checksum =
////                            Object::appendHeaderKeysAndValueToBuffer(*key.get(),
////                                    value, RECORD_SIZE,
////                                    nextBatch->objects->get(), false);
//                    //update WriteGroupBatchPart
//                    nextBatch->objects->get()->append(objectBuffer.get(), 0, objectBufferLength);
////                    nextBatch->objects->get()->appendCopy(objectBuffer.get()->getRange(0, objectBufferLength), objectBufferLength);
//                    nextBatch->batchPart->length += objectBufferLength;
//
//                    //update entry digest checksum
////                    nextBatch->chunkDigest->updateChecksum(checksum); //covers entry's checksum todo enable it
//                    nextBatch->chunkDigest->chunk->chunklength += objectBufferLength;
//                    nextBatch->chunkDigest->chunk->lengthLastEntry = objectBufferLength;
//                    nextBatch->chunkDigest->chunk->countEntries +=1;
//
//                    totalRecords++;
//
//                    //close it here if possible assuming records of 100 bytes and more
//                    if(BATCH_SIZE - nextBatch->batchPart->length < objectBufferLength ) {
//                        nextBatch->closed = true;
//                        break;
//                    }
//                }
////                 //no more space, mark this closed;
//                nextBatch->closed = true;
//                continue;
            }
        }

        //if last known batch exists and closed then take and reuse one new batch OR no known batch exists
        if(lastBatch == NULL ||  lastBatch->closed) {
            //get unused batch index
            if(prodBatchesReuseIndexesCached.size() > 0) {
                batchIndexReuse = prodBatchesReuseIndexesCached.front();
                prodBatchesReuseIndexesCached.pop();
            } else {
                while(batchIndexReuse == -1) {
                    takeCacheLock.construct(prodCacheLock); //protects prodBatchesReuseIndexes
                    //should drain prodBatchesReuseIndexes once and use a local cache
                    while (prodBatchesReuseIndexes.size() > 0) {
                        prodBatchesReuseIndexesCached.push(prodBatchesReuseIndexes.front());
                        prodBatchesReuseIndexes.pop();
                    }
                    takeCacheLock.destroy();

                    if (prodBatchesReuseIndexesCached.size() > 0) {
                        batchIndexReuse = prodBatchesReuseIndexesCached.front();
                        prodBatchesReuseIndexesCached.pop();
                    } else {
                        streamletId = prevStreamletId;
                        Cycles::sleep(1000); //give some time to writers
                        count++;
                    }
                }
            }

            //take a new batch and reset it
            nextBatch = prodBatchesReuse[batchIndexReuse];
            lastBatches[streamletId-1] = nextBatch;

            //reset nextBatch

            keyHash = (streamletId-1) * tabletRange;
            nextBatch->keyHash = keyHash;
            nextBatch->closed = false;
            nextBatch->objects->get()->resetFirstChunk();
            nextBatch->batchPart = nextBatch->objects->get()->emplaceAppend<WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>(0, streamletId);
            nextBatch->batchPart->length = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
            nextBatch->batchPart->countEntries = 0;
            nextBatch->numberObjectsAppended = 0; //this is taken from response, should match nextBatch->batchPart->countEntries
            nextBatch->creationTime = Cycles::toMicroseconds(Cycles::rdtsc());

            //append the record
            nextBatch->objects->get()->appendCopy(objectBuffer.get()->getRange(0, objectBufferLength), objectBufferLength);
            nextBatch->batchPart->length += objectBufferLength;
//            nextBatch->batchPart->lengthLastEntry = objectBufferLength;
            nextBatch->batchPart->countEntries = 1;
            totalRecords++;

            //give this batch to writers
            SpinLock::Guard _(prodStreamletActiveLocks[streamletId%MAXNUMSTREAMLETS]);
            prodStreamletBatches[streamletId].push_back(batchIndexReuse);
        }
    }

    while (!writerStop) {
        Cycles::sleep(1000000); //give time to writers to finish
    }
//    release resources
    fprintf(stdout, "======== cleaning&stopping produce_batches ======= count=%d\n",count);
    fflush (stdout);

    takeCacheLock.construct(prodCacheLock);

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        MultiWriteGroupBatchObjectE* nextBatch = prodBatchesReuse[b];
        nextBatch->objects->get()->reset();
    }

    takeCacheLock.destroy();
    producerStop = false;
    writerStop = false;

    fprintf(stdout, "======== done produce_batches =======\n");
    fflush (stdout);
}
catch (KerA::ClientException& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str());
    fprintf(stdout, "======== cleaning produce_batches =======\n");

    Tub<SpinLock::Guard> takeCacheLock;
    takeCacheLock.construct(prodCacheLock);

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        MultiWriteGroupBatchObjectE* nextBatch = prodBatchesReuse[b];
        nextBatch->objects->get()->reset();
    }

    takeCacheLock.destroy();
    producerStop = false;
    writerStop = false;

}
catch (KerA::Exception& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str());
    fprintf(stdout, "======== cleaning produce_batches =======\n");

    Tub<SpinLock::Guard> takeCacheLock;
    takeCacheLock.construct(prodCacheLock);

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        MultiWriteGroupBatchObjectE* nextBatch = prodBatchesReuse[b];
        nextBatch->objects->get()->reset();
    }

    takeCacheLock.destroy();
    producerStop = false;
    writerStop = false;

}

void write_batches(uint64_t streamId, RamCloud* ramcloud,
        uint32_t BATCH_SIZE, uint32_t NRECORDS, uint32_t NSTREAMLETS,
        uint32_t NNODES, uint32_t REQUEST_SIZE, uint64_t PRODID, uint64_t recordCount, uint64_t LINGERMS)
try
{
    fprintf(stdout, "========STARTING writes=======\n");
    fflush (stdout);

    //used for throttling
//    uint64_t startInsertSec = Cycles::rdtsc();
//    uint64_t stopInsertSec = Cycles::rdtsc();

    uint32_t totalRecords = 0;

    Tub<SpinLock::Guard> takeCacheLock;
    Tub<SpinLock::Guard> takeStreamletActiveLock;

    std::map<uint32_t, std::vector<uint32_t>> nodesToStreamletList;
    uint32_t nextNode = 1;

    //for each streamlet, associate the next node
    for(uint32_t streamletId=1; streamletId<=NSTREAMLETS; streamletId++)
    {
        if(nextNode > NNODES) {
            nextNode = 1;
        }
        if(!contains(nodesToStreamletList, nextNode)) {
            nodesToStreamletList[nextNode] = {};
        }
        nodesToStreamletList[nextNode].push_back(streamletId);
        nextNode++;
    }

    int countInFlight = 1; //todo make it configurable
    uint32_t recordsOffset = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);

    std::vector<MultiWriteGroupBatchObjectE*> batches[countInFlight];

    //allows to fill all then block until 1/2 are ready, then again
    WriteGroupBatchMultiRpcWrapper* requestsInFlight[countInFlight];
    std::deque<int> requestsIndexes; //available requestsInFlight
    std::deque<int> inFlightRequests;
    for(int i=0; i<countInFlight; i++) {
        requestsInFlight[i] = NULL;
        requestsIndexes.push_back(i);
    }

    int nextRequest = -1;
    int countNoRecords=0;
    std::queue<int> freeBatchIndexes;

    bool exit = false;
    while (!writerStop)
    {
        if(exit) {
            fprintf(stdout, "======== done write_batches =======\n");
            fflush (stdout);
            break;
        }

        //no batches to write
        //make some progress with cached requests
        while(requestsIndexes.size() == 0) {
            //do poll, maybe one request finished
            ramcloud->poll();

            //first check the oldest request
            int requestsInProgress = inFlightRequests.size();
            int currentRequest = -1;

            for (int j=0; j<requestsInProgress; j++) {
                currentRequest = inFlightRequests.front();
                inFlightRequests.pop_front();
                assert(requestsInFlight[currentRequest] != NULL);

                uint32_t numRequests = batches[currentRequest].size();
                bool requestReady = requestsInFlight[currentRequest]->isReady();
                if(requestReady) {
                    //allow reusing these batches
                    for(size_t i=0; i<numRequests; i++) {
                        MultiWriteGroupBatchObjectE* batch = batches[currentRequest][i];
                        // each request was processed -  STATUS_RETRY handled by MultiOpGroup
                        prodRecords += batch->numberObjectsAppended; //taken from responses
                        freeBatchIndexes.push(batch->streamletIdBatchIndex);
                    }

                    if(prodRecords >= recordCount) {
                        exit = true;
                    }

                    batches[currentRequest].clear();
                    delete requestsInFlight[currentRequest];
                    requestsInFlight[currentRequest] = NULL;

                    requestsIndexes.push_back(currentRequest);
                } else {
                    inFlightRequests.push_back(currentRequest);
                }
            }

            //give indexes back once
            SpinLock::Guard _(prodCacheLock);
            while(freeBatchIndexes.size()>0) {
                prodBatchesReuseIndexes.push(freeBatchIndexes.front());
                freeBatchIndexes.pop();
            }
        }

        nextRequest = requestsIndexes.front();
        requestsIndexes.pop_front();

        //take available batches for each streamlet

        //drain up to REQUEST_SIZE per node - considering all streamlets of a node
        // for each streamlet take at least one batch
        //MultiOpGroup has nBatches (requestSize) and nRpcs to launch in parallel
        //if nBatches * batchSize = REQUEST_SIZE and there is 1 node => synchronous RPC
        //on-the-fly requests == 1 for all nodes

        for(uint32_t nodeId=1; nodeId<=NNODES; nodeId++)
        {
            uint32_t nodeRequestSize = 0;
            bool nodeFilled = false;

            //next is used to increase the chance to take more than one batch thus reduce locks
            uint32_t countStreamletBatchesPerRequest = (REQUEST_SIZE/BATCH_SIZE)/nodesToStreamletList[nodeId].size();
            uint32_t count = 0;

            for (uint32_t streamletIdIndex = 0;
                    streamletIdIndex < nodesToStreamletList[nodeId].size();
                    streamletIdIndex++) {
                uint32_t streamletId =
                        nodesToStreamletList[nodeId][streamletIdIndex];

                SpinLock::Guard _(prodStreamletActiveLocks[streamletId%MAXNUMSTREAMLETS]);

                count = 0;
                while (prodStreamletBatches[streamletId].size() > 0
                        && count < countStreamletBatchesPerRequest) {
                    count++;
                    uint32_t streamletIdBatchIndex =
                            prodStreamletBatches[streamletId].front();

                    MultiWriteGroupBatchObjectE* nextBatch =
                            prodBatchesReuse[streamletIdBatchIndex];
//                        assert(streamletId == nextBatch->batchPart->streamletId);

                    //closed or not, each batch contains at least one record
                    if (nextBatch->closed
                            || (!nextBatch->closed
                                    && Cycles::toMicroseconds(Cycles::rdtsc())
                                            - nextBatch->creationTime
                                            >= LINGERMS)
                                            ) { //5msec equiv. linger.ms; should have at least one record
                        if (nodeRequestSize + nextBatch->batchPart->length
                                <= REQUEST_SIZE)
                        {
                            nodeRequestSize += nextBatch->batchPart->length;

                            //make sure checksum is computed
                            if(!nextBatch->closed)
                            {
                                //compute checksum
                                Crc32C crc;
                                crc.update(nextBatch->objects->get()->getRange(recordsOffset, nextBatch->batchPart->length-recordsOffset), nextBatch->batchPart->length-recordsOffset);
                                nextBatch->batchPart->header.checksum = crc.getResult();

//                                fprintf(stdout, "======== Producer: recordsChecksum, recordsOffset, length, chunkChecksum ======= count=%d,%d,%d\n", recordsChecksum,
//                                        recordsOffset, nextBatch->batchPart->length-recordsOffset, nextBatch->batchPart->header.checksum);
//                                fflush (stdout);
                            }

                            nextBatch->closed = true;
                            //save this index to restore in case of errors
                            nextBatch->streamletIdBatchIndex =
                                    streamletIdBatchIndex;
                            batches[nextRequest].push_back(nextBatch); //batchPart contains streamletId
                            prodStreamletBatches[streamletId].pop_front(); //it means we sent it
                        } else {
                            nodeFilled = true; //move to next node, this is one is filled already
                            break; //for
                        }
                    } //end if closed
                }
            }

            if(!nodeFilled && nodeRequestSize<REQUEST_SIZE) {
                for (uint32_t streamletIdIndex = 0;
                        streamletIdIndex < nodesToStreamletList[nodeId].size();
                        streamletIdIndex++) {
                    uint32_t streamletId =
                            nodesToStreamletList[nodeId][streamletIdIndex];

                    SpinLock::Guard _(prodStreamletActiveLocks[streamletId%MAXNUMSTREAMLETS]);

                    if (prodStreamletBatches[streamletId].size() > 0) {
                        uint32_t streamletIdBatchIndex =
                                prodStreamletBatches[streamletId].front();

                        MultiWriteGroupBatchObjectE* nextBatch =
                                prodBatchesReuse[streamletIdBatchIndex];
    //                        assert(streamletId == nextBatch->batchPart->streamletId);

                        if (nextBatch->closed
                                || (!nextBatch->closed
                                        && Cycles::toMicroseconds(Cycles::rdtsc())
                                                - nextBatch->creationTime
                                                >= LINGERMS)
                                                )
                        { //5msec equiv. linger.ms; should have at least one record
                            if (nodeRequestSize + nextBatch->batchPart->length
                                    <= REQUEST_SIZE) {
                                nodeRequestSize += nextBatch->batchPart->length;

                                //make sure checksum is computed
                                if(!nextBatch->closed)
                                {
                                    //compute checksum
                                    Crc32C crc;
                                    crc.update(nextBatch->objects->get()->getRange(recordsOffset, nextBatch->batchPart->length-recordsOffset), nextBatch->batchPart->length-recordsOffset);
                                    nextBatch->batchPart->header.checksum = crc.getResult();

//                                    fprintf(stdout, "======== Producer: recordsChecksum, recordsOffset, length, chunkChecksum ======= count=%d,%d,%d\n", recordsChecksum,
//                                            recordsOffset, nextBatch->batchPart->length-recordsOffset, nextBatch->batchPart->header.checksum);
//                                    fflush (stdout);
                                }

                                nextBatch->closed = true;
                                //save this index to restore in case of errors
                                nextBatch->streamletIdBatchIndex =
                                        streamletIdBatchIndex;
                                batches[nextRequest].push_back(nextBatch); //batchPart contains streamletId
                                prodStreamletBatches[streamletId].pop_front(); //it means we sent it
                            } else {
                                nodeFilled = true; //move to next node, this is one is filled already
                                break; //for
                            }
                        }//end if batch closed
                    }
                } //end for
            }
        }

        uint32_t numRequests = batches[nextRequest].size();

        if(numRequests == 0 ) {
            //do poll, maybe one request finished
            ramcloud->poll();

            //first check the oldest request
            int requestsInProgress = inFlightRequests.size();
            int currentRequest = inFlightRequests.front();

            for (int j=0; j<requestsInProgress; j++) {
                currentRequest = inFlightRequests.front();
                inFlightRequests.pop_front();
                assert(requestsInFlight[currentRequest] != NULL);

                uint32_t numRequests = batches[currentRequest].size();
                bool requestReady = requestsInFlight[currentRequest]->isReady();
                if(requestReady) {
                    //allow reusing these batches
                    for(size_t i=0; i<numRequests; i++) {
                        MultiWriteGroupBatchObjectE* batch = batches[currentRequest][i];
                        // each request was processed -  STATUS_RETRY handled by MultiOpGroup
                        prodRecords += batch->numberObjectsAppended; //taken from responses
                        freeBatchIndexes.push(batch->streamletIdBatchIndex);
                    }

                    if(prodRecords >= recordCount) {
                        exit = true;
                    }

                    batches[currentRequest].clear();
                    delete requestsInFlight[currentRequest];
                    requestsInFlight[currentRequest] = NULL;

                    requestsIndexes.push_back(currentRequest);
                } else {
                    inFlightRequests.push_back(currentRequest);
                }
            }
            //give indexes back once
            takeCacheLock.construct(prodCacheLock);
            while(freeBatchIndexes.size()>0) {
                prodBatchesReuseIndexes.push(freeBatchIndexes.front());
                freeBatchIndexes.pop();
            }
            takeCacheLock.destroy();

            requestsIndexes.push_back(nextRequest); //not used
            //give back the index
            Cycles::sleep(500); //give some time to producers
            countNoRecords++;
            continue;
        }

        //throttle, no more than NRECORDS/second
//        stopInsertSec = Cycles::rdtsc();
//        double time = static_cast<double>(Cycles::toMicroseconds(stopInsertSec - startInsertSec));
//
//        if (totalRecords >= NRECORDS && time <= 999999) { //check should we throttle
//            totalRecords = 0;
//            Cycles::sleep(1000000 - time); //up to 1s
//            startInsertSec = Cycles::rdtsc();
//        }
//
//        if (time > 999999) { //one second passed, reset throttle stuff?
//            totalRecords = 0; //maybe we produced less last second, reset counter
//            startInsertSec = stopInsertSec;
//        }

        //startRpcs is called internally
        //reusing requestsInFlight[nextRequest] MultiOpGroup object may bring some benefits todo

        //shuffle batches
        std::random_shuffle ( batches[nextRequest].begin(), batches[nextRequest].end() );

        requestsInFlight[nextRequest] = new WriteGroupBatchMultiRpcWrapper(ramcloud,
                reinterpret_cast<MultiWriteGroupBatchObject* const *>(&batches[nextRequest][0]),
                numRequests, streamId, PRODID);

        inFlightRequests.push_back(nextRequest);
    }

    for (int i = 0; i < countInFlight; i++) {
        delete requestsInFlight[i];
    }
    fprintf(stdout, "no records misses =%d\n",countNoRecords);
    fprintf(stdout, "======== done write_batches =======\n");

}
catch (KerA::ClientException& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str()); fflush (stdout);
}
catch (KerA::Exception& e) {
    fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str()); fflush (stdout);
}

JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppStartProducerThread(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();

    uint64_t producerId = buffer.read<uint64_t>(); //PRODID
    uint64_t streamId = buffer.read<uint64_t>();
    uint64_t recordCount = buffer.read<uint64_t>();

    uint32_t nNodes = buffer.read<uint32_t>(); //NNODES
    uint32_t nStreamlets = buffer.read<uint32_t>(); //NSTREAMLETS
    uint32_t batchStream = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart) + buffer.read<uint32_t>(); //BATCH_SIZE
    uint32_t requestSize = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart) + buffer.read<uint32_t>(); //REQUEST_SIZE
    uint32_t RECORD_SIZE = buffer.read<uint32_t>(); //RECORD_SIZE
    uint16_t KEY_SIZE = buffer.read<uint16_t>(); //KEY_SIZE

    //todo parametrize it
    uint32_t NRECORDS = 10000000; //when to throttle
    uint16_t PARTITIONER = 1; //partition r-r
    uint64_t LINGERMS = 1000; //microseconds

    uint64_t entryId = 0;

    try {
    for (size_t i = 0; i < MAXNUMSTREAMLETS; i++)
            prodStreamletActiveLocks[i].setName("streamlet::ActiveLock"+i);

    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        prodBuffers[b].construct();

        //preparing a batch for prodBatchesReuse - the request object
        MultiWriteGroupBatchObjectE* nextBatch =
                new MultiWriteGroupBatchObjectE(&prodBuffers[b], entryId++, -1); //keyHash==-1, to reset at reuse

        //the buffer holding headers and objects
        nextBatch->objects->get()->allocFirstChunkInternal(batchStream); //contiguous region, 100 extra for headers
        nextBatch->objects->get()->resetFirstChunk(); //use only batchStream

        //header of each chunk request
        nextBatch->batchPart = nextBatch->objects->get()->emplaceAppend<WireFormat::MultiOpGroup::Request::WriteGroupBatchPart>( 0, 0);
        //update WriteGroupBatchPart - after every object append; initially contains the WriteGroupBatchPart header
        nextBatch->batchPart->length = sizeof32(WireFormat::MultiOpGroup::Request::WriteGroupBatchPart);
        nextBatch->batchPart->streamletId = 0; //invalid, should be streamletId>0
        nextBatch->batchPart->countEntries = 0;
        nextBatch->numberObjectsAppended = 0; //this is taken from response, should match nextBatch->batchPart->countEntries

        prodBatchesReuse[b] = nextBatch;
        prodBatchesReuseIndexes.push(b);
    }

    for(uint32_t streamletId=1; streamletId<=nStreamlets; streamletId++) {
        prodStreamletBatches[streamletId] = {};
    }

    prodRecords = 0; //how many stream records written
    producerStop = false;
    writerStop = false;
    bool partitionerMode = (PARTITIONER > 0); //0 round robin , 1 key hash to streamlet

    //this thread is the source of the stream, continuously producing batches of records
    std::thread tproduce(produce_batches, streamId, batchStream, RECORD_SIZE, KEY_SIZE,
            nStreamlets, nNodes, partitionerMode, recordCount, producerId);

//    Cycles::sleep(7000000); //7s

    uint64_t startInsertSec = Cycles::rdtsc();
    uint64_t startInsert = Cycles::rdtsc();
    uint64_t stopInsertSec = Cycles::rdtsc();
    uint64_t lastNRecords = prodRecords;

    //next print throughput - disabled
    fprintf(stdout, "======== counting =======\n"); fflush (stdout);

    //this thread is responsible to send available batches
    //linger.ms = 0, i.e., we send a set of full batches of records (REQUEST_SIZE/BATCH_SIZE) but no more than REQUEST_SIZE (bytes)
    //once a batch/buffer is sent, it is also cleaned and put back to free prodBuffers
    std::thread twrite(write_batches, streamId, ramcloud, batchStream, NRECORDS,
            nStreamlets, nNodes, requestSize, producerId, recordCount, LINGERMS);

    twrite.detach();
    tproduce.detach();

    while (recordCount > lastNRecords) {
        uint64_t prevNRecords = lastNRecords;
        lastNRecords = prodRecords;
        stopInsertSec = Cycles::rdtsc();
        double time = static_cast<double>(Cycles::toMicroseconds(stopInsertSec - startInsertSec));
        fprintf(stdout, ">>> last seconds %.0f time %.0f records %lu total_records %lu \n",
                time, Cycles::toSeconds(stopInsertSec), lastNRecords-prevNRecords, lastNRecords);
        fflush (stdout);
        Cycles::sleep(999990); //1000ms
    }

    uint64_t stopInsert = Cycles::rdtsc();
    fprintf(stdout, ">>>>>>>>>>>>>total_last %.0f time %.0f records %.0f \n",
            static_cast<double>(Cycles::toMicroseconds(stopInsert - startInsert)),
            Cycles::toSeconds(stopInsert),
            static_cast<double>(prodRecords));
    fflush(stdout);

    writerStop = true;
    producerStop = true;

    while (producerStop && writerStop) {
        Cycles::sleep(1000000); //1000ms
    }

    }
    catch (KerA::ClientException& e) {
        fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str()); fflush (stdout);
    }
    catch (KerA::Exception& e) {
        fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str()); fflush (stdout);
    }

    fprintf(stdout, "======================ENDING producing=================\n");
    fflush(stdout);

    buffer.rewind();
    buffer.write<uint32_t>(0); //normally handled with EXCEPTION_CATCHER

}

static void printBuffer(ByteBuffer buffer, uint32_t len) {
    printf("\nBuffer %p, %u: \n%s\n",
           buffer.pointer,
           buffer.mark,
           Util::hexDump(
                   static_cast<void*>(buffer.pointer),
                   len).c_str());
}


/**
 * Create a RejectRules pointer from a byte array representation of a Java
 * RejectRules object.
 *
 * \param env
 *      The current JNI environment.
 * \param jRejectRules
 *      A Java byte array holding the data for a RejectRules struct.
 * \return A RejectRules object from the given byte array.
 */
static RejectRules
createRejectRules(JNIEnv* env, jbyteArray jRejectRules) {
    RejectRules out;
    void* rulesPointer = env->GetPrimitiveArrayCritical(jRejectRules, 0);
    out.givenVersion = static_cast<uint64_t*> (rulesPointer)[0];
    out.doesntExist = static_cast<char*> (rulesPointer)[8];
    out.exists = static_cast<char*> (rulesPointer)[9];
    out.versionLeGiven = static_cast<char*> (rulesPointer)[10];
    out.versionNeGiven = static_cast<char*> (rulesPointer)[11];
    env->ReleasePrimitiveArrayCritical(jRejectRules, rulesPointer, JNI_ABORT);
    /*
    printf("Created RejectRules:\n\tVersion: %u\n\tDE: %u\n\tE: %u\n\tVLG: %u\n\tVNG: %u\n",
           out.givenVersion,
           out.doesntExist,
           out.exists,
           out.versionLeGiven,
           out.versionNeGiven); */
    return out;
}

/**
 * Gets the pointer to the memory region the specified ByteBuffer points to.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param jByteBuffer
 *      The java.nio.ByteBuffer object, created with
 *      ByteBuffer.allocateDirect(), to retrieve a pointer for.
 * \return A pointer, as a Java long, to the memory region allocated for the
 *      specified ByteBuffer.
 */
JNIEXPORT jlong
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppGetByteBufferPointer(
        JNIEnv *env,
        jclass jRamCloud,
        jobject jByteBuffer) {
    jlong out = reinterpret_cast<jlong>(env->GetDirectBufferAddress(jByteBuffer));
    return out;
}

/**
 * Construct a RamCloud for a particular cluster.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          4 bytes for length of cluster locator string
 *          NULL-terminated string for cluster locator
 *          NULL-terminated string for cluster name
 *      The format for the output buffer is:
 *          4 bytes for status code of the RamCloud constructor
 *          8 bytes for a pointer to the created RamCloud object
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppConnect(
        JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    uint32_t locatorLength = buffer.read<uint32_t>();
    char* locator = buffer.pointer + buffer.mark;
    buffer.mark += 1 + locatorLength;
    char* name = buffer.pointer + buffer.mark;

    RamCloud* ramcloud = NULL;
    buffer.rewind();
    try {
        ramcloud = new RamCloud(locator, name);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(reinterpret_cast<uint64_t>(ramcloud));
}

/**
 * Disconnect from the RAMCloud cluster. This causes the JNI code to destroy
 * the underlying RamCloud C++ object.
 * 
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param ramcloudClusterHandle
 *      A pointer to the C++ RamCloud object.
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppDisconnect(JNIEnv *env,
        jclass jRamCloud,
        jlong ramcloudClusterHandle) {
    delete reinterpret_cast<RamCloud*> (ramcloudClusterHandle);
}

/**
 * Create a new table.
 * 
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          4 bytes for the number of servers across which this table will be
 *              divided
 *          NULL-terminated string for the name of the table to create
 *      The format for the output buffer is:
 *          4 bytes for the status code of the createTable operation
 *          8 bytes for the ID of the table created
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppCreateTable(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint32_t serverSpan = buffer.read<uint32_t>();
    char* tableName = buffer.pointer + buffer.mark;
    uint64_t tableId;
    buffer.rewind();
    try {
        tableId = ramcloud->createTable(tableName, serverSpan);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(tableId);
}

/**
 * Delete a table.
 *
 * All objects in the table are implicitly deleted, along with any
 * other information associated with the table.  If the table does
 * not currently exist then the operation returns successfully without
 * actually doing anything.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          NULL-terminated string for the name of the table to delete
 *      The format for the output buffer is:
 *          4 bytes for the status code of the dropTable operation
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppDropTable(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    char* tableName = buffer.pointer + buffer.mark;
    buffer.rewind();
    try {
        ramcloud->dropTable(tableName);
    } EXCEPTION_CATCHER(buffer);
}

/**
 * Given the name of a table, return the table's unique identifier, which
 * is used to access the table.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          NULL-terminated string for the name of the table to get the ID for
 *      The format for the output buffer is:
 *          4 bytes for the status code of the getTableId operation
 *          8 bytes for the ID of the table specified
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppGetTableId(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    char* tableName = buffer.pointer + buffer.mark;
    uint64_t tableId;
    buffer.rewind();
    try {
        tableId = ramcloud->getTableId(tableName);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(tableId);
}


#if TIME_CPP
uint32_t test_num_current = 0;
const uint32_t test_num_times = 100000;

uint64_t test_times[test_num_times];
#endif

/**
 * Read the current contents of an object.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to read from
 *          4 bytes for the length of the key to find
 *          byte array for the key to find
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the read operation
 *          8 bytes for the version of the read object
 *          4 bytes for the size of the read value
 *          byte array for the read value
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppRead(
        JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer byteBuffer(byteBufferPointer);
    RamCloud* ramcloud = byteBuffer.readPointer<RamCloud>();
    uint64_t tableId = byteBuffer.read<uint64_t>();
    uint32_t keyLength = byteBuffer.read<uint32_t>();
    void* key = byteBuffer.getVoidPointer(keyLength);
    RejectRules rejectRules = byteBuffer.read<RejectRules>();
    Buffer buffer;
    uint64_t version;
    byteBuffer.rewind();
#if TIME_CPP
    uint64_t start = Cycles::rdtsc();
#endif
    try {
        ramcloud->read(tableId,
                       key,
                       keyLength,
                       &buffer,
                       &rejectRules,
                       &version);
    } EXCEPTION_CATCHER(byteBuffer);
#if TIME_CPP
    test_times[test_num_current] = Cycles::rdtsc() - start;
    test_num_current++;
    if (test_num_current == test_num_times) {
        std::sort(boost::begin(test_times), boost::end(test_times));
        printf("Median C++ Read Time: %f\n", Cycles::toSeconds(test_times[test_num_times / 2]) * 1000000);
        test_num_current = 0;
    }
#endif
    byteBuffer.write(version);
    byteBuffer.write(buffer.size());
    buffer.copy(0, buffer.size(), byteBuffer.getVoidPointer());
}

/**
 * Delete an object from a table. If the object does not currently exist
 * then the operation succeeds without doing anything (unless rejectRules
 * causes the operation to be aborted).
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to remove from
 *          4 bytes for the length of the key to find and remove
 *          byte array for the key to find and remove
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the remove operation
 *          8 bytes for the version of the object just before deletion
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppRemove(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint64_t tableId = buffer.read<uint64_t>();
    uint32_t keyLength = buffer.read<uint32_t>();
    void* key = buffer.getVoidPointer(keyLength);
    RejectRules rejectRules = buffer.read<RejectRules>();
    uint64_t version;
    buffer.rewind();
    try {
        ramcloud->remove(tableId, key, keyLength, &rejectRules, &version);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(version);
}

/**
 * Replace the value of a given object, or create a new object if none
 * previously existed.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to write to
 *          4 bytes for the length of the key to write
 *          byte array for the key to write
 *          4 bytes for the length of the value to write
 *          byte array for the valule to write
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the write operation
 *          8 bytes for the version of the object written
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraProducerHandover_cppWrite(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint64_t tableId = buffer.read<uint64_t>();
    uint32_t keyLength = buffer.read<uint32_t>();
    void* key = buffer.getVoidPointer(keyLength);
    uint32_t valueLength = buffer.read<uint32_t>();
    void* value = buffer.getVoidPointer(valueLength);
    RejectRules rules = buffer.read<RejectRules>();
    uint64_t version;
    buffer.rewind();
#if TIME_CPP
    uint64_t start = Cycles::rdtsc();
#endif
    try {
        ramcloud->write(tableId,
                        key, keyLength,
                        value, valueLength,
                        &rules,
                        &version);
    } EXCEPTION_CATCHER(buffer);
#if TIME_CPP
    test_times[test_num_current] = Cycles::rdtsc() - start;
    test_num_current++;
    if (test_num_current == test_num_times) {
        std::sort(boost::begin(test_times), boost::end(test_times));
        printf("Median C++ Write Time: %f\n", Cycles::toSeconds(test_times[test_num_times / 2]) * 1000000);
        test_num_current = 0;
    }
#endif
    buffer.write(version);
}
