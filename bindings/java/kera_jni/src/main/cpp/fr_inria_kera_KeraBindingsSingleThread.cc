/* Copyright 2017-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "fr_inria_kera_KeraBindingsSingleThread.h"
#include "JavaCommon.h"

#include "Buffer.h"
#include "ClientException.h"
#include "Common.h"
#include "Cycles.h"
#include "Logger.h"
#include "LogMetadata.h"
#include "MultiRead.h"
#include "MultiReadGroup.h"
#include "MultiRemove.h"
#include "MultiWrite.h"
#include "RamCloud.h"
#include "Segment.h"
#include "SegmentIterator.h"
#include "ShortMacros.h"
#include "StreamObject.h"
#include "Tub.h"
#include "Util.h"
#include "WireFormat.h"
#include "WriteGroupMultiRpcWrapper.h"

#include "plasma/client.h"

#include <stdio.h>
#include <thread>
#include <list>
#include <iostream>
#include <sstream>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <algorithm>
#include <deque>

using namespace KerA;

namespace kst {
struct GroupSegmentOffset
{
    uint32_t streamletId;
    uint64_t groupId;
    uint64_t segmentId;
    uint32_t offset; //last object position to read from exclusive, ignored if -1
    uint32_t offsetIndex; //index into cached reusable offsets
    uint32_t pulledRecords; //n records pulled per segment
    bool segmentProcessed;
};

#define CACHE_SIZE 1000
#define MAXNUMSTREAMLETS 128

//key=streamletId, value is a list of indexes to batchesReuse; tail is first to be processed
//streamletBatches[streamletId] is protected by streamletActiveLocks[streamletId % MAXNUMSTREAMLETS]
std::map<uint32_t, std::deque<uint32_t>> streamletBatches;

//contains batch objects with objects' content in buffers[i]
//access is protected by streamletActiveLocks[streamletId%MAXNUMSTREAMLETS]
std::array<MultiReadGroupObject*, CACHE_SIZE> batchesReuse; //size of CACHE_SIZE

//here the consumers push processed object's indexes
//it is used by pullers
std::deque<uint32_t> batchesReuseIndexes;

std::atomic<uint64_t> records; //total records pulled and processed until now
std::atomic<bool> pullerStop;

Tub<Buffer> buffers[CACHE_SIZE];
Tub<GroupSegmentInfo> batchOffsets[CACHE_SIZE];

Tub<GroupSegmentOffset> readerOffsets[CACHE_SIZE];
std::deque<uint32_t> readerOffsetsReuseIndexes;

static const uint64_t INVALID_SEGMENT_ID = -1UL;
static const uint32_t INVALID_SEGMENT_OFFSET = -1U;

#define JNI_FALSE  0
#define JNI_TRUE   1

/**
 * Calls Java to read the results in the ByteBuffer and store them on the Java
 * heap, so that C++ can resume filling the buffer from the beginning without
 * fear of overwriting data that has not yet been stored in Java.
 */
bool flushBuffer(
        JNIEnv *env,
        const jobject &jRamCloud,
        const jclass &cls,
        const jmethodID &mid,
        ByteBuffer &buffer) {
//    jclass cls = env->GetObjectClass(jRamCloud);
//    jmethodID mid = env->GetMethodID(cls, "unloadBufferKera", "()Ljava/lang/String;");
    jint res = env->CallNonvirtualIntMethod(jRamCloud, cls, mid);
    buffer.rewind();
    return (res == JNI_TRUE);
}

} //end namespace kst

JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppStartPullThread(JNIEnv *env,
        jobject jRamCloud,
        jlong byteBufferPointer) {
    using namespace kst;
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();

    jclass cls = env->GetObjectClass(jRamCloud);
    jmethodID mid = env->GetMethodID(cls, "unloadBufferKera", "()I");

    uint64_t readerId = buffer.read<uint64_t>();
    uint64_t tableId1 = buffer.read<uint64_t>(); //streamID
    uint64_t recordCount = buffer.read<uint64_t>();
    uint32_t nNodes = buffer.read<uint32_t>();
    uint32_t NSTREAMLETS = buffer.read<uint32_t>();
    uint32_t NACTIVEGROUPS = buffer.read<uint32_t>();
    uint32_t BATCH_STREAM = buffer.read<uint32_t>();
    uint32_t streamletCount = buffer.read<uint32_t>();
    uint32_t NRECORDS = buffer.read<uint32_t>(); //used for throttle

    std::vector<uint32_t> streamletIds;
    std::vector<uint32_t> streamletAssignedIds;

    for (uint32_t si = 1; si <= streamletCount; si++) {
        uint32_t streamletId = buffer.read<uint32_t>();
        uint32_t streamletAssigned = buffer.read<uint32_t>();
        streamletIds.push_back(streamletId);
        if(streamletAssigned == static_cast<uint32_t>(1)) {
            streamletAssignedIds.push_back(streamletAssigned);
            streamletBatches[streamletId] = {};
        }
    }

    fprintf(stdout, "Started client. Params [readerId, streamId, nNodes, nStreamlets]: [%lu %lu %u %u]\n", readerId, tableId1, nNodes, NSTREAMLETS);
    fprintf(stdout, "Streamlets %u", streamletIds.size());
    fflush(stdout);

    records = 0;

    const uint16_t keysize = 9;
    const uint64_t fakeKeyHash = -1;
    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        buffers[b].construct();
        batchOffsets[b].construct();

        //only for cached reader offsets
        readerOffsets[b].construct();
        readerOffsetsReuseIndexes.push_back(b);

        MultiReadGroupObject* multiReadGroupObject =
                new MultiReadGroupObject(tableId1, fakeKeyHash, &buffers[b], batchOffsets[b].get());
        multiReadGroupObject->respvalue->get()->reset();

        batchesReuse[b] = multiReadGroupObject;
        batchesReuseIndexes.push_back(b);
    }
    pullerStop = false;

    fprintf(stdout, "Started pull_batches consume_batches\n");
    fflush (stdout);

    try {

        fprintf(stdout, "==========STARTING pull_batches===============\n");
        fflush (stdout);

        //puller cached last used offsets - used by next requests
        std::unordered_map<uint32_t,  //streamletId
            std::map<uint64_t, //groupId
            GroupSegmentOffset*> //last segment offset/s in order of segmentId
        > streamletIdGroupIdOffsets; //managing reader offsets

        std::unordered_map<uint32_t,  //streamletId
            std::map<uint64_t, //groupId
            AvailableSegments* //last segment offset/s in order of segmentId
        >> streamletIdGroupIdNextSegments; //managing reader offsets - it keeps a list of segment ids for [streamletId, groupId]

        for(uint32_t sindex=0; sindex<streamletIds.size(); sindex++)
        {
            streamletIdGroupIdOffsets[streamletIds[sindex]] = {}; //no group, no segment offset
            streamletIdGroupIdNextSegments[streamletIds[sindex]] = {}; //no group, no next segment/s
        }

        //used for throttle
        uint64_t startInsertSec = Cycles::rdtsc();
        uint64_t stopInsertSec = Cycles::rdtsc();
        uint32_t totalRecords = 0;

        uint64_t totalCountValueLength = 0;

        uint32_t streamletSpan = NSTREAMLETS; //number of streamlets
        uint64_t tabletRange = 1 + ~0UL / streamletSpan;
        uint64_t keyHash = -1; // (streamletId-1) * tabletRange;
        const uint32_t maxStreamletFetchBytes = 1000000; //not used; up to this n bytes per streamlet

//start pulling and consuming

    std::unordered_map<uint32_t,  //streamletId
        std::map<uint64_t, //groupId
            uint32_t> //index into batchesReuse
    > streamletIdGroupIdIndexes; //managing used batch indexes

    int countZeroes = 0;
    std::vector<MultiReadGroupObject*> batches; //to be pulled

        while (!pullerStop) //&& totalNumberObjectsPulled < recordCount
        {
        streamletIdGroupIdIndexes.clear();
        countZeroes = 0;

            //we should have at least one batch object for each streamlet's active group
            //each request is guaranteed to have one batch for each streamlet group
            if (batchesReuseIndexes.size() < NACTIVEGROUPS * streamletIds.size()) {
                Cycles::sleep(1000);
                continue;
            }

        batches.clear();

            //for each streamlet: check and take next available groupId/s - from group-offset cache or query
            for(uint32_t sindex=0; sindex<streamletIds.size(); sindex++)
            {
                uint32_t streamletId = streamletIds[sindex];
                keyHash = (streamletId-1) * tabletRange;

                uint32_t streamletGroupLength = streamletIdGroupIdOffsets[streamletId].size();

                //get next available [NACTIVEGROUPS - streamletGroupLength] groups
                if(NACTIVEGROUPS - streamletGroupLength > 0)
                {
                    Buffer groupsBuffer;
                    ramcloud->getNextAvailableGroupSegmentIds(tableId1,
                            streamletId, readerId, keyHash,
                            NACTIVEGROUPS - streamletGroupLength, &groupsBuffer);
                    uint32_t groupRespOffset = 0;
                    const WireFormat::GetNextAvailableGroupSegmentId::Response* groupRespHdr =
                            groupsBuffer.getOffset<WireFormat::GetNextAvailableGroupSegmentId::Response>(0);
                    groupRespOffset += sizeof32(*groupRespHdr);
                    uint32_t discoveredGroups = groupRespHdr->length;

                    for (uint32_t i=0; i<discoveredGroups; i++)
                    {
                        const GroupSegmentId* nextGroup = groupsBuffer.getOffset<GroupSegmentId>(groupRespOffset);
                    //nextGroup gives a groupId and max segmentId -> available segments 1..maxSegmentId
                        uint64_t nextGroupId = nextGroup->groupId;
                    if(!contains(streamletIdGroupIdOffsets[streamletId], nextGroupId)) {
                        streamletIdGroupIdOffsets[streamletId][nextGroupId] = NULL; //next offset to be initialized

                        //initialize available segments
                        //lastSegmentId was given to previous rpc multiRead up to maxSegmentId inclusive
                        AvailableSegments *as = new AvailableSegments();
                        as->firstSegmentId = 1;
                        as->lastSegmentId = nextGroup->segmentId; //last available segment
                        streamletIdGroupIdNextSegments[streamletId][nextGroupId] = as;
                    }
//                    RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>Discovered groupId %lu", nextGroupId );
                        groupRespOffset += sizeof32(*nextGroup);
                    }
                }

                //for each group obtain next available segment from group-offset cache or query it, update streamletIdGroupIdOffsets
                for (auto it = streamletIdGroupIdOffsets[streamletId].begin();
                        it != streamletIdGroupIdOffsets[streamletId].end(); ++it)
                {
                    uint64_t groupId = it->first;
                    uint64_t segmentId = INVALID_SEGMENT_ID; //it->second.front()->segmentId;
                    uint32_t offset = INVALID_SEGMENT_OFFSET; //it->second.front()->offset;

                    GroupSegmentOffset* segmentInfo = streamletIdGroupIdOffsets[streamletId][groupId]; //if I have a group-offset use it
                    if(segmentInfo != NULL)
                    {
                        segmentId = segmentInfo->segmentId;
                        offset = segmentInfo->offset;
                    }

                    //if segmentId invalid OR
                    //segmentId valid and segment processed => query getSegmentsByGroupId
                    if(segmentId == INVALID_SEGMENT_ID || (segmentInfo != NULL && segmentInfo->segmentProcessed))
                    {
                        AvailableSegments* as = contains(streamletIdGroupIdNextSegments[streamletId], groupId) ? streamletIdGroupIdNextSegments[streamletId][groupId] : NULL;
                        //4. first check streamletIdGroupIdNextSegments
                        if(as != NULL)
                        {
                            if(as->lastSegmentId >= as->firstSegmentId) {
                                uint64_t nextSegmentId = as->firstSegmentId;
                                as->firstSegmentId += 1;
                                if(as->firstSegmentId > as->lastSegmentId) {
                                    streamletIdGroupIdNextSegments[streamletId].erase(groupId);
                                }

                                if(segmentInfo == NULL) {
                                    uint32_t nextOffsetIndex = readerOffsetsReuseIndexes.front();
                                    segmentInfo = readerOffsets[nextOffsetIndex].get();
                                    segmentInfo->offsetIndex = nextOffsetIndex; // to be pushed back for reuse
                                    readerOffsetsReuseIndexes.pop_front();
                                    segmentInfo->streamletId = streamletId;
                                    segmentInfo->groupId = groupId;
                                    streamletIdGroupIdOffsets[streamletId][groupId] = segmentInfo;
                                }
                                segmentInfo->segmentProcessed = false;
                                segmentInfo->pulledRecords = 0;
                                segmentInfo->segmentId = nextSegmentId;
                                segmentInfo->offset = 0;
                                continue;
                            } else {
                                streamletIdGroupIdNextSegments[streamletId].erase(groupId);
                            }
                        }

                        //5. get next available segments by groupId and its current group-offset segment if one exists
                        bool useGroupOffset = segmentId != INVALID_SEGMENT_ID;
                        Buffer responseBufferSegments;
                        //should return segments starting from provided offset > lastOffset.segmentId
                        uint32_t groupSegmentsCount = ramcloud->getSegmentsByGroupId(
                                tableId1, streamletId, keyHash, groupId, useGroupOffset,
                                segmentId, offset, -1, &responseBufferSegments);

                        if(groupSegmentsCount == 0) {
                            continue; //this groupId has no new segments
                        }

                        //6. identify segments in responseBufferSegments
                        uint32_t respOffset = 0;
                        //first object in the response
                        const WireFormat::GetSegmentsByGroupId::Response* respHdr =
                                responseBufferSegments.getOffset<WireFormat::GetSegmentsByGroupId::Response>(respOffset);
                        respOffset = +sizeof32(*respHdr);

                        assert(respHdr->length == groupSegmentsCount);

                        //7. initialize streamletIdGroupIdOffsets
                        AvailableSegments *as1 = new AvailableSegments(); //first last available segments
                        SegmentIdAndOffset* next = responseBufferSegments.getOffset<SegmentIdAndOffset>(respOffset);
                        respOffset += sizeof32(*next);
                        as1->firstSegmentId = next->segmentId;
                        uint64_t nextSegmentId = as1->firstSegmentId;
                        as1->firstSegmentId += 1;
                        next = responseBufferSegments.getOffset<SegmentIdAndOffset>(respOffset);
                        as1->lastSegmentId = next->segmentId; //last available segment

                        if(as1->lastSegmentId >= as1->firstSegmentId) {
                            streamletIdGroupIdNextSegments[streamletId][groupId] = as1;
                        }

                        if(segmentInfo == NULL) {
                            uint32_t nextOffsetIndex = readerOffsetsReuseIndexes.front();
                            segmentInfo = readerOffsets[nextOffsetIndex].get();
                            segmentInfo->offsetIndex = nextOffsetIndex; // to be pushed back for reuse
                            readerOffsetsReuseIndexes.pop_front();
                            segmentInfo->streamletId = streamletId;
                            segmentInfo->groupId = groupId;
                            streamletIdGroupIdOffsets[streamletId][groupId] = segmentInfo;
                        }
                        segmentInfo->segmentProcessed = false;
                        segmentInfo->pulledRecords = 0;
                        segmentInfo->segmentId = nextSegmentId;
                        segmentInfo->offset = 0;
                    }

                    if(segmentInfo == NULL) {
                        continue;
                    }

                    //take batch
                    if(batchesReuseIndexes.size() == 0) {
                        continue;
                    }

                    uint32_t batchIndex = batchesReuseIndexes.front();
                    batchesReuseIndexes.pop_front(); //exclusive access

                    MultiReadGroupObject* nextBatch = batchesReuse[batchIndex];
                    streamletIdGroupIdIndexes[streamletId][groupId] = batchIndex;
                    //we push batchIndex to streamletBatches after pulling succeeded

                    //do reset nextBatch
                    nextBatch->isSegmentClosed = false;
                    nextBatch->isGroupProcessed = false;
                    nextBatch->nextOffset = 0;
                    nextBatch->numberObjectsRead = 0;
                    nextBatch->numberOfObjectEntries = 0;
                    nextBatch->responseLength = 0;
                    nextBatch->responseOffset = 0;
                    nextBatch->respvalue->get()->reset(); //done in MultiReadGroup: destroy & reset
                    nextBatch->keyHash = keyHash; //has to reflect streamletId and streamletSpan
                    nextBatch->segmentInfo->streamletId = streamletId;
                    nextBatch->segmentInfo->groupId = groupId;
                    nextBatch->segmentInfo->segmentId = segmentInfo->segmentId;
                    nextBatch->segmentInfo->offset = segmentInfo->offset;
                    nextBatch->segmentInfo->maxObjects = 100000;
                    nextBatch->segmentInfo->maxResponseLength = BATCH_STREAM; //batch size

                    batches.push_back(nextBatch);
                }
            }

            if(batches.size() == 0) {
                Cycles::sleep(1000);
                continue;
            }

            //build request multi-read with head segmentIdAndOffset for each [streamletId,groupId]
            //pull, check segment not closed, every time make sure each streamlet is pulled, else check again for segments/groups
            //always take first group/first segment

            std::random_shuffle ( batches.begin(), batches.end() );
            uint32_t requestsSize = batches.size();

            ramcloud->multiReadGroup(reinterpret_cast<MultiReadGroupObject**>(&batches[0]), 
            requestsSize, maxStreamletFetchBytes,
                    NULL, static_cast<uint32_t>(0), static_cast<uint64_t>(0), static_cast<uint64_t>(0), false);

            //check each object - maybe segment processed or group processed
            for(uint32_t sindex=0; sindex<requestsSize; sindex++)
            {
                MultiReadGroupObject* nextBatch = batches[sindex];
                uint32_t streamletId = nextBatch->segmentInfo->streamletId;
                uint64_t groupId = nextBatch->segmentInfo->groupId;
                GroupSegmentOffset* streamletGroupOffset = streamletIdGroupIdOffsets[streamletId][groupId];

//            keyHash = (streamletId-1) * tabletRange;
//            assert(keyHash == nextBatch->keyHash);
//            assert(groupId == streamletGroupOffset->groupId);
//            assert(streamletGroupOffset->segmentId == nextBatch->segmentInfo->segmentId);

                //9. get and process objects from response
//                totalNumberObjectsPulled += nextBatch->numberObjectsRead;
                streamletGroupOffset->pulledRecords += nextBatch->numberObjectsRead;
                // total number objects read per segment to avoid numberObjectsRead to be 0

    //            fprintf(stdout, ">>> Current Group Offset [segmentId-offset-groupId]: %lu %u %lu SegmLength %u Entries %u \n",
    //                    nextBatch->segmentInfo->segmentId, nextBatch->segmentInfo->offset, groupId,
    //                    streamletIdGroupIdOffsets[streamletId][groupId].front()->pulledRecords,
    //                    nextBatch->numberOfObjectEntries);

//            if(nextBatch->isSegmentClosed) {
//                assert(streamletGroupOffset->pulledRecords == nextBatch->numberOfObjectEntries);
//                assert(nextBatch->numberOfObjectEntries > 0);
//            }

                //mark this segment processed and closed
                //so next step involves another getSegmentsByGroupId query
                streamletGroupOffset->segmentProcessed = nextBatch->isSegmentClosed
                        && (streamletGroupOffset->pulledRecords == nextBatch->numberOfObjectEntries); //segmentProcessedAndClosed;

                if(nextBatch->numberObjectsRead == static_cast<uint32_t>(0)
                        && !streamletGroupOffset->segmentProcessed) {
                countZeroes++;
                    continue;
                }

//            if(streamletGroupOffset->offset > 0 && nextBatch->nextOffset == 0) { //see Stream multiread
//                assert(false); //our invalid offset went past segment head
//            }
            //move segment offset to last pulled record's offset
            if(nextBatch->nextOffset > 0) {
                streamletGroupOffset->offset = nextBatch->nextOffset;
            }

            // if segment processed and segment is last in group
            if(streamletGroupOffset->segmentProcessed && nextBatch->isGroupProcessed)
            {
//                    fprintf(stdout, ">>>>>>>>>>>>>groupProcessed [streamletId,groupId] [%lu,%lu] \n", streamletId, groupId);
//                    fflush(stdout);
                    //remove groupId and give back index
                    readerOffsetsReuseIndexes.push_back(streamletGroupOffset->offsetIndex);
                    streamletIdGroupIdOffsets[streamletId].erase(groupId);
                    streamletIdGroupIdNextSegments[streamletId].erase(groupId);
            }

            }

        //from streamletIdGroupIdIndexes give back indexes to consumers - update streamletBatches!
        for (auto its = streamletIdGroupIdIndexes.begin();
                its != streamletIdGroupIdIndexes.end(); ++its)
        {
            uint32_t streamletId = its->first;
            for (auto it = its->second.begin();
                    it != its->second.end(); ++it)
            {
                uint32_t batchIndex = it->second; //it->first is groupId
                streamletBatches[streamletId].push_back(batchIndex);
            }
        }


//START CONSUMING BATCHES
            //consume available batches for each streamlet

            uint32_t batchesCount = 0; //how many batches in buffer
            //batchesCount requestSize is <= SHARED_JAVA_BUFFER_SIZE
            //for each batch:
            //streamletId
             //numberResults
            //1..* keyLength key valueLength value
            buffer.rewind(); //reset buffer position
            buffer.write(batchesCount);
            uint32_t iterRecordsPosition = buffer.mark;
            uint32_t lastBufferPosition = buffer.mark;

            //take next nActiveGroups available batches for each streamlet, process them, and release their indexes back to puller
            for (uint32_t sindex=0; sindex < streamletIds.size(); sindex++)
            {
                uint32_t streamletId = streamletIds[sindex];

                uint32_t batchesReuseFrontIndex = -1;
                uint32_t activeGroups = NACTIVEGROUPS;
                bool streamletDone = false;

                while(activeGroups>0 && !streamletDone)
                {
                    MultiReadGroupObject* nextBatch = NULL;
                    activeGroups--;
                    if(streamletBatches[streamletId].size() > 0)
                    {
                        batchesReuseFrontIndex = streamletBatches[streamletId].front();
                        nextBatch = batchesReuse[batchesReuseFrontIndex];
                        assert(streamletId == nextBatch->segmentInfo->streamletId);
                        streamletBatches[streamletId].pop_front(); //it means we consumed it
                    } else {
                        streamletDone = true;
                    }

                    if(nextBatch != NULL)
                    {
                        uint32_t numberObjectsRead = nextBatch->numberObjectsRead;

                        if(numberObjectsRead != static_cast<uint32_t>(0))
                        {
                            batchesCount += 1;

                            Buffer* responseBuffer = nextBatch->respvalue->get(); // shared buffer
                            assert(responseBuffer != NULL);

                            uint32_t respOffset = 0;
                            uint32_t totalLength = *responseBuffer->getOffset<uint32_t>(respOffset);
                            respOffset += sizeof32(uint32_t); // length
                            //SegmentCertificate sc; // take this from responseBuffer as well
                            //sc.segmentLength = totalLength;
                            SegmentIterator it(responseBuffer->getRange(respOffset, totalLength), totalLength);

                            buffer.write(streamletId);
                            iterRecordsPosition = buffer.mark; //right after streamletId follows iterRecords
                            uint32_t iterRecords = 0;
                            buffer.write(iterRecords); //holder for number of objects

                            StreamObject obj0(tableId1, *responseBuffer);
                            //uint32_t headerGroupSize = sizeof32(StreamObject::HeaderGroup);
                            while (!it.isDone())
                            {
                                respOffset += it.getEntryOffset();
                                uint32_t itLength = it.getLength();
                                
                                if (it.getType() != LOG_ENTRY_TYPE_CHUNK) {
                                    obj0.reset(respOffset, itLength);
                                    /*
                                    StreamObject obj0(tableId1, 1, 0, *responseBuffer,
                                            respOffset + sizeof32(StreamObject::HeaderGroup),
                                            it.getLength() - sizeof32(StreamObject::HeaderGroup));
                                    fprintf(stdout,
                                            ">>> Latency: key=%s value=%s now=%lu\n",
                                            string(reinterpret_cast<const char*>(obj0.getKey()), obj0.getKeyLength()).c_str(),
                                            string(reinterpret_cast<const char*>(obj0.getValue()), obj0.getValueLength()).c_str(),
                                            Cycles::toMicroseconds(Cycles::rdtsc()));
                                    */
                                    //if no more space for next record
                                    uint32_t keyLength = obj0.getKeyLength();
                                    uint32_t valueLength = obj0.getValueLength();

                                    assert(buffer.mark + 8 + keyLength + valueLength <= SHARED_JAVA_BUFFER_SIZE);

                                    //key
                                    buffer.write(keyLength);
                                    memcpy(buffer.getVoidPointer(), obj0.getKey(), keyLength);
                                    buffer.mark += keyLength;
                                    //value
                                    buffer.write(valueLength);
                                    memcpy(buffer.getVoidPointer(), obj0.getValue(), valueLength);
                                    buffer.mark += valueLength;

                                    totalCountValueLength += valueLength;
                                    iterRecords++;
                                }
                                respOffset += itLength;
                                it.next();
                            }
                            
                            lastBufferPosition = buffer.mark;
                            buffer.mark = iterRecordsPosition;
                            buffer.write(iterRecords); //update
                            buffer.mark = lastBufferPosition; //so we can continue with next batch

                            //fprintf(stdout, "flushBuffer %d", iterRecords);
                            //assert(iterRecords == numberObjectsRead);
                            records += numberObjectsRead;
                            totalRecords += numberObjectsRead;

                            //throttle, no more than NRECORDS/second
                            stopInsertSec = Cycles::rdtsc();
                            double time = static_cast<double>(Cycles::toMicroseconds(stopInsertSec - startInsertSec));

                            if (totalRecords >= NRECORDS && time <= 999999) { //check should we throttle
                                totalRecords = 0;
                                Cycles::sleep(1000000 - time); //100us
                                startInsertSec = Cycles::rdtsc();
                            }
                            if (time > 999999) { //one second passed, reset throttle stuff?
                                totalRecords = 0; //maybe we produced less last second, reset counter
                                startInsertSec = stopInsertSec;
                            }

                            if(records >= recordCount) {
                                pullerStop = true;
                                break;
                            }
                        }
                        //give back nextBatch's index
                        batchesReuseIndexes.push_back(batchesReuseFrontIndex);
                    }
                }
            } //end loop batches
            //next unload buffer
            assert(buffer.mark <= SHARED_JAVA_BUFFER_SIZE);

            buffer.mark = 0;
            buffer.write(batchesCount);

            if(flushBuffer(env, jRamCloud, cls, mid, buffer)) {
                pullerStop = true;
                break;
            }

            if(records >= recordCount) {
                pullerStop = true;
                fprintf(stdout,
                        "======== exiting write_batches totalValueLength %lu =======\n",
                        totalCountValueLength);
                fflush(stdout);
                break;
            }
            //end consumer
        }

        fprintf(stdout, "==========done pull_batches===============\n");
        fflush (stdout);
    }
    catch (KerA::ClientException& e) {
        fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str()); fflush (stdout);
    }
    catch (KerA::Exception& e) {
        fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str()); fflush (stdout);
    }

    fprintf(stdout, "======================ENDING reading=================\n");
    fflush(stdout);

    buffer.rewind();
    buffer.write<uint32_t>(0); //normally handled with EXCEPTION_CATCHER

}

static void printBuffer(ByteBuffer buffer, uint32_t len) {
    printf("\nBuffer %p, %u: \n%s\n",
           buffer.pointer,
           buffer.mark,
           Util::hexDump(
                   static_cast<void*>(buffer.pointer),
                   len).c_str());
}


/**
 * Create a RejectRules pointer from a byte array representation of a Java
 * RejectRules object.
 *
 * \param env
 *      The current JNI environment.
 * \param jRejectRules
 *      A Java byte array holding the data for a RejectRules struct.
 * \return A RejectRules object from the given byte array.
 */
static RejectRules
createRejectRules(JNIEnv* env, jbyteArray jRejectRules) {
    RejectRules out;
    void* rulesPointer = env->GetPrimitiveArrayCritical(jRejectRules, 0);
    out.givenVersion = static_cast<uint64_t*> (rulesPointer)[0];
    out.doesntExist = static_cast<char*> (rulesPointer)[8];
    out.exists = static_cast<char*> (rulesPointer)[9];
    out.versionLeGiven = static_cast<char*> (rulesPointer)[10];
    out.versionNeGiven = static_cast<char*> (rulesPointer)[11];
    env->ReleasePrimitiveArrayCritical(jRejectRules, rulesPointer, JNI_ABORT);
    /*
    printf("Created RejectRules:\n\tVersion: %u\n\tDE: %u\n\tE: %u\n\tVLG: %u\n\tVNG: %u\n",
           out.givenVersion,
           out.doesntExist,
           out.exists,
           out.versionLeGiven,
           out.versionNeGiven); */
    return out;
}

/**
 * Gets the pointer to the memory region the specified ByteBuffer points to.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param jByteBuffer
 *      The java.nio.ByteBuffer object, created with
 *      ByteBuffer.allocateDirect(), to retrieve a pointer for.
 * \return A pointer, as a Java long, to the memory region allocated for the
 *      specified ByteBuffer.
 */
JNIEXPORT jlong
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppGetByteBufferPointer(
        JNIEnv *env,
        jclass jRamCloud,
        jobject jByteBuffer) {
    jlong out = reinterpret_cast<jlong>(env->GetDirectBufferAddress(jByteBuffer));
    return out;
}

/**
 * Construct a RamCloud for a particular cluster.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          4 bytes for length of cluster locator string
 *          NULL-terminated string for cluster locator
 *          NULL-terminated string for cluster name
 *      The format for the output buffer is:
 *          4 bytes for status code of the RamCloud constructor
 *          8 bytes for a pointer to the created RamCloud object
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppConnect(
        JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    uint32_t locatorLength = buffer.read<uint32_t>();
    char* locator = buffer.pointer + buffer.mark;
    buffer.mark += 1 + locatorLength;
    char* name = buffer.pointer + buffer.mark;

    RamCloud* ramcloud = NULL;
    buffer.rewind();
    try {
        ramcloud = new RamCloud(locator, name);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(reinterpret_cast<uint64_t>(ramcloud));
}

/**
 * Disconnect from the RAMCloud cluster. This causes the JNI code to destroy
 * the underlying RamCloud C++ object.
 * 
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param ramcloudClusterHandle
 *      A pointer to the C++ RamCloud object.
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppDisconnect(JNIEnv *env,
        jclass jRamCloud,
        jlong ramcloudClusterHandle) {
    delete reinterpret_cast<RamCloud*> (ramcloudClusterHandle);
}

/**
 * Create a new table.
 * 
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          4 bytes for the number of servers across which this table will be
 *              divided
 *          NULL-terminated string for the name of the table to create
 *      The format for the output buffer is:
 *          4 bytes for the status code of the createTable operation
 *          8 bytes for the ID of the table created
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppCreateTable(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint32_t serverSpan = buffer.read<uint32_t>();
    uint32_t streamletSpan = buffer.read<uint32_t>();
    char* tableName = buffer.pointer + buffer.mark;
    uint64_t tableId;
    buffer.rewind();
    try {
        tableId = ramcloud->createTable(tableName, serverSpan, streamletSpan);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(tableId);
}

JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppGetLocalStreamlets(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint64_t streamId = buffer.read<uint64_t>();
    uint32_t numberConsumersLocal = buffer.read<uint32_t>();
    uint32_t indexOfThisSubtask = buffer.read<uint32_t>();

    buffer.rewind();
    uint32_t streamletCount = 0;
    Buffer response;

    uint64_t serverId = 0; //unknown, take it from plasma
    plasma::PlasmaClient plasmaClient;
    ARROW_CHECK_OK(plasmaClient.Connect("/tmp/plasma", ""));
    std::vector<plasma::ObjectID> object_ids;
    std::vector<plasma::ObjectBuffer> object_buffers;
    plasma::ObjectID objectServerId = plasma::ObjectID::from_binary("00000000000000000001");
    plasma::ObjectID objectCountLocalConsumersId = plasma::ObjectID::from_binary("00000000000000000002");

    bool objectExists = false;
    std::set<uint32_t> consumerIndexes; //ordered consumer indexes - their position indicates the partition
    consumerIndexes.insert(indexOfThisSubtask);

    uint32_t indexPartition = 1; //unique consumer

    try {
        while(true) {
            ARROW_CHECK_OK(plasmaClient.Contains(objectServerId, &objectExists));
            if(objectExists) {
                object_ids.push_back(objectServerId);
                ARROW_CHECK_OK(plasmaClient.Get(object_ids, -1, &object_buffers));
                Tub<Buffer> objBuffer; //used to manipulate current object
                objBuffer.construct();
                objBuffer.get()->appendExternal(object_buffers[0].data->data(), sizeof32(uint64_t));
                serverId = *objBuffer.get()->getOffset<uint64_t>(0);
            } else {
                continue; //need to wait for Master live
            }
            streamletCount = ramcloud->getLocalStreamlets(streamId, serverId, &response);

            //Master takes this to optimize for many local consumers
            //first try to create object 004 on partition 0 with numberConsumersLocal
            ARROW_CHECK_OK(plasmaClient.CreateObjectCountLocalConsumers(numberConsumersLocal));

            //then create consumerObject with indexOfThisSubtask
            char objectKey[20];
            Util::genRandomString(objectKey, 20);
            string objectKeyStr(objectKey);
            plasma::ObjectID consumerIndexObject = plasma::ObjectID::from_binary(objectKeyStr);
            ARROW_CHECK_OK(plasmaClient.CreateObjectConsumerIndex(consumerIndexObject, indexOfThisSubtask));


            //subscribe on partition 0 to get:
            //numberConsumersLocalx(CreateObjectConsumerIndex) + 1x(CreateObjectCountLocalConsumers)+1x(for objectServerId) objects
            //order subtask indexes and get my position

            int partitionNotify = 0;
            int fd;
            ARROW_CHECK_OK(plasmaClient.Subscribe(&fd, partitionNotify));

            //goal is to determine the Main Consumer responsible to submit multiRead request
            //before submitting multiRead request we make sure all consumers configured
            //control objects are received in order (below while makes sure all consumers get configured)
            //partition 0 further receives iteration control objects from consumer to Master
            uint32_t countConsumers = numberConsumersLocal + 2; //2 = ObjectCountLocalConsumers + objectServerId

            while(countConsumers > 0) {
                countConsumers--;

                plasma::ObjectID control_object_id;
                int64_t co_data_size;
                int64_t co_metadata_size;

                ARROW_CHECK_OK(plasmaClient.GetNotification(fd,  &control_object_id, &co_data_size, &co_metadata_size));

                if(control_object_id == objectServerId || control_object_id == objectCountLocalConsumersId) {
                    continue;
                } else {
                    object_ids.clear();
                    object_ids.push_back(control_object_id);
                    object_buffers.clear();
                    ARROW_CHECK_OK(plasmaClient.Get(object_ids, -1, &object_buffers));
                    Tub<Buffer> objBuffer; //used to manipulate current object
                    objBuffer.construct();
                    objBuffer.get()->appendExternal(object_buffers[0].data->data(), sizeof32(uint32_t));
                    uint32_t otherConsumerIndex = *objBuffer.get()->getOffset<uint32_t>(0);
                    consumerIndexes.insert(otherConsumerIndex);
                }
            }

            break;
        }
    } EXCEPTION_CATCHER(buffer);

    buffer.write(streamletCount);

    uint32_t bufferOffset = 0;
    uint32_t assigned = 1; //all local partitions gets assigned

    for (std::set<uint32_t>::iterator i = consumerIndexes.begin(); i != consumerIndexes.end(); i++) {
       if(*i == indexOfThisSubtask) {
           break;
       }
       indexPartition++;
    }
    buffer.write(indexPartition);

    //based on position we split equally the streamlets between numberConsumersLocal
    uint32_t pos = 0;
    while(streamletCount>0) {
        if(pos >= numberConsumersLocal) {
            pos = 0;
        }
        pos++;

        uint32_t* streamletId = response.getOffset<uint32_t>(bufferOffset);
        buffer.write(*streamletId);

        assigned = (pos == indexPartition);

        buffer.write(assigned);
        bufferOffset += sizeof32(uint32_t);
        streamletCount--;
    }

}

/**
 * Delete a table.
 *
 * All objects in the table are implicitly deleted, along with any
 * other information associated with the table.  If the table does
 * not currently exist then the operation returns successfully without
 * actually doing anything.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          NULL-terminated string for the name of the table to delete
 *      The format for the output buffer is:
 *          4 bytes for the status code of the dropTable operation
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppDropTable(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    char* tableName = buffer.pointer + buffer.mark;
    buffer.rewind();
    try {
        ramcloud->dropTable(tableName);
    } EXCEPTION_CATCHER(buffer);
}

/**
 * Given the name of a table, return the table's unique identifier, which
 * is used to access the table.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          NULL-terminated string for the name of the table to get the ID for
 *      The format for the output buffer is:
 *          4 bytes for the status code of the getTableId operation
 *          8 bytes for the ID of the table specified
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppGetTableId(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    char* tableName = buffer.pointer + buffer.mark;
    uint64_t tableId;
    buffer.rewind();
    try {
        tableId = ramcloud->getTableId(tableName);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(tableId);
}


#if TIME_CPP
uint32_t test_num_current = 0;
const uint32_t test_num_times = 100000;

uint64_t test_times[test_num_times];
#endif

/**
 * Read the current contents of an object.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to read from
 *          4 bytes for the length of the key to find
 *          byte array for the key to find
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the read operation
 *          8 bytes for the version of the read object
 *          4 bytes for the size of the read value
 *          byte array for the read value
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppRead(
        JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer byteBuffer(byteBufferPointer);
    RamCloud* ramcloud = byteBuffer.readPointer<RamCloud>();
    uint64_t tableId = byteBuffer.read<uint64_t>();
    uint32_t keyLength = byteBuffer.read<uint32_t>();
    void* key = byteBuffer.getVoidPointer(keyLength);
    RejectRules rejectRules = byteBuffer.read<RejectRules>();
    Buffer buffer;
    uint64_t version;
    byteBuffer.rewind();
#if TIME_CPP
    uint64_t start = Cycles::rdtsc();
#endif
    try {
        ramcloud->read(tableId,
                       key,
                       keyLength,
                       &buffer,
                       &rejectRules,
                       &version);
    } EXCEPTION_CATCHER(byteBuffer);
#if TIME_CPP
    test_times[test_num_current] = Cycles::rdtsc() - start;
    test_num_current++;
    if (test_num_current == test_num_times) {
        std::sort(boost::begin(test_times), boost::end(test_times));
        printf("Median C++ Read Time: %f\n", Cycles::toSeconds(test_times[test_num_times / 2]) * 1000000);
        test_num_current = 0;
    }
#endif
    byteBuffer.write(version);
    byteBuffer.write(buffer.size());
    buffer.copy(0, buffer.size(), byteBuffer.getVoidPointer());
}

/**
 * Delete an object from a table. If the object does not currently exist
 * then the operation succeeds without doing anything (unless rejectRules
 * causes the operation to be aborted).
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to remove from
 *          4 bytes for the length of the key to find and remove
 *          byte array for the key to find and remove
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the remove operation
 *          8 bytes for the version of the object just before deletion
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppRemove(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint64_t tableId = buffer.read<uint64_t>();
    uint32_t keyLength = buffer.read<uint32_t>();
    void* key = buffer.getVoidPointer(keyLength);
    RejectRules rejectRules = buffer.read<RejectRules>();
    uint64_t version;
    buffer.rewind();
    try {
        ramcloud->remove(tableId, key, keyLength, &rejectRules, &version);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(version);
}

/**
 * Replace the value of a given object, or create a new object if none
 * previously existed.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to write to
 *          4 bytes for the length of the key to write
 *          byte array for the key to write
 *          4 bytes for the length of the value to write
 *          byte array for the valule to write
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the write operation
 *          8 bytes for the version of the object written
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsSingleThread_cppWrite(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint64_t tableId = buffer.read<uint64_t>();
    uint32_t keyLength = buffer.read<uint32_t>();
    void* key = buffer.getVoidPointer(keyLength);
    uint32_t valueLength = buffer.read<uint32_t>();
    void* value = buffer.getVoidPointer(valueLength);
    RejectRules rules = buffer.read<RejectRules>();
    uint64_t version;
    buffer.rewind();
#if TIME_CPP
    uint64_t start = Cycles::rdtsc();
#endif
    try {
        ramcloud->write(tableId,
                        key, keyLength,
                        value, valueLength,
                        &rules,
                        &version);
    } EXCEPTION_CATCHER(buffer);
#if TIME_CPP
    test_times[test_num_current] = Cycles::rdtsc() - start;
    test_num_current++;
    if (test_num_current == test_num_times) {
        std::sort(boost::begin(test_times), boost::end(test_times));
        printf("Median C++ Write Time: %f\n", Cycles::toSeconds(test_times[test_num_times / 2]) * 1000000);
        test_num_current = 0;
    }
#endif
    buffer.write(version);
}
