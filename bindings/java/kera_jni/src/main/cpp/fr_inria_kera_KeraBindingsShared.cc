/* Copyright 2020-2021 Inria
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//this version was tested in the middle just before optimizations - currently unused not updated

#include "fr_inria_kera_KeraBindingsShared.h"
#include "JavaCommon.h"

#include "SegmentIterator.h"
#include "LogMetadata.h"
#include "Segment.h"
#include "Logger.h"
#include "Common.h"
#include "Cycles.h"
#include "Buffer.h"
#include "StreamObject.h"
#include "ShortMacros.h"
#include "ClientException.h"
#include "MultiReadGroup.h"
#include "WriteGroupMultiRpcWrapper.h"
#include "Util.h"
#include "WireFormat.h"
#include "Tub.h"
#include "MultiRead.h"
#include "MultiWrite.h"
#include "MultiRemove.h"
#include "RamCloud.h"

#include "plasma/client.h"

#include <stdio.h>
#include <thread>
#include <list>
#include <iostream>
#include <sstream>
#include <atomic>
#include <mutex>
#include <condition_variable>
#include <deque>

using namespace KerA;

namespace sharedHandover {
struct GroupSegmentOffset
{
    uint32_t streamletId;
    uint64_t groupId;
    uint64_t segmentId;
    uint32_t offset; //last object position to read from exclusive, ignored if -1
    uint32_t offsetIndex; //index into cached reusable offsets
    uint32_t pulledRecords; //n records pulled per segment
    bool segmentProcessed;
};

#define CACHE_SIZE 1000 //gives how many shared objects are pulled in the plasma store
#define MAXNUMSTREAMLETS 128

//key=streamletId, value is a list of indexes to batchesReuse; tail is first to be processed
//streamletBatches[streamletId] is protected by streamletActiveLocks[streamletId % MAXNUMSTREAMLETS]
std::map<uint32_t, std::deque<uint32_t>> streamletBatches;

//contains batch objects with objects' content in buffers[i]
//access is protected by streamletActiveLocks[streamletId%MAXNUMSTREAMLETS]
std::array<MultiReadGroupObject*, CACHE_SIZE> batchesReuse; //size of CACHE_SIZE

//here the consumers push processed object's indexes
//it is used by pullers
std::deque<uint32_t> batchesReuseIndexes;

std::atomic<uint64_t> records; //total records pulled and processed until now
std::atomic<bool> pullerStop;

Tub<Buffer> buffers[CACHE_SIZE];
Tub<GroupSegmentInfo> batchOffsets[CACHE_SIZE];

Tub<GroupSegmentOffset> readerOffsets[CACHE_SIZE];
std::deque<uint32_t> readerOffsetsReuseIndexes;

static const uint64_t INVALID_SEGMENT_ID = -1UL;
static const uint32_t INVALID_SEGMENT_OFFSET = -1U;

#define JNI_FALSE  0
#define JNI_TRUE   1

/**
 * Calls Java to read the results in the ByteBuffer and store them on the Java
 * heap, so that C++ can resume filling the buffer from the beginning without
 * fear of overwriting data that has not yet been stored in Java.
 */
bool flushBuffer(
        JNIEnv *env,
        const jobject &jRamCloud,
        const jclass &cls,
        const jmethodID &mid,
        ByteBuffer &buffer) {
//    jclass cls = env->GetObjectClass(jRamCloud);
//    jmethodID mid = env->GetMethodID(cls, "unloadBufferKera", "()Ljava/lang/String;");
    jint res = env->CallNonvirtualIntMethod(jRamCloud, cls, mid);
    buffer.rewind();
    return (res == JNI_TRUE);
}

} //end namespace sharedHandover

JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppStartPullThread(JNIEnv *env,
        jobject jRamCloud,
        jlong byteBufferPointer) {
    using namespace sharedHandover;
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();

    plasma::PlasmaClient plasmaClient;
    ARROW_CHECK_OK(plasmaClient.Connect("/tmp/plasma", ""));

    jclass cls = env->GetObjectClass(jRamCloud);
    jmethodID mid = env->GetMethodID(cls, "unloadBufferKera", "()I");

    uint64_t readerId = buffer.read<uint64_t>();
    uint64_t tableId1 = buffer.read<uint64_t>(); //streamId
    uint64_t recordCount = buffer.read<uint64_t>();
    uint32_t nNodes = buffer.read<uint32_t>();
    uint32_t NSTREAMLETS = buffer.read<uint32_t>();
    uint32_t NACTIVEGROUPS = buffer.read<uint32_t>();
    uint32_t BATCH_STREAM = buffer.read<uint32_t>();
    uint32_t streamletCount = buffer.read<uint32_t>();
    uint32_t NRECORDS = buffer.read<uint32_t>(); //used for throttle

    uint64_t totalObjectsSize = 1; //650000000UL; //if 1-> a markup for pull based, else push based/notify strategy

    std::vector<uint32_t> streamletIds;
    for (uint32_t si = 1; si <= streamletCount; si++) {
        uint32_t streamletId = buffer.read<uint32_t>();
        streamletIds.push_back(streamletId);
        streamletBatches[streamletId] = {};
    }

    fprintf(stdout, "Started client. Params [readerId, streamId, nNodes, nStreamlets]: [%lu %lu %u %u]\n", readerId, tableId1, nNodes, NSTREAMLETS);
    fprintf(stdout, "Streamlets %u", streamletIds.size());
    fflush(stdout);

    records = 0;

    const uint16_t keysize = 9;
    const uint64_t fakeKeyHash = -1;
    for (uint32_t b = 0; b < CACHE_SIZE; b++) {
        buffers[b].construct();
        batchOffsets[b].construct();

        //only for cached reader offsets
        readerOffsets[b].construct();
        readerOffsetsReuseIndexes.push_back(b);

        MultiReadGroupObject* multiReadGroupObject =
                new MultiReadGroupObject(tableId1, fakeKeyHash, &buffers[b], batchOffsets[b].get());
        multiReadGroupObject->respvalue->get()->reset();

        batchesReuse[b] = multiReadGroupObject;
        batchesReuseIndexes.push_back(b);
    }
    pullerStop = false;

    Tub<Buffer> objectsDataBuffer;
    objectsDataBuffer.construct();

    try {

        fprintf(stdout, "==========STARTING pull_batches===============\n");
        fflush (stdout);

        uint32_t totalNumberObjectsRead = 0;
        uint64_t totalCountValueLength = 0;

        //used for throttle
        uint64_t startInsertSec = Cycles::rdtsc();
        uint64_t stopInsertSec = Cycles::rdtsc();
        uint32_t totalRecords = 0;

        //puller cached last used offsets - used by next requests
        std::unordered_map<uint32_t,  //streamletId
            std::map<uint64_t, //groupId
            GroupSegmentOffset*> //last segment offset/s in order of segmentId
        > streamletIdGroupIdOffsets; //managing reader offsets

        std::unordered_map<uint32_t,  //streamletId
            std::map<uint64_t, //groupId
                std::deque<uint64_t> //last segment offset/s in order of segmentId
        >> streamletIdGroupIdNextSegments; //managing reader offsets - it keeps a list of segment ids for [streamletId, groupId]

        for(uint32_t sindex=0; sindex<streamletIds.size(); sindex++)
        {
            streamletIdGroupIdOffsets[streamletIds[sindex]] = {}; //no group, no segment offset
            streamletIdGroupIdNextSegments[streamletIds[sindex]] = {}; //no group, no next segment/s
        }

        uint32_t totalNumberObjectsPulled = 0;
        uint32_t streamletSpan = NSTREAMLETS; //number of streamlets
        uint64_t tabletRange = 1 + ~0UL / streamletSpan;
        uint64_t keyHash = -1; // (streamletId-1) * tabletRange;
        const uint32_t maxStreamletFetchBytes = 1000000; //not used; up to this n bytes per streamlet

        while (!pullerStop && totalNumberObjectsPulled <= recordCount)
        {
            std::unordered_map<uint32_t,  //streamletId
                std::map<uint64_t, //groupId
                    uint32_t> //index into batchesReuse
            > streamletIdGroupIdIndexes; //managing used batch indexes

            //we should have at least one batch object for each streamlet's active group
            //each request is guaranteed to have one batch for each streamlet group
            if (batchesReuseIndexes.size() < NACTIVEGROUPS * streamletIds.size()) {
                Cycles::sleep(1000);
                continue;
            }

            std::vector<MultiReadGroupObject*> batches; //to be pulled

            //for each streamlet: check and take next available groupId/s - from group-offset cache or query
            for(uint32_t sindex=0; sindex<streamletIds.size(); sindex++)
            {
                uint32_t streamletId = streamletIds[sindex];
                keyHash = (streamletId-1) * tabletRange;
                streamletIdGroupIdIndexes[streamletId] = {};

                uint32_t streamletGroupLength = streamletIdGroupIdOffsets[streamletId].size();

                //get next available [NACTIVEGROUPS - streamletGroupLength] groups
                if(NACTIVEGROUPS - streamletGroupLength > 0)
                {
                    Buffer groupsBuffer;
                    ramcloud->getNextAvailableGroupSegmentIds(tableId1,
                            streamletId, readerId, keyHash,
                            NACTIVEGROUPS - streamletGroupLength, &groupsBuffer);
                    uint32_t groupRespOffset = 0;
                    const WireFormat::GetNextAvailableGroupSegmentId::Response* groupRespHdr =
                            groupsBuffer.getOffset<WireFormat::GetNextAvailableGroupSegmentId::Response>(0);
                    groupRespOffset += sizeof32(*groupRespHdr);
                    uint32_t discoveredGroups = groupRespHdr->length;

                    for (uint32_t i=0; i<discoveredGroups; i++)
                    {
                        const GroupSegmentId* nextGroup = groupsBuffer.getOffset<GroupSegmentId>(groupRespOffset);
                        uint64_t nextGroupId = nextGroup->groupId;
                        if(!contains(streamletIdGroupIdOffsets[streamletId], nextGroupId)) {
                            streamletIdGroupIdOffsets[streamletId][nextGroupId] = NULL; //next offset to be initialized
                            streamletIdGroupIdNextSegments[streamletId][nextGroupId] = {};
                        }

                        if(!contains(streamletIdGroupIdIndexes[streamletId], nextGroupId)) {
                            streamletIdGroupIdIndexes[streamletId][nextGroupId] = -1; //fake index to be initialized
                        }
    //                    RAMCLOUD_LOG(NOTICE, ">>>>>>>>>>>>>Discovered groupId %lu", nextGroupId );
                        groupRespOffset += sizeof32(*nextGroup);
                    }
                }

                //for each group obtain next available segment from group-offset cache or query it, update streamletIdGroupIdOffsets
                for (auto it = streamletIdGroupIdOffsets[streamletId].begin();
                        it != streamletIdGroupIdOffsets[streamletId].end(); ++it)
                {
                    uint64_t groupId = it->first;
                    uint64_t segmentId = INVALID_SEGMENT_ID; //it->second.front()->segmentId;
                    uint32_t offset = INVALID_SEGMENT_OFFSET; //it->second.front()->offset;

                    GroupSegmentOffset* segmentInfo = streamletIdGroupIdOffsets[streamletId][groupId]; //if I have a group-offset use it
                    if(segmentInfo != NULL)
                    {
                        segmentId = segmentInfo->segmentId;
                        offset = segmentInfo->offset;
                    }

                    //if segmentId invalid OR
                    //segmentId valid and segment processed => query getSegmentsByGroupId
                    if(segmentId == INVALID_SEGMENT_ID || (segmentInfo != NULL && segmentInfo->segmentProcessed))
                    {
                        //4. first check streamletIdGroupIdNextSegments
                        if(streamletIdGroupIdNextSegments[streamletId][groupId].size() > 0)
                        {
                            uint64_t nextSegmentId = streamletIdGroupIdNextSegments[streamletId][groupId].front();
                            streamletIdGroupIdNextSegments[streamletId][groupId].pop_front();

                            if(segmentInfo == NULL) {
                                segmentInfo = readerOffsets[readerOffsetsReuseIndexes.front()].get();
                                segmentInfo->offsetIndex = readerOffsetsReuseIndexes.front(); // to be pushed back for reuse
                                readerOffsetsReuseIndexes.pop_front();
                                segmentInfo->streamletId = streamletId;
                                segmentInfo->groupId = groupId;
                                streamletIdGroupIdOffsets[streamletId][groupId] = segmentInfo;
                            }
                            segmentInfo->segmentProcessed = false;
                            segmentInfo->pulledRecords = 0;
                            segmentInfo->segmentId = nextSegmentId;
                            segmentInfo->offset = 0;
                            continue;
                        }

                        //5. get next available segments by groupId and its current group-offset segment if one exists
                        bool useGroupOffset = segmentId != INVALID_SEGMENT_ID;
                        Buffer responseBufferSegments;
                        //should return segments starting from provided offset > lastOffset.segmentId
                        uint32_t groupSegmentsCount = ramcloud->getSegmentsByGroupId(
                                tableId1, streamletId, keyHash, groupId, useGroupOffset,
                                segmentId, offset, -1, &responseBufferSegments);

                        if(groupSegmentsCount == 0) {
                            continue; //this groupId has no new segments
                        }

                        //6. identify segments in responseBufferSegments
                        uint32_t respOffset = 0;
                        //first object in the response
                        const WireFormat::GetSegmentsByGroupId::Response* respHdr =
                                responseBufferSegments.getOffset<WireFormat::GetSegmentsByGroupId::Response>(respOffset);
                        respOffset = +sizeof32(*respHdr);

                        assert(respHdr->length == groupSegmentsCount);

                        //7. initialize streamletIdGroupIdOffsets
                        for (size_t i = 0; i < groupSegmentsCount; i++)
                        {
                            const SegmentIdAndOffset* next = responseBufferSegments.getOffset<SegmentIdAndOffset>(respOffset);
                            assert (next != NULL);
                            respOffset += sizeof32(*next);
                            streamletIdGroupIdNextSegments[streamletId][groupId].push_back(next->segmentId);
                        }

                        if(streamletIdGroupIdNextSegments[streamletId][groupId].size() > 0) {
                            uint64_t nextSegmentId = streamletIdGroupIdNextSegments[streamletId][groupId].front();
                            streamletIdGroupIdNextSegments[streamletId][groupId].pop_front();

                            if(segmentInfo == NULL) {
                                segmentInfo = readerOffsets[readerOffsetsReuseIndexes.front()].get();
                                segmentInfo->offsetIndex = readerOffsetsReuseIndexes.front(); // to be pushed back for reuse
                                readerOffsetsReuseIndexes.pop_front();
                                segmentInfo->streamletId = streamletId;
                                segmentInfo->groupId = groupId;
                                streamletIdGroupIdOffsets[streamletId][groupId] = segmentInfo;
                            }
                            segmentInfo->segmentProcessed = false;
                            segmentInfo->pulledRecords = 0;
                            segmentInfo->segmentId = nextSegmentId;
                            segmentInfo->offset = 0;
                        }
                    }

                    if(segmentInfo == NULL) {
                        continue;
                    }

                    //take batch
                    if(batchesReuseIndexes.size() == 0) {
                        continue;
                    }
                    uint32_t batchIndex = batchesReuseIndexes.front();
                    batchesReuseIndexes.pop_front(); //exclusive access

                    MultiReadGroupObject* nextBatch = batchesReuse[batchIndex];
                    streamletIdGroupIdIndexes[streamletId][groupId] = batchIndex;
                    //we push batchIndex to streamletBatches after pulling succeeded

                    //do reset nextBatch
                    // initialize this field based on streamlet locality with this process (same node)
                    //for now consumers get local streamlets
                    nextBatch->isSegmentClosed = false;
                    nextBatch->isGroupProcessed = false;
                    nextBatch->nextOffset = 0;
                    nextBatch->numberObjectsRead = 0;
                    nextBatch->numberOfObjectEntries = 0;
                    nextBatch->responseLength = 0;
                    nextBatch->responseOffset = 0;
                    nextBatch->respvalue->get()->reset(); //done in MultiReadGroup: destroy & reset
                    nextBatch->keyHash = keyHash; //has to reflect streamletId and streamletSpan
                    nextBatch->segmentInfo->streamletId = streamletId;
                    nextBatch->segmentInfo->groupId = groupId;
                    nextBatch->segmentInfo->segmentId = segmentInfo->segmentId;
                    nextBatch->segmentInfo->offset = segmentInfo->offset;
                    nextBatch->segmentInfo->maxObjects = 100000;
                    nextBatch->segmentInfo->maxResponseLength = BATCH_STREAM; //batch size

                    batches.push_back(nextBatch);
                }
            }

            if(batches.size() == 0) {
                Cycles::sleep(1000);
                continue;
            }

            //build request multi-read with head segmentIdAndOffset for each [streamletId,groupId]
            //pull, check segment not closed, every time make sure each streamlet is pulled, else check again for segments/groups
            //always take first group/first segment

            uint32_t requestsSize = batches.size();
            MultiReadGroupObject* requests[requestsSize];
            for(size_t i=0; i<requestsSize; i++) {
                requests[i] = batches.back();
                batches.pop_back();
            }

            ramcloud->multiReadGroup(requests, requestsSize, maxStreamletFetchBytes,
                    NULL, static_cast<uint32_t>(0), totalObjectsSize, readerId, true);

            //check each object - maybe segment processed or group processed
            for(uint32_t sindex=0; sindex<requestsSize; sindex++)
            {
                MultiReadGroupObject* nextBatch = requests[sindex];
                uint32_t streamletId = nextBatch->segmentInfo->streamletId;
                keyHash = (streamletId-1) * tabletRange;
                assert(keyHash == nextBatch->keyHash);
                uint64_t groupId = nextBatch->segmentInfo->groupId;

                GroupSegmentOffset* streamletGroupOffset = streamletIdGroupIdOffsets[streamletId][groupId];

                assert(groupId == streamletGroupOffset->groupId);
                assert(streamletGroupOffset->segmentId == nextBatch->segmentInfo->segmentId);

                //9. get and process objects from response
                totalNumberObjectsPulled += nextBatch->numberObjectsRead;
                streamletGroupOffset->pulledRecords += nextBatch->numberObjectsRead;
                // total number objects read per segment to avoid numberObjectsRead to be 0

//                fprintf(stdout, ">>> Current Group Offset [segmentId-offset-groupId]: %lu %u %lu SegmLength %u Entries %u \n",
//                        nextBatch->segmentInfo->segmentId, nextBatch->segmentInfo->offset, groupId,
//                        streamletIdGroupIdOffsets[streamletId][groupId].front()->pulledRecords,
//                        nextBatch->numberOfObjectEntries);

                //10. condition to exit segment processing: closed and all records pulled
                bool segmentProcessedAndClosed = (nextBatch->isSegmentClosed)
                        && (streamletGroupOffset->pulledRecords == nextBatch->numberOfObjectEntries)
                        && (nextBatch->numberOfObjectEntries > 0);//double checking

                //mark this segment processed and closed
                //so next step involves another getSegmentsByGroupId query
                streamletGroupOffset->segmentProcessed = segmentProcessedAndClosed;

                if(nextBatch->numberObjectsRead == static_cast<uint32_t>(0)
                        && !streamletGroupOffset->segmentProcessed) {
                    fprintf(stdout, ">>> Current Group Offset [segmentId-offset-groupId]: %lu %u %lu Entries %u \n",
                            nextBatch->segmentInfo->segmentId, nextBatch->segmentInfo->offset, groupId,
                            nextBatch->numberOfObjectEntries);

                    continue;
                }

                //move segment offset to last pulled record's offset
                if(nextBatch->nextOffset > 0) {
                    streamletGroupOffset->offset = nextBatch->nextOffset;
                }

                //we have a new offset
                if(segmentProcessedAndClosed && nextBatch->isGroupProcessed)
                {
//                    bool isGroupProcessed = ramcloud->updateGroupOffset(tableId1,
//                            keyHash, streamletId, readerId,
//                            groupId, false, -1, -1, -1, -1,
//                            streamletGroupOffset->segmentId,
//                            streamletGroupOffset->offset, -1, -1, NULL);
//
//                    if (isGroupProcessed) {
    //                    fprintf(stdout, ">>>>>>>>>>>>>groupProcessed [streamletId,groupId] [%lu,%lu] \n", streamletId, groupId);
    //                    fflush(stdout);
                        //remove groupId and give back index
                        readerOffsetsReuseIndexes.push_back(streamletGroupOffset->offsetIndex);
                        streamletIdGroupIdOffsets[streamletId].erase(groupId);
                        streamletIdGroupIdNextSegments[streamletId].erase(groupId);
//                    }
                }
            }

            //from streamletIdGroupIdIndexes give back indexes to consumers - update streamletBatches!

            for(uint32_t sindex=0; sindex<streamletIds.size(); sindex++)
            {
                uint32_t streamletId = streamletIds[sindex];

                for (auto it = streamletIdGroupIdIndexes[streamletId].begin();
                        it != streamletIdGroupIdIndexes[streamletId].end(); ++it)
                {
                    uint32_t batchIndex = it->second; //it->first is groupId
                    if(batchIndex != -1)
                    {
                        streamletBatches[streamletId].push_back(batchIndex);
                    }
                }
            }

            //start consumer

            //take next NACTIVEGROUPS available batches for each streamlet, process them, and release their indexes back to puller
            for (uint32_t sindex=0; sindex < streamletIds.size(); sindex++)
            {
                uint32_t streamletId = streamletIds[sindex];
                uint32_t activeGroups = NACTIVEGROUPS < 16 ? 1 : NACTIVEGROUPS; //todo tune this param

                // optimizing for plasma calls
                // collect batches and their indexes
                std::vector<MultiReadGroupObject*> nextBatchList;
                std::vector<uint32_t> batchesReuseFrontIndexList;
                while (activeGroups>0) {
                    activeGroups--;
                    if (streamletBatches[streamletId].size() > 0) {
                        uint32_t batchesReuseFrontIndex = streamletBatches[streamletId].front();
                        batchesReuseFrontIndexList.push_back(batchesReuseFrontIndex); //save index
                        MultiReadGroupObject* nextBatch = batchesReuse[batchesReuseFrontIndex];
                        nextBatchList.push_back(nextBatch); //save batch
                        assert(streamletId == nextBatch->segmentInfo->streamletId);
                        streamletBatches[streamletId].pop_front(); //it means we 'consumed it
                    } else {
                        break;
                    }
                }

                // prepare object ids
                std::vector<plasma::ObjectID> object_ids;
                std::vector<uint32_t> object_totalLength;
                std::vector<plasma::ObjectBuffer> object_buffers;
                uint32_t respOffset = 0;
                uint32_t totalLength = 0;

                for (uint32_t bi=0; bi < nextBatchList.size(); bi++)
                {
                    uint32_t batchesReuseFrontIndex = batchesReuseFrontIndexList[bi];
                    MultiReadGroupObject* nextBatch = nextBatchList[bi];
                    uint32_t numberObjectsRead = nextBatch != NULL ? nextBatch->numberObjectsRead : 0;
                    if (numberObjectsRead != static_cast<uint32_t>(0)) {
                        Buffer* responseBuffer = nextBatch->respvalue->get(); // shared buffer
                        assert(responseBuffer != NULL);

                        totalLength = *responseBuffer->getOffset<uint32_t>(respOffset);
                        object_totalLength.push_back(totalLength);

                        const void* objectId = responseBuffer->getRange(sizeof32(uint32_t), 20); //todo 20 is plasma object size
                        plasma::ObjectID object_id;
                        std::memcpy(&object_id, objectId, sizeof(object_id));
                        object_ids.push_back(object_id);
                    } else { // empty request, free batch index, maintain object position
                        // give back nextBatch's index
                        batchesReuseIndexes.push_back(batchesReuseFrontIndex);
                        /*
                        totalLength = 0;
                        object_totalLength.push_back(totalLength); //we dont have an object
                        plasma::ObjectID object_id; //kind of empty fake object
                        object_ids.push_back(object_id);
                        */
                    }
                }

                //assert(object_ids.size() == nextBatchList.size());

                // init object buffer's shared pointers - reduce plasma client calls
                //plasma::PlasmaClient plasmaClient;
                //ARROW_CHECK_OK(plasmaClient.Connect("/tmp/plasma", ""));
                ARROW_CHECK_OK(plasmaClient.Get(object_ids, -1, &object_buffers));

                // iterate and flush buffer
                for (uint32_t bi=0; bi < object_ids.size(); bi++)
                {
                    uint32_t batchesReuseFrontIndex = batchesReuseFrontIndexList[bi];
                    totalLength = object_totalLength[bi];
                    if (totalLength != static_cast<uint32_t>(0)) { //if we have an object
                        respOffset = 0;

                        //actual data -> object_buffers[0].data->data()
                        objectsDataBuffer.get()->reset();
                        objectsDataBuffer.get()->appendExternal(object_buffers[bi].data->data(), totalLength);
                        SegmentIterator it(object_buffers[bi].data->data(), totalLength);
                        buffer.rewind();
                        buffer.write(streamletId);
                        uint32_t iterRecords = 0;
                        buffer.write(iterRecords); //holder for number of objects

                        StreamObject obj0(tableId1, *objectsDataBuffer.get());
                        //uint32_t headerGroupSize = sizeof32(StreamObject::HeaderGroup);

                        while (!it.isDone())
                        {
                            respOffset += it.getEntryOffset();
                            uint32_t itLength = it.getLength();

                            if (it.getType() != LOG_ENTRY_TYPE_CHUNK) {
                                obj0.reset(respOffset, itLength);
                                /*
                                StreamObject obj0(tableId1, 1, 0, *objectsDataBuffer.get(),
                                        respOffset + sizeof32(StreamObject::HeaderGroup),
                                        it.getLength() - sizeof32(StreamObject::HeaderGroup));
                                fprintf(stdout,
                                        ">>> Latency: key=%s value=%s now=%lu\n",
                                        string(reinterpret_cast<const char*>(obj0.getKey()), obj0.getKeyLength()).c_str(),
                                        string(reinterpret_cast<const char*>(obj0.getValue()), obj0.getValueLength()).c_str(),
                                        Cycles::toMicroseconds(Cycles::rdtsc()));
                                */
                                // if no more space for next record
                                uint32_t keyLength = obj0.getKeyLength();
                                uint32_t valueLength = obj0.getValueLength();
                                if (buffer.mark + 8 + keyLength + valueLength >= SHARED_JAVA_BUFFER_SIZE) {
                                    buffer.mark = 4;
                                    buffer.write(iterRecords); //update
                                    //fprintf(stdout, "flushBuffer %d", iterRecords);
                                    if (flushBuffer(env, jRamCloud, cls, mid, buffer)) {
                                        pullerStop = true;
                                        break;
                                    }
                                    records += iterRecords;
                                    totalRecords += iterRecords;
                                    iterRecords = 0; //reset

                                    buffer.write(streamletId);
                                    buffer.write(iterRecords);
                                }
                                //key
                                buffer.write(keyLength);
                                memcpy(buffer.getVoidPointer(), obj0.getKey(), keyLength);
                                buffer.mark += keyLength;
                                //value
                                buffer.write(valueLength);
                                memcpy(buffer.getVoidPointer(), obj0.getValue(), valueLength);
                                buffer.mark += valueLength;
                                iterRecords++;

                                totalCountValueLength += valueLength; //something todo broken here, value higher than expected
                            }
                            respOffset += it.getLength();
                            it.next();
                        }
                        buffer.mark = 4;
                        buffer.write(iterRecords); //update
                        //fprintf(stdout, "flushBuffer %d", iterRecords);
                        if (flushBuffer(env, jRamCloud, cls, mid, buffer)) {
                            pullerStop = true;
                            break;
                        }

                        //throttle, no more than NRECORDS/second
                        stopInsertSec = Cycles::rdtsc();
                        double time = static_cast<double>(Cycles::toMicroseconds(stopInsertSec - startInsertSec));

                        if (totalRecords >= NRECORDS && time <= 999999) { //check should we throttle
                            totalRecords = 0;
                            Cycles::sleep(1000000 - time); //100us
                            startInsertSec = Cycles::rdtsc();
                        }
                        if (time > 999999) { //one second passed, reset throttle stuff?
                            totalRecords = 0; //maybe we produced less last second, reset counter
                            startInsertSec = stopInsertSec;
                        }

                        //ARROW_CHECK_OK(plasmaClient.Release(object_ids[bi])); //some error object in use here todo

                        records += iterRecords;
                        totalRecords += iterRecords;

                        if (records >= recordCount) {
                            pullerStop = true;
                            fprintf(stdout,
                                    "======== exiting write_batches totalValueLength %lu =======\n",
                                    totalCountValueLength);
                            fflush(stdout);
                            break;
                        }

                        //give back nextBatch's index
                        batchesReuseIndexes.push_back(batchesReuseFrontIndex);
                    }
                }
    //                ARROW_CHECK_OK(plasmaClient.Disconnect());
            }


//            fprintf(stdout,
//                    "======== records totalValueLength %lu %lu =======\n",
//                    (unsigned)records, totalCountValueLength);

            if(records >= recordCount) {
                pullerStop = true;
                fprintf(stdout,
                        "======== exiting write_batches totalValueLength %lu =======\n",
                        totalCountValueLength);
                fflush(stdout);
                break;
            }

            //end consumer
        }

        pullerStop = true;
        fprintf(stdout, "==========done pull_batches===============\n");
        fflush (stdout);
    }
    catch (KerA::ClientException& e) {
        fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str()); fflush (stdout);
    }
    catch (KerA::Exception& e) {
        fprintf(stdout, "RAMCloud exception: %s\n", e.str().c_str()); fflush (stdout);
    }

    ARROW_CHECK_OK(plasmaClient.Disconnect());

    fprintf(stdout, "======================ENDING reading=================\n");
    fflush(stdout);

    buffer.rewind();
    buffer.write<uint32_t>(0); //normally handled with EXCEPTION_CATCHER
}

static void printBuffer(ByteBuffer buffer, uint32_t len) {
    printf("\nBuffer %p, %u: \n%s\n",
           buffer.pointer,
           buffer.mark,
           Util::hexDump(
                   static_cast<void*>(buffer.pointer),
                   len).c_str());
}


/**
 * Create a RejectRules pointer from a byte array representation of a Java
 * RejectRules object.
 *
 * \param env
 *      The current JNI environment.
 * \param jRejectRules
 *      A Java byte array holding the data for a RejectRules struct.
 * \return A RejectRules object from the given byte array.
 */
static RejectRules
createRejectRules(JNIEnv* env, jbyteArray jRejectRules) {
    RejectRules out;
    void* rulesPointer = env->GetPrimitiveArrayCritical(jRejectRules, 0);
    out.givenVersion = static_cast<uint64_t*> (rulesPointer)[0];
    out.doesntExist = static_cast<char*> (rulesPointer)[8];
    out.exists = static_cast<char*> (rulesPointer)[9];
    out.versionLeGiven = static_cast<char*> (rulesPointer)[10];
    out.versionNeGiven = static_cast<char*> (rulesPointer)[11];
    env->ReleasePrimitiveArrayCritical(jRejectRules, rulesPointer, JNI_ABORT);
    /*
    printf("Created RejectRules:\n\tVersion: %u\n\tDE: %u\n\tE: %u\n\tVLG: %u\n\tVNG: %u\n",
           out.givenVersion,
           out.doesntExist,
           out.exists,
           out.versionLeGiven,
           out.versionNeGiven); */
    return out;
}

/**
 * Gets the pointer to the memory region the specified ByteBuffer points to.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param jByteBuffer
 *      The java.nio.ByteBuffer object, created with
 *      ByteBuffer.allocateDirect(), to retrieve a pointer for.
 * \return A pointer, as a Java long, to the memory region allocated for the
 *      specified ByteBuffer.
 */
JNIEXPORT jlong
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppGetByteBufferPointer(
        JNIEnv *env,
        jclass jRamCloud,
        jobject jByteBuffer) {
    jlong out = reinterpret_cast<jlong>(env->GetDirectBufferAddress(jByteBuffer));
    return out;
}

/**
 * Construct a RamCloud for a particular cluster.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          4 bytes for length of cluster locator string
 *          NULL-terminated string for cluster locator
 *          NULL-terminated string for cluster name
 *      The format for the output buffer is:
 *          4 bytes for status code of the RamCloud constructor
 *          8 bytes for a pointer to the created RamCloud object
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppConnect(
        JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    uint32_t locatorLength = buffer.read<uint32_t>();
    char* locator = buffer.pointer + buffer.mark;
    buffer.mark += 1 + locatorLength;
    char* name = buffer.pointer + buffer.mark;

    RamCloud* ramcloud = NULL;
    buffer.rewind();
    try {
        ramcloud = new RamCloud(locator, name);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(reinterpret_cast<uint64_t>(ramcloud));
}

/**
 * Disconnect from the RAMCloud cluster. This causes the JNI code to destroy
 * the underlying RamCloud C++ object.
 * 
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param ramcloudClusterHandle
 *      A pointer to the C++ RamCloud object.
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppDisconnect(JNIEnv *env,
        jclass jRamCloud,
        jlong ramcloudClusterHandle) {
    delete reinterpret_cast<RamCloud*> (ramcloudClusterHandle);
}

/**
 * Create a new table.
 * 
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          4 bytes for the number of servers across which this table will be
 *              divided
 *          NULL-terminated string for the name of the table to create
 *      The format for the output buffer is:
 *          4 bytes for the status code of the createTable operation
 *          8 bytes for the ID of the table created
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppCreateTable(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint32_t serverSpan = buffer.read<uint32_t>();
    uint32_t streamletSpan = buffer.read<uint32_t>();
    uint32_t countLogs = buffer.read<uint32_t>();
    char* tableName = buffer.pointer + buffer.mark;
    uint64_t tableId;
    buffer.rewind();
    try {
        tableId = ramcloud->createTable(tableName, serverSpan, streamletSpan, countLogs);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(tableId);
}

/**
 * Delete a table.
 *
 * All objects in the table are implicitly deleted, along with any
 * other information associated with the table.  If the table does
 * not currently exist then the operation returns successfully without
 * actually doing anything.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          NULL-terminated string for the name of the table to delete
 *      The format for the output buffer is:
 *          4 bytes for the status code of the dropTable operation
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppDropTable(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    char* tableName = buffer.pointer + buffer.mark;
    buffer.rewind();
    try {
        ramcloud->dropTable(tableName);
    } EXCEPTION_CATCHER(buffer);
}

/**
 * Given the name of a table, return the table's unique identifier, which
 * is used to access the table.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          NULL-terminated string for the name of the table to get the ID for
 *      The format for the output buffer is:
 *          4 bytes for the status code of the getTableId operation
 *          8 bytes for the ID of the table specified
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppGetTableId(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    char* tableName = buffer.pointer + buffer.mark;
    uint64_t tableId;
    buffer.rewind();
    try {
        tableId = ramcloud->getTableId(tableName);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(tableId);
}


#if TIME_CPP
uint32_t test_num_current = 0;
const uint32_t test_num_times = 100000;

uint64_t test_times[test_num_times];
#endif

/**
 * Read the current contents of an object.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to read from
 *          4 bytes for the length of the key to find
 *          byte array for the key to find
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the read operation
 *          8 bytes for the version of the read object
 *          4 bytes for the size of the read value
 *          byte array for the read value
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppRead(
        JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer byteBuffer(byteBufferPointer);
    RamCloud* ramcloud = byteBuffer.readPointer<RamCloud>();
    uint64_t tableId = byteBuffer.read<uint64_t>();
    uint32_t keyLength = byteBuffer.read<uint32_t>();
    void* key = byteBuffer.getVoidPointer(keyLength);
    RejectRules rejectRules = byteBuffer.read<RejectRules>();
    Buffer buffer;
    uint64_t version;
    byteBuffer.rewind();
#if TIME_CPP
    uint64_t start = Cycles::rdtsc();
#endif
    try {
        ramcloud->read(tableId,
                       key,
                       keyLength,
                       &buffer,
                       &rejectRules,
                       &version);
    } EXCEPTION_CATCHER(byteBuffer);
#if TIME_CPP
    test_times[test_num_current] = Cycles::rdtsc() - start;
    test_num_current++;
    if (test_num_current == test_num_times) {
        std::sort(boost::begin(test_times), boost::end(test_times));
        printf("Median C++ Read Time: %f\n", Cycles::toSeconds(test_times[test_num_times / 2]) * 1000000);
        test_num_current = 0;
    }
#endif
    byteBuffer.write(version);
    byteBuffer.write(buffer.size());
    buffer.copy(0, buffer.size(), byteBuffer.getVoidPointer());
}

/**
 * Delete an object from a table. If the object does not currently exist
 * then the operation succeeds without doing anything (unless rejectRules
 * causes the operation to be aborted).
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to remove from
 *          4 bytes for the length of the key to find and remove
 *          byte array for the key to find and remove
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the remove operation
 *          8 bytes for the version of the object just before deletion
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppRemove(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint64_t tableId = buffer.read<uint64_t>();
    uint32_t keyLength = buffer.read<uint32_t>();
    void* key = buffer.getVoidPointer(keyLength);
    RejectRules rejectRules = buffer.read<RejectRules>();
    uint64_t version;
    buffer.rewind();
    try {
        ramcloud->remove(tableId, key, keyLength, &rejectRules, &version);
    } EXCEPTION_CATCHER(buffer);
    buffer.write(version);
}

/**
 * Replace the value of a given object, or create a new object if none
 * previously existed.
 *
 * \param env
 *      The current JNI environment.
 * \param jRamCloud
 *      The calling class.
 * \param byteBufferPointer
 *      A pointer to the ByteBuffer through which Java and C++ will communicate.
 *      The format for the input buffer is:
 *          8 bytes for a pointer to a C++ RamCloud object
 *          8 bytes for the ID of the table to write to
 *          4 bytes for the length of the key to write
 *          byte array for the key to write
 *          4 bytes for the length of the value to write
 *          byte array for the valule to write
 *          12 bytes representing the RejectRules
 *      The format for the output buffer is:
 *          4 bytes for the status code of the write operation
 *          8 bytes for the version of the object written
 */
JNIEXPORT void
JNICALL Java_fr_inria_kera_KeraBindingsShared_cppWrite(JNIEnv *env,
        jclass jRamCloud,
        jlong byteBufferPointer) {
    ByteBuffer buffer(byteBufferPointer);
    RamCloud* ramcloud = buffer.readPointer<RamCloud>();
    uint64_t tableId = buffer.read<uint64_t>();
    uint32_t keyLength = buffer.read<uint32_t>();
    void* key = buffer.getVoidPointer(keyLength);
    uint32_t valueLength = buffer.read<uint32_t>();
    void* value = buffer.getVoidPointer(valueLength);
    RejectRules rules = buffer.read<RejectRules>();
    uint64_t version;
    buffer.rewind();
#if TIME_CPP
    uint64_t start = Cycles::rdtsc();
#endif
    try {
        ramcloud->write(tableId,
                        key, keyLength,
                        value, valueLength,
                        &rules,
                        &version);
    } EXCEPTION_CATCHER(buffer);
#if TIME_CPP
    test_times[test_num_current] = Cycles::rdtsc() - start;
    test_num_current++;
    if (test_num_current == test_num_times) {
        std::sort(boost::begin(test_times), boost::end(test_times));
        printf("Median C++ Write Time: %f\n", Cycles::toSeconds(test_times[test_num_times / 2]) * 1000000);
        test_num_current = 0;
    }
#endif
    buffer.write(version);
}
